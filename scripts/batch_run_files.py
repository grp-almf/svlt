import argparse

from model_server.clients.batch_runner import FileBatchRunnerClient


def parse_args():
    parser = argparse.ArgumentParser(
        description='Push batch analysis of image files to server',
    )
    parser.add_argument(
        '--json',
        help='JSON file to configure batch job',
        required=True,
    )
    parser.add_argument(
        '--host',
        default='127.0.0.1',
        help='bind socket to this host'
    )
    parser.add_argument(
        '--port',
        default=8000,
        help='bind socket to this port',
    )
    parser.add_argument(
        '--raise_nonsuccess',
        default=True,
        help='raise a clientside exception if a response other than 200 is received',
    )
    return parser.parse_args()


def main(args):
    client = FileBatchRunnerClient(
        conf_json=args.json,
        **args.__dict__,
    )
    client.run()
    return


if __name__ == '__main__':
    args = parse_args()
    print('CLI args:\n' + str(args))
    main(args)
    print('Finished')