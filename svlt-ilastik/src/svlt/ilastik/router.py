from pathlib import Path

from fastapi import APIRouter
from pydantic import BaseModel, Field

from svlt.session import session

from svlt.ilastik import models as ilm

router = APIRouter(
    prefix='/ilastik',
    tags=['ilastik'],
)


import svlt.ilastik.pipelines.px_then_ob
router.include_router(svlt.ilastik.pipelines.px_then_ob.router)


class IlastikParams(BaseModel):
    project_file: str = Field(description='(*.ilp) ilastik project filename')
    duplicate: bool = Field(
        True,
        description='Load another instance of the same project file if True; return existing one if False'
    )

class IlastikPixelClassifierParams(IlastikParams):
    px_class: int = 0
    px_prob_threshold: float = 0.5

@router.put('/seg/load/')
def load_px_model(p: IlastikPixelClassifierParams, model_id=None) -> dict:
    """
    Load an ilastik pixel classifier model from its project file
    """
    return load_ilastik_model(
        ilm.IlastikPixelClassifierModel,
        p,
        model_id=model_id,
    )

@router.put('/pxmap_to_obj/load/')
def load_pxmap_to_obj_model(p: IlastikParams, model_id=None) -> dict:
    """
    Load an ilastik object classifier from pixel predictions model from its project file
    """
    return load_ilastik_model(
        ilm.IlastikObjectClassifierFromPixelPredictionsModel,
        p,
        model_id=model_id,
    )

@router.put('/seg_to_obj/load/')
def load_seg_to_obj_model(p: IlastikParams, model_id=None) -> dict:
    """
    Load an ilastik object classifier from segmentation model from its project file
    """
    return load_ilastik_model(
        ilm.IlastikObjectClassifierFromMaskSegmentationModel,
        p,
        model_id=model_id,
    )

def load_ilastik_model(model_class: ilm.IlastikModel, p: IlastikParams, model_id=None) -> dict:
    if Path(p.project_file).is_absolute():
        pf = p.project_file
    else:
        pf = session.paths['conf'] / p.project_file
        p.project_file = pf.__str__()
    if not p.duplicate:
        existing_model_id = session.find_param_in_loaded_models('project_file', pf, is_path=True)
        if existing_model_id is not None:
            session.log_info(f'An ilastik model from {pf} already existing exists; did not load a duplicate')
            return {'model_id': existing_model_id}
    result = session.load_model(model_class, key=model_id, params=p)
    session.log_info(f'Loaded ilastik model {result} from {pf}')
    return {'model_id': result}