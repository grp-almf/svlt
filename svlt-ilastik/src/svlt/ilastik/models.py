import json
from logging import getLogger
import os
from pathlib import Path
import warnings

import numpy as np
import vigra

from svlt.accessors import PatchStack
from svlt.accessors import GenericImageDataAccessor, InMemoryDataAccessor
from svlt.models import Model, ImageToImageModel, InstanceMaskSegmentationModel, InvalidInputImageError, ParameterExpectedError, SemanticSegmentationModel


class IlastikModel(Model):

    def __init__(self, project_file, autoload=True, enforce_embedded=True, **kwargs):
        """
        Base class for models that run via ilastik shell API
        :param: project_file: path to ilastik project file
        :param autoload: automatically load model into memory if true
        :param enforce_embedded:
            raise an error if all input data are not embedded in the project file, i.e. on the filesystem
        """
        pf = Path(project_file)
        self.enforce_embedded = enforce_embedded
        if not pf.exists():
            raise FileNotFoundError(f'Project file does not exist: {pf}')

        self.shell = None
        self.project_file_abspath = pf.absolute()
        super().__init__(autoload, info={'project_file': pf.__str__(), **kwargs})

    def load(self):
        # suppress warnings when loading ilastik app
        getLogger('ilastik.app').setLevel('ERROR')

        with warnings.catch_warnings():
            warnings.filterwarnings('ignore', category=DeprecationWarning)
            from ilastik import app
            from ilastik.applets.dataSelection.opDataSelection import PreloadedArrayDatasetInfo

        self.PreloadedArrayDatasetInfo = PreloadedArrayDatasetInfo

        os.environ["LAZYFLOW_THREADS"] = "8"
        os.environ["LAZYFLOW_TOTAL_RAM_MB"] = "24000"

        args = app.parse_args([])
        args.headless = True
        args.project = self.project_file_abspath.__str__()
        shell = app.main(args, init_logging=False)

        # validate if inputs are embedded in project file
        h5 = shell.projectManager.currentProjectFile
        for lane in h5['Input Data/infos'].keys():
            for role in h5[f'Input Data/infos/{lane}'].keys():
                grp = h5[f'Input Data/infos/{lane}/{role}']
                if self.enforce_embedded and ('location' in grp.keys()) and grp['location'][()] != b'ProjectInternal':
                    raise IlastikInputEmbedding('Cannot load ilastik project file where inputs are on filesystem')
            assert True
        if not isinstance(shell.workflow, self.get_workflow()):
            raise ParameterExpectedError(
                f'Ilastik project file {self.project_file_abspath} does not describe an instance of {self.__class__}'
            )
        self.shell = shell

        return True

    @property
    def model_shape_dict(self):
        raw_info = self.shell.projectManager.currentProjectFile['Input Data']['infos']['lane0000']['Raw Data']
        ax = raw_info['axistags'][()]
        ax_keys = [ax['key'].upper() for ax in json.loads(ax)['axes']]
        shape = raw_info['shape'][()]
        dd = dict(zip(ax_keys, shape))
        for ci in 'TCZ':
            if ci not in dd.keys():
                dd[ci] = 1
        return dd

    @property
    def labels(self):
        return []

    @property
    def info(self):
        return {**self._info, 'labels': self.labels}

    @property
    def model_chroma(self):
        return self.model_shape_dict['C']

    @property
    def model_3d(self):
        return self.model_shape_dict['Z'] > 1


class IlastikPixelClassifierModel(IlastikModel, SemanticSegmentationModel):
    operations = ['segment', ]

    def __init__(self, px_class: int = 0, px_prob_threshold: float = 0.5, **kwargs):
        self.px_class = px_class
        self.px_prob_threshold = px_prob_threshold
        super(IlastikPixelClassifierModel, self).__init__(
            px_class=px_class,
            px_prob_threshold=px_prob_threshold,
            **kwargs
        )

    @staticmethod
    def get_workflow():
        from ilastik.workflows import PixelClassificationWorkflow
        return PixelClassificationWorkflow

    @property
    def labels(self):
        h5 = self.shell.projectManager.currentProjectFile
        return [l.decode() for l in h5['PixelClassification/LabelNames'][()]]

    def infer(self, input_img: GenericImageDataAccessor) -> (InMemoryDataAccessor, dict):
        if self.model_chroma != input_img.chroma:
            raise IlastikInputShapeError(
                f'Model {self} expects {self.model_chroma} input channels but received {input_img.chroma}'
            )
        if self.model_3d != input_img.is_3d():
            if self.model_3d:
                raise IlastikInputShapeError(f'Model is 3D but input image is 2D')
            else:
                raise IlastikInputShapeError(f'Model is 2D but input image is 3D')

        tagged_input_data = vigra.taggedView(input_img.data, 'yxcz')
        dsi = [
            {
                'Raw Data': self.PreloadedArrayDatasetInfo(preloaded_array=tagged_input_data),
            }
        ]
        pxmaps = self.shell.workflow.batchProcessingApplet.run_export(dsi, export_to_array=True) # [z x h x w x n]

        assert len(pxmaps) == 1, 'ilastik generated more than one pixel map'

        yxcz = np.moveaxis(
            pxmaps[0],
            [1, 2, 3, 0],
            [0, 1, 2, 3]
        )
        return InMemoryDataAccessor(data=yxcz)

    def infer_patch_stack(self, img: PatchStack, crop=True, normalize=False, **kwargs) -> (np.ndarray, dict):
        """
        Iterative over a patch stack, call inference separately on each cropped patch
        :param img: patch stack of input data
        :param crop: pass list of cropped (generally non-uniform) patches to classifier if True
        :param normalize: scale the inference result (generally 0.0 to 1.0) to the range of img if True
        """
        dsi = [
            {
                'Raw Data': self.PreloadedArrayDatasetInfo(
                    preloaded_array=vigra.taggedView(patch, 'yxcz'))

            } for patch in img.get_list(crop=crop)
        ]
        pxmaps = self.shell.workflow.batchProcessingApplet.run_export(dsi, export_to_array=True)  # [z x h x w x n]
        yxcz = [np.moveaxis(pm, [1, 2, 3, 0], [0, 1, 2, 3]) for pm in pxmaps]
        if normalize:
            return PatchStack(yxcz).apply(
                lambda x: (x * img.dtype_max).astype(img.dtype),
                preserve_dtype=False,
            )
        else:
            return PatchStack(yxcz)

    def label_pixel_class(self, img: GenericImageDataAccessor, **kwargs):
        pxmap = self.infer(img)
        tr = kwargs.get('px_prob_threshold', self.px_prob_threshold)
        mask = pxmap.get_mono(
            self.px_class,
        ).apply(
            lambda x: (x > tr), preserve_dtype=False
        )
        return mask


class IlastikObjectClassifierFromMaskSegmentationModel(IlastikModel, InstanceMaskSegmentationModel):

    @staticmethod
    def _make_8bit_mask(nda):
        if nda.dtype == 'bool':
            return 255 * nda.astype('uint8')
        else:
            return nda

    @staticmethod
    def get_workflow():
        from ilastik.workflows.objectClassification.objectClassificationWorkflow import ObjectClassificationWorkflowBinary
        return ObjectClassificationWorkflowBinary

    @property
    def labels(self):
        h5 = self.shell.projectManager.currentProjectFile
        return [None] + [l.decode() for l in h5['ObjectClassification/LabelNames'][()]]

    def infer(self, input_img: GenericImageDataAccessor, segmentation_img: GenericImageDataAccessor) -> (np.ndarray, dict):
        if self.model_chroma != input_img.chroma:
            raise IlastikInputShapeError(
                f'Model {self} expects {self.model_chroma} input channels but received {input_img.chroma}'
            )
        if self.model_3d != input_img.is_3d():
            if self.model_3d:
                raise IlastikInputShapeError(f'Model is 3D but input image is 2D')
            else:
                raise IlastikInputShapeError(f'Model is 2D but input image is 3D')

        assert segmentation_img.is_mask()
        if isinstance(input_img, PatchStack):
            assert isinstance(segmentation_img, PatchStack)
            tagged_input_data = vigra.taggedView(input_img.pczyx, 'tczyx')
            tagged_seg_data = vigra.taggedView(
                self._make_8bit_mask(segmentation_img.pczyx),
                'tczyx'
            )
        else:
            tagged_input_data = vigra.taggedView(input_img.data, 'yxcz')
            tagged_seg_data = vigra.taggedView(
                self._make_8bit_mask(segmentation_img.data),
                'yxcz'
            )

        dsi = [
            {
                'Raw Data': self.PreloadedArrayDatasetInfo(preloaded_array=tagged_input_data),
                'Segmentation Image': self.PreloadedArrayDatasetInfo(preloaded_array=tagged_seg_data),
            }
        ]

        obmaps = self.shell.workflow.batchProcessingApplet.run_export(dsi, export_to_array=True) # [z x h x w x n]

        assert len(obmaps) == 1, 'ilastik generated more than one object map'


        if isinstance(input_img, PatchStack):
            pyxcz = np.moveaxis(
                obmaps[0],
                [0, 1, 2, 3, 4],
                [0, 4, 1, 2, 3]
            )
            return PatchStack(data=pyxcz)
        else:
            yxcz = np.moveaxis(
                obmaps[0],
                [1, 2, 3, 0],
                [0, 1, 2, 3]
            )
            return InMemoryDataAccessor(data=yxcz)


class IlastikObjectClassifierFromPixelPredictionsModel(IlastikModel, ImageToImageModel):

    @staticmethod
    def get_workflow():
        from ilastik.workflows.objectClassification.objectClassificationWorkflow import ObjectClassificationWorkflowPrediction
        return ObjectClassificationWorkflowPrediction

    @property
    def labels(self):
        h5 = self.shell.projectManager.currentProjectFile
        return [None] + [l.decode() for l in h5['ObjectClassification/LabelNames'][()]]

    def infer(self, input_img: GenericImageDataAccessor, pxmap_img: GenericImageDataAccessor) -> (np.ndarray, dict):
        if self.model_chroma != input_img.chroma:
            raise IlastikInputShapeError(
                f'Model {self} expects {self.model_chroma} input channels but received {input_img.chroma}'
            )
        if self.model_3d != input_img.is_3d():
            if self.model_3d:
                raise IlastikInputShapeError(f'Model is 3D but input image is 2D')
            else:
                raise IlastikInputShapeError(f'Model is 2D but input image is 3D')

        if isinstance(input_img, PatchStack):
            assert isinstance(pxmap_img, PatchStack)
            tagged_input_data = vigra.taggedView(input_img.pczyx, 'tczyx')
            tagged_pxmap_data = vigra.taggedView(pxmap_img.pczyx, 'tczyx')
        else:
            tagged_input_data = vigra.taggedView(input_img.data, 'yxcz')
            tagged_pxmap_data = vigra.taggedView(pxmap_img.data, 'yxcz')

        dsi = [
            {
                'Raw Data': self.PreloadedArrayDatasetInfo(preloaded_array=tagged_input_data),
                'Prediction Maps': self.PreloadedArrayDatasetInfo(preloaded_array=tagged_pxmap_data),
            }
        ]

        obmaps = self.shell.workflow.batchProcessingApplet.run_export(dsi, export_to_array=True) # [z x h x w x n]

        assert len(obmaps) == 1, 'ilastik generated more than one object map'

        if isinstance(input_img, PatchStack):
            pyxcz = np.moveaxis(
                obmaps[0],
                [0, 1, 2, 3, 4],
                [0, 4, 1, 2, 3]
            )
            return PatchStack(data=pyxcz)
        else:
            yxcz = np.moveaxis(
                obmaps[0],
                [1, 2, 3, 0],
                [0, 1, 2, 3]
            )
            return InMemoryDataAccessor(data=yxcz)


    def label_instance_class(self, img: GenericImageDataAccessor, pxmap: GenericImageDataAccessor, **kwargs):
        """
        Given an image and a map of pixel probabilities of the same shape, return a map where each connected object is
        assigned a class
        :param img: input image
        :param pxmap: map of pixel probabilities
        :param kwargs:
            pixel_classification_channel: channel of pxmap used to segment objects
            pixel_classification_thresold: threshold of pxmap used to segment objects
        :return:
        """
        if not img.shape == pxmap.shape:
            raise InvalidInputImageError('Expecting input image and pixel probabilities to be the same shape')
        if not pxmap.data.min() >= 0.0 and pxmap.data.max() <= 1.0:
            raise InvalidInputImageError('Pixel probability values must be between 0.0 and 1.0')
        pxch = kwargs.get('pixel_classification_channel', 0)
        pxtr = kwargs.get('pixel_classification_threshold', 0.5)
        mask = InMemoryDataAccessor(pxmap.get_one_channel_data(pxch).data > pxtr)
        return self.infer(img, mask)


class Error(Exception):
    pass

class IlastikInputEmbedding(Error):
    pass

class IlastikInputShapeError(Error):
    """Raised when an ilastik classifier is asked to infer on data that is incompatible with its input shape"""
    pass