from typing import Dict

from fastapi import APIRouter, HTTPException
from pydantic import Field

from svlt.accessors import GenericImageDataAccessor
from svlt.models import Model
from svlt.pipelines.common import call_pipeline, PipelineTrace, PipelineParams, PipelineRecord

from ..models import IlastikPixelClassifierModel, IlastikObjectClassifierFromPixelPredictionsModel

router = APIRouter(
    prefix='/pipelines',
)

class PxThenObParams(PipelineParams):
    accessor_id: str = Field(description='ID(s) of previously loaded accessor(s) to use as pipeline input')
    px_model_id: str = Field(description='ID of model for pixel classification')
    ob_model_id: str = Field(description='ID of model for object classification')
    channel: int = Field(None, description='Image channel to pass to pixel classification, or all channels if empty.')
    mip: bool = Field(False, description='Use maximum intensity projection of input image if True')


class PxThenObRecord(PipelineRecord):
    pass

@router.put('/pixel_then_object_classification/infer')
def pixel_then_object_classification(p: PxThenObParams) -> PxThenObRecord:
    """
    Workflow that specifically runs an ilastik pixel classifier, then passes results to an object classifier.
    """

    try:
        return call_pipeline(pixel_then_object_classification_pipeline, p)
    except IncompatibleModelsError as e:
        raise HTTPException(status_code=409, detail=str(e))


def pixel_then_object_classification_pipeline(
        accessors: Dict[str, GenericImageDataAccessor],
        models: Dict[str, Model],
        **k
) -> PxThenObRecord:

    if not isinstance(models['px_'], IlastikPixelClassifierModel):
        raise IncompatibleModelsError(
            f'Expecting px_model to be an ilastik pixel classification model'
        )
    if not isinstance(models['ob_'], IlastikObjectClassifierFromPixelPredictionsModel):
        raise IncompatibleModelsError(
            f'Expecting ob_model to be an ilastik object classification from pixel predictions model'
        )

    d = PipelineTrace(accessors[''])
    if (ch := k.get('channel')) is not None:
        channels = [ch]
    else:
        channels = range(0, d['input'].chroma)
    d['select_channels'] = d.last.get_channels(channels, mip=k.get('mip', False))
    d['pxmap'] = models['px_'].infer(d.last)
    d['ob_map'] = models['ob_'].infer(d['select_channels'], d['pxmap'])

    return d

class Error(Exception):
    pass

class IncompatibleModelsError(Error):
    pass