from multiprocessing import Process
from pathlib import Path
import requests
from shutil import copyfile

import uvicorn

host = '127.0.0.1'
port = 5001
px3d_ilp = '3d_model/px3d.ilp'
ob3d_ilp = '3d_model/ob3d.ilp'

czi_root = Path('z:/rhodes/projects/proj0004-marine-photoactivation/data/exp0036')
czis = [
    'A01-selection-test01.czi',
    'B01-selection-test01.czi'
]
channel = 4

if __name__ == '__main__':
    server_process = Process(
        target=uvicorn.run,
        args=('api:app',),
        kwargs={'host': host, 'port': port, 'log_level': 'debug'},
        daemon=True
    )
    uri = f'http://{host}:{port}/'
    server_process.start()

    resp_paths = requests.get(uri + 'paths')
    paths = resp_paths.json()

    def request_put(endpoint, params):
        resp = requests.put(uri + endpoint, params=params)
        assert resp.status_code == 200, resp.content.decode()
        return resp.json()

    # load ilastik pixel model
    model_id_px = request_put(
        'models/ilastik/pixel_classification/load/',
        {'project_file': px3d_ilp}
    )['model_id']

    # load ilastik object model
    model_id_ob = request_put(
        'models/ilastik/object_classification/load/',
        {'project_file': ob3d_ilp}
    )['model_id']

    # copy files from network storage to processing directory
    for ff in czis:
        infile = Path(czi_root) / ff
        outfile = Path(paths['inbound_images']) / ff
        copyfile(infile, outfile)

        # infer pixel maps
        request_put(
            'infer/from_image_file',
            {
                'model_id': model_id_px,
                'input_filename': ff,
                'channel': channel,
            }
        )

    resp_models = requests.get(uri + 'models')

    # raise Exception('x, y axes are currently flipped in pixelmap')

    server_process.terminate()
    print('Finished')