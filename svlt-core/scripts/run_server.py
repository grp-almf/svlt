from svlt.conf.startup import main, parse_args

if __name__ == '__main__':
    args = parse_args()
    print('CLI args:\n' + str(args))
    main(confpath='svlt.api', **args.__dict__)
    print('Finished')

