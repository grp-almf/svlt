from collections import OrderedDict
import json
from pathlib import Path
import re
import shutil

import pandas as pd

from .py3 import HttpClient, NonSuccessResponseError


class FileBatchRunnerClient(HttpClient):
    def __init__(self, conf_json: str, debug=False, max_count=None, resume=False, allow_overwrite=False, **kwargs):
        """
        Iterate a sequence of requests on files of multiple directories
        :param conf_json: path to configuration JSON file
        :param debug: interrupt execution if a non-200 request is received, otherwise continue
        :param resume: skip over existing files and continue job if True, other raise error if an existing file is found
        :param max_count: if specified, only process the specified number of input files
        """
        self.debug = debug
        self.resume = resume
        self.allow_overwrite = allow_overwrite
        self.conf_root = Path(conf_json).parent
        with open(conf_json, 'r') as fh:
            self.conf = json.load(fh)

        self.local_paths = {k: Path(v) for k, v in self.conf['paths']['local'].items()}
        self.remote_paths = {k: Path(v).as_posix() for k, v in self.conf['paths']['remote'].items()}

        for pa in self.local_paths.values():
            pa.mkdir(parents=True, exist_ok=True)

        shutil.copy(conf_json, self.local_paths['output'] / conf_json.name)

        self.stacks = self.get_stacks(max_count=max_count)
        if self.resume and (self.conf_root / 'filelist.csv').exists():
            prev_stacks = pd.read_csv(self.conf_root / 'filelist.csv')[['remote_path', 'all_tasks_complete']]
            dfm = pd.merge(self.stacks.remote_path.apply(str), prev_stacks)
            self.stacks.loc[dfm.index, 'all_tasks_complete'] = dfm['all_tasks_complete']

        self.tasks = {}
        self.write_df()

        if not self.stacks['exists'].all():
            raise FileNotFoundError(f'Trying to process non-existent image files')
        return super().__init__(**kwargs)

    def message(self, message):
        print(message)

    def write_df(self):
        self.stacks.to_csv(self.conf_root / 'filelist.csv')
        self.stacks.to_csv(self.local_paths['output'] / 'filelist_copy.csv')

    def write_tasks_json(self):
        tasks_dict = self.hit('get', 'tasks')
        with open(self.local_paths['output'] / 'tasks.json', 'w') as fh:
            json.dump(tasks_dict, fh)

    def hit(self, method, endpoint, params=None, body=None, catch=True, **kwargs):
        resp = super(FileBatchRunnerClient, self).hit(method, endpoint, params=params, body=body)
        if resp.status_code != 200:
            self.message(f'Non-200 response from {endpoint}:\n{resp.text}')
            if not catch:
                raise NonSuccessResponseError(resp.text)
        return resp.json()

    def restart_session(self):
        self.hit('get', 'session/restart')

    def verify_server(self):
        try:
            # test server communication
            self.get('')
        except Exception as e:
            self.message('Could not find server at: ' + self.uri)
            raise(e)
        self.message('Verified server is online at: ' + self.uri)

    def watch_path(self, key, remote_path, local_path, make=True, verify=False, catch=False):
        if make:
            local_path.mkdir(parents=True, exist_ok=True)

        touch_uuid = self.hit(
            'put',
            f'/paths/watch_{key}',
            params={'path': remote_path.__str__(), 'touch': verify},
            catch=catch
        )
        if verify:
            pa_touch = local_path / 'svlt-pheno.touch'
            try:
                with open(pa_touch, 'r') as fh:
                    cont = fh.read()
                    assert touch_uuid == cont
                pa_touch.unlink()
            except Exception as e:
                raise WatchPathVerificationError(e)
        self.message(f'Watching {remote_path} (remote), {local_path} (local) for {key} data')

    def setup(self, catch=True,):
        self.watch_path(
            'conf',
            self.remote_paths['conf'],
            self.local_paths['conf'],
            verify=False,
            make=False,
            catch=catch,
        )
        self.watch_path(
            'output',
            self.remote_paths['output'],
            self.local_paths['output'],
            verify=False,
            make=True,
            catch=catch,
        )
        self.watch_path(
            'input',
            self.remote_paths['input'],
            self.local_paths['input'],
            verify=False,
            make=False,
            catch=catch,
        )

        for v in self.conf['setup']:
            resp = self.hit(**v, catch=False)
            self.message(f'Completed setup call to ' + v['endpoint'])

    def get_stacks(self, max_count=None):
        paths = []
        for inp in self.conf['inputs']:
            where_local = Path(self.local_paths['input']) / inp['directory']

            # get explicit filenames
            files = inp.get('files', [])

            # get files by pattern
            def _append_files_by_pattern(where, pattern):
                matching_files = []
                if pattern is None or pattern == '':
                    return
                for f in list(where.iterdir()):
                    if f.is_dir():
                        continue
                    if pattern.upper() in f.name.upper() and f.name not in files:
                        matching_files.append(f.name)
                return matching_files

            is_multiposition = inp.get('multiposition', False)
            where_remote = Path(self.remote_paths['input']) / inp['directory']

            # search subdirectories for pattern if so specified
            if inp.get('search_subdirectories'):
                for subdir in where_local.iterdir():
                    if not subdir.is_dir():
                        continue
                    if (sdp := inp.get('subdirectory_pattern')) is not None:
                        if sdp.upper() not in subdir.name.upper():
                            continue
                    matches = _append_files_by_pattern(subdir, inp.get('pattern'))
                    files += [f'{subdir}/{f}' for f in matches]
            else:
                files += _append_files_by_pattern(where_local, inp.get('pattern'))

            def _get_file_info(fpath_str):
                info = {
                    'remote_path': (where_remote / fpath_str).as_posix(),
                    'local_path': where_local / fpath_str,
                    'is_multiposition': is_multiposition,
                }
                filename = Path(fpath_str).name
                if (coord_regex := inp.get('coord_regex')) is not None:
                    for coord_k, coord_v in re.search(coord_regex, filename).groupdict().items():
                        if coord_k.lower() in ['well', 'position', 'time', 'date']:
                            info[f'coord_{coord_k.lower()}'] = int(coord_v)
                        else:
                            info[f'coord_{coord_k.lower()}'] = coord_v
                return info
            paths = paths + [_get_file_info(f) for f in files]
        if max_count is not None:
            df = pd.DataFrame(paths).head(min(max_count, len(paths)))
        else:
            df = pd.DataFrame(paths)
        if len(df) == 0:
            raise EmptyFileListError('No files were found')
        df['exists'] = df['local_path'].apply(lambda x: x.exists())
        df['parent'] = df['local_path'].apply(lambda x: x.parent)
        df['filename'] = df['local_path'].apply(lambda x: x.name)
        df['all_tasks_complete'] = False
        self.message(f'Found {len(df)} input files.')
        return df

    def read_accessor(self, row):
        try:
            if row.is_multiposition:
                return self.put(f'accessors/read_multiposition_file/{row.filename}', query={'lazy': True}).json()
            else:
                return [self.put(f'accessors/read_from_file/{row.filename}', query={'lazy': True}).json()]
        except NonSuccessResponseError:
            return None

    def read_all_accessors(self):
        accessor_ids = []
        for loc_pa, df_gb in self.stacks.groupby('parent'):
            pa_dir = loc_pa.relative_to(self.local_paths['input']).as_posix()
            self.watch_path(
                'input',
                (Path(self.remote_paths['input']) / pa_dir).as_posix(),
                self.local_paths['input'] / pa_dir,
                verify=False,
                make=False
            )
            df_gb['accessor_id'] = df_gb.apply(self.read_accessor, axis=1)
            df_gb['position'] = df_gb['accessor_id'].apply(lambda x: range(0, len(x)))
            df_gb = df_gb.explode(['accessor_id', 'position'])
            accessor_ids.append(df_gb)
        df_concat = pd.concat(accessor_ids)[['local_path', 'accessor_id', 'position']]
        self.stacks = self.stacks.merge(df_concat, on='local_path')
        self.write_df()

    def queue_tasks(self):
        def _task(stack):
            input_acc_id = stack['accessor_id']
            if self.resume and stack.all_tasks_complete:
                self.message(f'Resume mode: skipping stack {input_acc_id} because all of its tasks were marked complete.')
                return
            tasks = self.conf['analyze']
            output_acc_ids = OrderedDict({'input': input_acc_id})
            for v in tasks:
                v['body']['schedule'] = True
                if aim := v.get('accessor_id_mapping'):  # explicitly map inputs to review tasks' outputs
                    for pki, tki in aim.items():
                        v['body'][pki] = output_acc_ids[tki]
                else:  # input is the output of the last task
                    v['body']['accessor_id'] = list(output_acc_ids.values())[-1]
                if cpi := v.get('stack_info_to_param'):
                    v['body'][cpi] = {'stack': stack.name, 'position': stack.position}
                task_id = self.hit(**v, catch=not self.debug)['task_id']
                v['task_id'] = task_id

                # relate each task ID to its starting input accessor ID and task
                self.tasks[v['task_id']] = {
                    'stack_index': pd.Index(self.stacks['accessor_id']).get_loc(input_acc_id),
                    'task_key': v['key'],
                    'complete': False,
                }
                output_acc_ids[v['key']] = self.hit('get', f'tasks/get_output_accessor/{task_id}')
        self.stacks.apply(_task, axis=1)

    def run_tasks(self):
        while task_id := self.hit('get', 'tasks/next'):
            task_info = self.tasks[task_id]
            task_key = task_info['task_key']
            stack_index = task_info['stack_index']
            p = {
                'write_all': True,
                'prefix': f'stack-{stack_index:04d}-task-{task_key}',
                'output_subdirectory': task_key,
                'allow_overwrite': self.allow_overwrite,
            }
            resp = self.hit('put', f'tasks/run/{task_id}', params=p, catch=not self.debug)
            dt = sum(resp['timer'].values())
            self.message(f'Ran task {task_id} in {dt:.3f} sec')

            # track task progress and write to file if an entire stack's tasks are complete
            self.tasks[task_id]['complete'] = True
            df_tasks = pd.DataFrame(self.tasks).T
            if all(df_tasks[df_tasks.stack_index == stack_index].complete):
                self.stacks.loc[stack_index, 'all_tasks_complete'] = True
                self.write_df()
                self.write_tasks_json()

            self.hit('put', f'tasks/delete_accessors')

    def cleanup(self):
        waiting_tasks = self.hit('get', 'tasks/waiting')

        if len(waiting_tasks) > 0:
            raise TasksRemainingError(f'There are tasks that did not run:\n' + str(waiting_tasks.keys()))

    def run(self):
        self.restart_session()
        self.verify_server()
        self.read_all_accessors()
        self.setup()
        self.queue_tasks()
        self.run_tasks()
        self.cleanup()
        self.message('Finished')

class Error(Exception):
    pass

class FileNotFoundError(Error):
    pass

class EmptyFileListError(Error):
    pass

class WatchPathVerificationError(Error):
    pass

class TasksRemainingError(Error):
    pass

class InvalidStackCoordinateKeyError(Error):
    pass