import numpy as np
from matplotlib import font_manager
from PIL import Image, ImageDraw, ImageFont

from .process import rescale

def _get_font(font_size=18):
    return ImageFont.truetype(
        font_manager.findfont(
            font_manager.FontProperties(
                family='sans-serif',
                weight='bold'
            )
        ),
        size=font_size,
    )


def draw_boxes_on_3d_image(roiset, draw_full_depth=False, **kwargs):
    h, w, chroma, nz = roiset.acc_raw.shape
    linewidth = kwargs.get('linewidth', 4)

    if ck := kwargs.get('channel'):
        channels = [ck]
        shape = (h, w, 1, nz)
    else:
        channels = range(0, chroma)
        shape = roiset.acc_raw.shape

    annotated = np.zeros(shape, dtype=roiset.acc_raw.dtype)

    for zi in range(0, nz):
        if draw_full_depth:
            subset = roiset.df().bounding_box
        else:
            subset = roiset.df().bounding_box.query(f'zi == {zi}')
        for ci in range(0, len(channels)):
            pilimg = Image.fromarray(roiset.acc_raw.data[:, :, channels[ci], zi])
            draw = ImageDraw.Draw(pilimg)
            draw.font = _get_font()

            def _draw_boxes(bb):
                xm = round((bb.x0 + bb.x1) / 2)
                draw.rectangle([(bb.x0, bb.y0), (bb.x1, bb.y1)], outline='white', width=linewidth)

                if kwargs.get('draw_label') is True:
                    draw.text((xm, bb.y0), f'{bb.name:04d}', fill='white', anchor='mb')
            subset.apply(_draw_boxes, axis=1)
            annotated[:, :, ci, zi] = pilimg


    if clip := kwargs.get('rescale_clip'):
        assert clip >= 0.0 and clip <= 1.0
        annotated = rescale(annotated, clip=clip)

    return annotated

def draw_box_on_patch(patch, bbox, linewidth=1):
    assert len(patch.shape) == 2
    ((x0, y0), (x1, y1)) = bbox
    pilimg = Image.fromarray(patch)  # drawing modifies array in-place
    draw = ImageDraw.Draw(pilimg)
    draw.rectangle([(x0, y0), (x1, y1)], outline='white', width=linewidth)
    return np.array(pilimg)

def draw_contours_on_patch(patch, contours, linewidth=1):
    assert len(patch.shape) == 2
    pilimg = Image.fromarray(patch)  # drawing modifies array in-place
    draw = ImageDraw.Draw(pilimg)
    for co in contours:
        draw.line([(p[1], p[0]) for p in co], width=linewidth, joint='curve')
    return np.array(pilimg)