import argparse
from multiprocessing import Process
import os
import requests
from requests.adapters import HTTPAdapter
from urllib3 import Retry
import uvicorn
import webbrowser

from svlt.conf.defaults import server_conf, root


def main(confpath, host, port, reload, debug, root) -> None:

    if root:
        os.environ['SVLT_SESSION_ROOT'] = root

    server_process = Process(
        target=uvicorn.run,
        args=(f'{confpath}:app',),
        kwargs={
            'app_dir': '..',
            'host': host,
            'port': int(port),
            'log_level': 'debug',
            'reload': reload,
        },
        daemon=(reload is False),
    )
    url = f'http://{host}:{int(port):04d}/'
    print(url)
    server_process.start()

    try:
        sesh = requests.Session()
        retries = Retry(
            total=10,
            backoff_factor=0.5,
        )
        sesh.mount('http://', HTTPAdapter(max_retries=retries))
        resp = sesh.get(url + 'status')
        assert resp.status_code == 200
    except Exception:
        print('Error starting server')
        server_process.terminate()
        exit()

    webbrowser.open(url + 'status', new=1, autoraise=True)

    if debug:
        print('Running in debug mode')
    print('Type "STOP" to stop server')
    input_str = ''
    while input_str.upper() != 'STOP':
        input_str = input()
    session_path = requests.get(url + 'paths/session').text

    print(f'Terminating server.\nSession data are located in {session_path}')

    server_process.terminate()
    return session_path


def parse_args():
    parser = argparse.ArgumentParser(
        description='Start model server with optional arguments',
    )
    parser.add_argument(
        '--host',
        default=server_conf['host'],
        help='bind socket to this host'
    )
    parser.add_argument(
        '--port',
        default=str(server_conf['port']),
        help='bind socket to this port',
    )
    parser.add_argument(
        '--root',
        default=root.__str__(),
        help='root directory of session data'
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='display extra information that is helpful for debugging'
    )
    parser.add_argument(
        '--reload',
        action='store_true',
        help='automatically restart server when changes are noticed, for development purposes'
    )

    return parser.parse_args()
