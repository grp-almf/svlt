from collections import OrderedDict
import itertools
import logging
import os

import psutil
import uuid

from pathlib import Path, PureWindowsPath
from pydantic import BaseModel
from time import strftime, localtime
from typing import Union

import pandas as pd

from .conf import defaults
from .accessors import GenericImageDataAccessor
from .models import Model

logger = logging.getLogger(__name__)

class CsvTable(object):
    def __init__(self, fpath: Path):
        self.path = fpath
        self.empty = True

    def append(self, coords: dict, data: pd.DataFrame) -> bool:
        assert isinstance(data, pd.DataFrame)
        for c in reversed(coords.keys()):
            data.insert(0, c, coords[c])
        if self.empty:
            data.to_csv(self.path, index=False, mode='w', header=True)
        else:
            data.to_csv(self.path, index=False, mode='a', header=False)
        self.empty = False
        return True


class TaskCollection(object):

    status_codes = {
        'waiting': 'WAITING',
        'ready': 'READY',
        'in_progress': 'IN_PROGRESS',
        'finished': 'FINISHED',
        'failed': 'FAILED',
    }

    def __init__(self):
        self._tasks = OrderedDict()
        self._handles = OrderedDict()

    def add_task(self, func: callable, params: dict, target_info: str = None) -> str:
        task_id = str(uuid.uuid4())

        self._tasks[task_id] = {
            'module': func.__module__,
            'params': params,
            'func_str': str(func),
            'status': self.status_codes['waiting'],
            'error': None,
            'result': None,
            'target': target_info,
            'output_accessor_id': session.reserve_accessor_id(parent_task_id=task_id),
        }

        self._handles[task_id] = func
        logger.info(f'Added task {task_id}: {str(func)}')

        return str(task_id)

    def get_dependent_accessors(self, task_id):
        task = self._tasks[task_id]
        return [v for k, v in dict(task['params']).items() if k.endswith('accessor_id')]

    def get_accessors_for_cleanup(self):
        """
        Return a list of accessor_ids that are no longer a dependency of any unfinished task
        """
        deps_chain = [self.get_dependent_accessors(ti) for ti in self.list_unfinished_tasks().keys()]
        deps = list(set(itertools.chain.from_iterable(deps_chain)))
        return [k for k, v in session.accessor_info.items() if session.is_accessor_loaded(k) and k not in deps]

    @property
    def tasks(self):
        def _prereqs_are_ready(task_id):
            deps = self.get_dependent_accessors(task_id)
            return all(
                [session.is_accessor_loaded(acc_id) or session.accessor_info[acc_id]['lazy'] for acc_id in deps]
            )

        waiting = {tid: task for tid, task in self._tasks.items() if task['status'].upper() == self.status_codes['waiting']}
        for tid, task in waiting.items():
            if _prereqs_are_ready(tid):
                task['status'] = self.status_codes['ready']
        return self._tasks

    def get_task_info(self, task_id: str) -> dict:
        return self.tasks[task_id]

    def get_output_accessor_id(self, task_id: str) -> str:
        return self.tasks[task_id]['output_accessor_id']

    def list_tasks(self, status=None) -> OrderedDict:
        if status is None:
            return self.tasks
        else:
            return {k: v for k, v in self.tasks.items() if v['status'].upper() == status.upper()}

    def list_unfinished_tasks(self):
        return {k: v for k, v in self.tasks.items() if v['status'].upper() not in ['FINISHED', 'FAILED']}

    @property
    def next_waiting(self):
        """
        Return the task_id of the first status waiting for completion, or else None
        """
        for k, v in self.tasks.items():
            if v['status'] == self.status_codes['ready']:
                return k
        return None

    def run_task(
            self,
            task_id: str,
            write_all: bool = False,
            prefix: str = None,
            output_subdirectory: str = None,
            allow_overwrite: bool = False
    ):
        """
        Run a specific task immediately
        :param task_id: identifier of task to run
        :param write_all: if True, automatically write any accessors created by the task
        :param prefix: optional prefix for naming output files
        :param output_subdirectory: optional subdirectory in which to write outputs
        :param allow_overwrite: overwrite an existing file if True, else raise WriteAccessorError
        :return: task output object
        """
        if self.tasks[task_id]['status'] != self.status_codes['ready']:
            raise RunTaskError(f'Task {task_id} is not ready, likely because dependent input accessors are not loaded')
        task = self.tasks[task_id]
        f = self._handles[task_id]
        p = task['params']
        p.calling_task_id = task_id
        if hasattr(p, 'prefix'):
            p.prefix = prefix
        try:
            logger.info(f'Started running task {task_id}')
            task['status'] = self.status_codes['in_progress']
            result = f(p)
            oid = self.get_output_accessor_id(task_id)
            if session.get_accessor(oid, pop=False) is None:
                raise RunTaskError(f'Task {task_id} did not bind data to accessor {oid} as expected')
            if write_all:
                if prefix is not None:
                    filename = prefix + '.tif'
                else:
                    filename = result.output_accessor_id + '.tif'
                if output_subdirectory is not None:
                    output_path = os.path.join(output_subdirectory, filename)
                else:
                    output_path = filename
                session.write_accessor(result.output_accessor_id, pop=False, filename=output_path, allow_overwrite=allow_overwrite)

                if result.interm_accessor_ids is not None:
                    for step_i, step in enumerate(result.interm_accessor_ids.items()):
                        step_id, acc_id = step
                        interm_filename = f'{step_i:02d}-{step_id}/{filename}'
                        if output_subdirectory is not None:
                            interm_filename = os.path.join(output_subdirectory, interm_filename)
                        session.write_accessor(acc_id, filename=interm_filename, pop=False, allow_overwrite=allow_overwrite)
            logger.info(f'Finished running task {task_id}')
            task['status'] = self.status_codes['finished']
            task['result'] = result
            return result
        except Exception as e:
            task['status'] = self.status_codes['failed']
            task['error'] = str(e)
            logger.error(f'Error running task {task_id}: {str(e)}')
            raise e


class _Session(object):
    """
    Singleton class for a server session that persists data between API calls
    """

    log_format = '%(asctime)s - %(levelname)s - %(message)s'

    def __init__(self):
        self.models = {}  # model_id : model object
        self.paths = self.make_paths()
        self.accessor_info = OrderedDict()
        self.accessor_objects = {}
        self.tasks = TaskCollection()
        self.phenobase = None

        self.logfile = self.paths['logs'] / f'session.log'
        logging.basicConfig(filename=self.logfile, level=logging.INFO, force=True, format=self.log_format)

        self.log_info('Initialized session')

    def get_paths(self):
        return self.paths

    def set_data_directory(self, key: str, path: str):
        if not key in self.paths.keys():
            raise InvalidPathError(f'No such path {key}')
        if not Path(path).exists():
            raise InvalidPathError(f'Could not find {path}')
        self.paths[key] = Path(path)

    def is_accessor_loaded(self, accessor_id):
        acc = session.accessor_objects[accessor_id]
        if acc is None:
            return False
        return acc.loaded

    def reserve_accessor_id(self, parent_task_id: str = None):
        idx = len(self.accessor_info)
        accessor_id = f'acc_{idx:06d}'
        self.accessor_info[accessor_id] = {
            'parent_task_id': parent_task_id,
            'lazy': False,
        }
        self.accessor_objects[accessor_id] = None
        self.log_info(f'Reserved accessor ID {accessor_id}')
        return accessor_id

    def bind_accessor(self, acc: GenericImageDataAccessor, accessor_id: str):
        self.accessor_info[accessor_id] = {
            **self.accessor_info[accessor_id],
            'lazy': acc.lazy,
            **acc.info,
        }
        self.accessor_objects[accessor_id] = acc
        self.log_info(f'Bound accessor object to accessor ID {accessor_id}')

    def add_accessor(
            self,
            acc: GenericImageDataAccessor,
            parent_task_id: str = None,
    ) -> str:
        """
        Add an accessor to session context
        :param acc: accessor to add
        :param accessor_id: unique ID, or autogenerate if None
        :param parent_task_id: optional ID of the task that created this accessor
        :return: ID of accessor
        """
        accessor_id = self.reserve_accessor_id(parent_task_id=parent_task_id)
        self.bind_accessor(acc, accessor_id)
        self.log_mem()
        return accessor_id

    def del_accessor(self, accessor_id: str) -> str:
        """
        Remove accessor object but retain its info dictionary
        :param accessor_id: accessor's ID
        :return: ID of accessor
        """
        if accessor_id not in self.accessor_info.keys():
            raise AccessorIdError(f'No accessor with ID {accessor_id} is registered')
        v = self.accessor_info[accessor_id]
        if isinstance(v, dict) and self.is_accessor_loaded(accessor_id) is False:
            logger.warning(f'Accessor {accessor_id} is already deleted')
        else:
            self.accessor_objects[accessor_id] = None
        self.log_info(f'Deleted accessor {accessor_id}')
        self.log_mem()
        return accessor_id

    def list_accessors(self):
        return {k: {**d, 'loaded': self.is_accessor_loaded(k)} for k, d in self.accessor_info.items()}


    def del_all_accessors(self, parent_task_id: str = None) -> list[str]:
        """
        Remove all accessor objects but keep their info in dictionary.  Delete only those accessors that are not
        registered as dependencies in unfinished tasks.
        :param: if specified, only delete accessors associated with this task id
        :return: list of removed accessor IDs
        """
        res = []

        for acc_id in self.tasks.get_accessors_for_cleanup():
            info = self.accessor_info[acc_id]
            if parent_task_id is not None and info['parent_task_id'] is not None:
                if info['parent_task_id'] != parent_task_id:
                    continue
            if self.is_accessor_loaded(acc_id):
                self.accessor_objects[acc_id] = None
                res.append(acc_id)
                self.log_info(f'Deleted accessor {acc_id}')
        self.log_mem()
        return res

    def get_accessor_info(self, acc_id: str) -> dict:
        """
        Get information about a single accessor
        """
        if acc_id not in self.accessor_info.keys():
            raise AccessorIdError(f'No accessor with ID {acc_id} is registered')
        return {**self.accessor_info[acc_id], 'loaded': self.is_accessor_loaded(acc_id)}

    def get_accessor(self, acc_id: str, pop: bool = False) -> GenericImageDataAccessor:
        """
        Return an accessor object
        :param acc_id: accessor's ID
        :param pop: remove object from session accessor registry if True
        :return: accessor object
        """
        if acc_id not in self.accessor_info.keys():
            raise AccessorIdError(f'No accessor with ID {acc_id} is registered')
        acc = self.accessor_objects[acc_id]
        if pop:
            self.del_accessor(acc_id)
        return acc

    def write_accessor(self, acc_id: str, filename: Union[str, None] = None, pop: bool = False, allow_overwrite: bool = False) -> str:
        """
        Write an accessor to file and optionally remove its object from the session
        :param acc_id: accessor's ID
        :param filename: force use of a specific filename, raise InvalidPathError if this already exists
        :param pop: unload accessor from the session if True
        :param allow_overwrite: overwrite an existing file if True, else raise WriteAccessorError
        :return: name of file
        """
        if filename is None:
            fp = self.paths['outbound_images'] / f'{acc_id}.tif'
        else:
            fp = self.paths['outbound_images'] / filename
            if fp.exists() and not allow_overwrite:
                raise InvalidPathError(f'Cannot overwrite file {filename} when writing accessor')
        acc = self.get_accessor(acc_id, pop=pop)

        old_fp = self.accessor_info[acc_id]['filepath']
        if old_fp != '':
            raise WriteAccessorError(
                f'Cannot overwrite accessor that is already written to {old_fp}'
            )

        acc.write(fp)
        self.accessor_info[acc_id]['filepath'] = fp.__str__()
        self.log_info(f'Wrote accessor {acc_id} to {fp.__str__()}')
        return fp.name

    @staticmethod
    def make_paths() -> dict:
        """
        Set paths where images, logs, etc. are located in this session; can set custom session root data directory
        with SVLT_SESSION_ROOT environmental variable
        :return: dictionary of session paths
        """

        root = os.environ.get('SVLT_SESSION_ROOT', defaults.root)
        root_path = Path(root)
        sid = _Session.create_session_id(root_path)
        paths = {'root': root_path, 'session': root_path / sid}
        for pk in ['conf', 'inbound_images', 'outbound_images', 'logs', 'tables']:
            pa = root_path / sid / defaults.subdirectories[pk]
            paths[pk] = pa
            try:
                pa.mkdir(parents=True, exist_ok=True)
            except Exception:
                raise CouldNotCreateDirectory(f'Could not create directory: {pa}')
        return paths

    @staticmethod
    def create_session_id(look_where: Path) -> str:
        """
        Autogenerate a session ID by incrementing from a list of log files.
        """
        yyyymmdd = strftime('%Y%m%d', localtime())
        idx = 0
        while os.path.exists(look_where / f'{yyyymmdd}-{idx:04d}'):
            idx += 1
        return f'{yyyymmdd}-{idx:04d}'

    def get_log_data(self) -> list:
        log = []
        with open(self.logfile, 'r') as fh:
            for line in fh:
                k = ['datatime', 'level', 'message']
                v = line.strip().split(' - ')[0:3]
                log.insert(0, dict(zip(k, v)))
        return log

    def get_mem(self):
        pid = os.getpid()
        ps = psutil.Process(pid)
        rss = ps.memory_info().rss
        return {'pid': pid, 'rss': rss}

    def log_mem(self):
        mem_stats = self.get_mem()
        pid = mem_stats['pid']
        rss = mem_stats['rss']
        logger.info(f'RSS memory of pid={pid}: {rss}')

    def log_info(self, msg):
        logger.info(msg)

    def log_warning(self, msg):
        logger.warning(msg)

    def log_error(self, msg):
        logger.error(msg)

    def load_model(
            self,
            ModelClass: Model,
            key: Union[str, None] = None,
            params: Union[BaseModel, None] = None,
    ) -> dict:
        """
        Load an instance of a given model class and attach to this session's model registry
        :param ModelClass: subclass of Model
        :param key: unique identifier of model, or autogenerate if None
        :param params: optional parameters that are passed to the model's constructor
        :return: model_id of loaded model
        """
        if params:
            mi = ModelClass(**params.dict())
        else:
            mi = ModelClass()
        assert mi.loaded, f'Error loading instance of {ModelClass.__name__}'
        ii = 0

        if key is None:
            def mid(i):
                return f'{mi.name}_{i:02d}'

            while mid(ii) in self.models.keys():
                ii += 1

            key = mid(ii)
        elif key in self.models.keys():
            raise CouldNotInstantiateModelError(f'Model with key {key} already exists.')

        self.models[key] = {
            'object': mi,
            'info': getattr(mi, 'info', None)
        }
        self.log_info(f'Loaded model {key}')
        return key

    def describe_loaded_models(self) -> dict:
        return {
            k: {
                'class': self.models[k]['object'].__class__.__name__,
                'info': self.models[k]['info'],
            }
            for k in self.models.keys()
        }

    def find_param_in_loaded_models(self, key: str, value: str, is_path=False) -> str:
        """
        Returns model_id of first model where key and value match with .params field, or None
        :param is_path: uses platform-independent path comparison if True
        """

        models = self.describe_loaded_models()
        for mid, det in models.items():
            if is_path:
                if PureWindowsPath(det.get('info').get(key)).as_posix() == Path(value).as_posix():
                    return mid
            else:
                if det.get('info').get(key) == value:
                    return mid
        return None

    def restart(self, **kwargs):
        self.__init__()


# create singleton instance
session = _Session()


class Error(Exception):
    pass

class InferenceRecordError(Error):
    pass

class CouldNotInstantiateModelError(Error):
    pass

class AccessorIdError(Error):
    pass

class WriteAccessorError(Error):
    pass

class CouldNotCreateDirectory(Error):
    pass

class CouldNotCreateTable(Error):
    pass

class CouldNotAppendToTable(Error):
    pass

class InvalidPathError(Error):
    pass

class RunTaskError(Error):
    pass