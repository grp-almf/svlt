import importlib

from fastapi import APIRouter

router = APIRouter(
    prefix='/pipelines',
    tags=['pipelines'],
)

for m in ['segment', 'segment_zproj']:
    router.include_router(
        importlib.import_module(
            f'{__package__}.{m}'
        ).router
    )