from fastapi import APIRouter
from typing import Dict, Union

from .segment import SegmentParams, SegmentRecord, segment_pipeline
from .common import call_pipeline, PipelineQueueRecord, PipelineTrace
from ..accessors import GenericImageDataAccessor
from ..models import Model

from pydantic import Field


class SegmentZStackParams(SegmentParams):
    zi: int = Field(None, description='z coordinate to use on input stack; apply MIP if empty')


class SegmentZStackRecord(SegmentRecord):
    pass


router = APIRouter()


@router.put('/segment_zproj')
def segment_zproj(p: SegmentZStackParams) -> Union[SegmentZStackRecord, PipelineQueueRecord]:
    """
    Run a semantic segmentation model to compute a binary mask from a projected input zstack
    """
    return call_pipeline(segment_zproj_pipeline, p)


def segment_zproj_pipeline(
        accessors: Dict[str, GenericImageDataAccessor],
        models: Dict[str, Model],
        **k
) -> PipelineTrace:
    d = PipelineTrace(accessors.get(''))

    if isinstance(k.get('zi'), int):
        assert 0 < k['zi'] < d.last.nz
        d['mip'] = d.last.get_zi(k['zi'])
    else:
        d['mip'] = d.last.get_mip()
    return segment_pipeline({'': d.last}, models, **k)

