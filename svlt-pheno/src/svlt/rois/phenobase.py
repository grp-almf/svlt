import re
from pathlib import Path
from typing import Dict, Union

import pandas as pd
from sklearn.model_selection import train_test_split

from svlt.accessors import PatchStack, GenericImageFileAccessor
from svlt.session import session
from svlt.rois.roiset import RoiSet, RoiSetExportParams

class RoiSetIndex(dict):
    """
    Dictionary that identifies each unique RoiSet in the PhenoBase.
    """
    pass

class PhenoBase(object):
    def __init__(
            self,
            dataframe: pd.DataFrame,
            root: Path,
            info: Dict[str, str],
            roiset_subdir='phenobase',
            roiset_df_paths: list[Path] = None,
    ):
        """
        Create a database containing one or more RoiSets and their associated data
        :param dataframe: DataFrame object containing RoiSet information indexed by a unique RoiSet identifier
        :param root: top-level directory containing a directory with RoiSets
        :param info: about patch subdirectories, filters, etc.; passed to methods that create derivative PhenoBases
        :param roiset_subdir: name of subdirectory that contains serialized RoiSets
        :param roiset_df_paths: list of CSV files that individually define an Roiset
        """
        self.df = dataframe
        self.root = root
        self.info = info
        self.roiset_root_path = root / roiset_subdir
        self.roiset_df_paths = roiset_df_paths

        assert len(self.list_patch_series()) > 0, f'No patch collections found in {self.roiset_root_path}'
        for patch_series in self.list_patch_series():
            patches_path = self.roiset_root_path / patch_series
            self.verify_patches(
                self.df,
                patches_path.name,
                self.info['mask_patch_series'],
                self.roiset_root_path
            )

    def update(self):
        """
        Scan the target directory specified in .read() for additional entries and update accordingly.
        """
        list_df_paths = []
        for pa in (self.roiset_root_path / 'dataframe').iterdir():
            if pa.suffix.upper() == '.CSV' and pa not in self.roiset_df_paths:
                list_df_paths.append(pa)
                self.roiset_df_paths.append(pa)
        new_roisets = self._read_roiset_dfs(list_df_paths, max_stack_count=None, df_stacks=None)

        if len(new_roisets) == 0:
            return

        for patch_series in self.list_patch_series():
            patches_path = self.roiset_root_path / patch_series
            self.verify_patches(
                pd.concat(new_roisets),
                patches_path.name,
                self.info['mask_patch_series'],
                self.roiset_root_path
            )

        self.df = pd.concat([
            self.df,
            *new_roisets,
        ])

    def push(
            self,
            roiset: RoiSet,
            index_dict: RoiSetIndex,
            csv_prefix: str = None,
            export_params: RoiSetExportParams = None,
    ):
        """
        Push an RoiSet onto a PhenoBase based on a unique key
        :param roiset: RoiSet object to be added to PhenoBase
        :param index_dict: dict that uniquely identifies the RoiSet; keys must match existing ones
        :param csv_prefix:  (optional) prefix of newly created CSV file
        :param export_params: (optional) parameters for exporting RoiSet patch series and other products
        """
        # validate that index of RoiSet matches PhenoBase's schema and is unique
        index_str_parts = [f'{k}{v:04d}' for k, v in index_dict.items()]
        if csv_prefix is not None:
            index_str_parts.insert(0, csv_prefix)
        index_str = '-'.join(index_str_parts)
        for k in self.roiset_index:
            if k not in self.roiset_index:
                raise RoiSetIndexError(f'RoiSet index {k} not recognized in existing PhenoBase')
        if all([v in self.df.index.to_frame()[f'coord_{k}'] for k, v in index_dict.items()]):
            raise RoiSetIndexError(f'RoiSet index {index_dict} already exists in PhenoBase')

        # validate that patch sets are specified in RoiSet export parameters before attempting to export them
        if (export_params is None or len(export_params.patches) == 0) and self.list_patch_series() is not None:
            raise MissingPatchSeriesError(f'RoiSet needs to specify patch export parameters: {self.list_patch_series()}')
        missing_patch_series = [
            psk for psk in self.list_patch_series() if psk.split('patches_')[-1] not in export_params.patches.keys()
        ]
        if any(missing_patch_series):
            raise MissingPatchSeriesError(f'Could not find patch series: {missing_patch_series}')

        # export RoiSet products and then re-scan target directory to update PhenoBase
        roiset.run_exports(
            self.roiset_root_path,
            prefix=index_str,
            params=export_params,
        )
        self.update()

    @staticmethod
    def _read_roiset_dfs(paths, max_stack_count, df_stacks=None):
        list_df = []
        for pa in paths[0: max_stack_count]:
            df_i = pd.read_csv(pa, header=[0, 1], index_col=0)
            df_i.index.name = 'label'
            roiset_index = {m[0]: int(m[1]) for m in re.findall(r'-*([a-zA-Z]+)(\d+)', pa.stem)}
            if len(roiset_index) == 0:
                raise RoiSetIndexError(f'No unique RoiSet identifier found in {pa.name}')

            for k, v in roiset_index.items():
                df_i.insert(0, f'coord_{k}', v)

            # transfer metadata from table of stack, if this is specified
            if df_stacks is not None:
                df_i['paths', 'input_file'] = df_stacks['remote_path'].loc[roiset_index]

                # if ROIs are labeled the same in their input stacks, transfer this to ROI table
                if 'category_label' in df_stacks.columns:
                    df_i['annotations', 'category_label'] = df_stacks['category_label'].loc[roiset_index]
                    df_i['annotations', 'category_id'] = df_stacks['category_id'].loc[roiset_index]

            # transfer in object classification results, if relevant
            if 'classifications' in df_i.columns:
                classifications = df_i.classifications.columns
                if len(classifications) > 1 or ('annotations', 'category_label') in df_i.columns:
                    raise AnnotationColumnsError('PhenoBase can only contain one annotation or classification column')
                if len(classifications) == 1:
                    df_i.rename(columns={classifications[0]: 'category_id'}, inplace=True)
                    df_i['classifications', 'category_label'] = df_i['classifications', 'category_id']
            df_i.set_index([f'coord_{k}' for k in roiset_index.keys()] + [df_i.index], inplace=True)
            list_df.append(df_i)
        return list_df

    @classmethod
    def from_roiset(
            cls,
            root: Path,
            roiset: RoiSet,
            index_dict: RoiSetIndex,
            roiset_subdir='phenobase',
            export_params: RoiSetExportParams = None,
    ):
        # serialize RoiSet
        index_str = '-'.join([f'{k}{v:04d}' for k, v in index_dict.items()])
        roiset.run_exports(
            root / roiset_subdir,
            prefix=index_str,
            params=export_params
        )
        return cls.read(root, roiset_subdir)

    @classmethod
    def read(
            cls,
            root: Path,
            roiset_subdir='phenobase',
            mask_patch_series: str = 'tight_patch_masks',
            max_stack_count: Union[int, None] = None,
            input_files_csv: str = None,
    ):
        """
        Automatically read serialized RoiSet from the specified directory and create a PhenoBase.  The resulting table
        parses its (generally multicolumn) index from RoiSet CSV filenames in the /dataframe subdirectory:
            roiset-x000-y001-t002 results in the index {x=0, y=1, t=2}
        and so on.
        :param root: top-level directory containing a directory with RoiSets
        :param roiset_subdir: name of subdirectory that contains serialized RoiSets
        :param mask_patch_series: name of subdirectory containing patch masks needed to deserialize each RoiSet
        :param max_stack_count: read only the specified number of RoiSets, or read all if None
        :param input_files_csv: (optional) the name of a file with supplementary information about RoiSets' input files
        :return: PhenoBase object
        """
        # concat single-stack dfs
        where_dfs = root / roiset_subdir / 'dataframe'
        roiset_df_paths = [pa for pa in where_dfs.iterdir() if pa.suffix.upper() == '.CSV']
        n_df = len(roiset_df_paths)
        assert n_df > 0, f'No patch dataframes found in {where_dfs}'
        if max_stack_count is None:
            max_stack_count = n_df

        if input_files_csv is not None:
            df_stacks = pd.read_csv(root / input_files_csv)
        else:
            df_stacks = None

        df_concat = pd.concat(
                cls._read_roiset_dfs(
                    roiset_df_paths,
                    max_stack_count=max_stack_count,
                    df_stacks=df_stacks,
                )
        )

        return cls(
            df_concat,
            root,
            {'mask_patch_series': mask_patch_series},
            roiset_df_paths=roiset_df_paths,
        )



    def list_patch_series(self):
        return [x.name for x in self.roiset_root_path.iterdir() if x.is_dir() and x.name.startswith('patches_')]

    @property
    def count(self):
        return len(self.df)

    def chroma(self, patch_series: str):
        return self.sample(1).get_raw_patchstack(patch_series).chroma

    def filter(self, filters) -> pd.DataFrame:
        if filters is None:
            self.info['filters'] = None
            return self
        else: # parse filters
            query_str = 'label > 0'
            for k, val in filters.items():
                assert k in ('area', 'diag', 'min_hw', 'diag')
                if val is None:
                    continue
                vmin = val['min']
                vmax = val['max']
                assert vmin >= 0
                query_str = query_str + f' & {k} > {vmin} & {k} < {vmax}'
                info = {'filters': query_str, **self.info}
            return self.__class__(self.df.query(query_str), self.root, info)

    def sample(self, n_max: int):
        if n_max is None or n_max >= self.count:
            return self
        else:
            return self.__class__(self.df.sample(int(n_max)), self.root, self.info)

    def get_rows(self, rows: pd.Index) -> pd.DataFrame:
        return self.__class__(self.df.loc[rows], self.root, self.info)

    def write_df(self):
        return self.df.reset_index(
            col_level=1
        ).to_csv(
            self.root / 'phenobase.csv',
            index=False
        )

    def get_raw_patchstack(self, patch_series: str) -> PatchStack:
        def _get_patch(roi):
            pa = getattr(roi.patches, f'{patch_series}_path')
            fp = self.roiset_root_path / pa
            acc = GenericImageFileAccessor.read(fp)
            return acc.data
        return PatchStack(self.df.apply(_get_patch, axis=1).to_list(), force_ydim_longest=True)

    def get_patch_masks(self) -> PatchStack:
        patch_mask_column = self.info['mask_patch_series'] + '_path'
        def _get_mask(roi):
            fp = self.roiset_root_path / getattr(roi.patches, patch_mask_column)
            return GenericImageFileAccessor.read(fp).data
        return PatchStack(self.df.apply(_get_mask, axis=1).to_list(), force_ydim_longest=True)

    def get_patch_obmaps(self) -> PatchStack:
        if ('classifications', 'category_id') not in self.df.columns:
            raise AnnotationColumnsError(
                'PhenoBase does not have an annotation column with which to generate object maps'
            )
        patch_mask_column = self.info['mask_patch_series'] + '_path'
        def _get_obmap(roi):
            fp = self.roiset_root_path / getattr(roi.patches, patch_mask_column)
            nda = GenericImageFileAccessor.read(fp).data
            return ((nda > 0) * roi.classifications.category_id).astype('uint8')
        return PatchStack(self.df.apply(_get_obmap, axis=1).to_list(), force_ydim_longest=True)

    @staticmethod
    def verify_patches(df, patch_series: str, mask_patch_series: str, root: PatchStack):
        # verify patch files and put paths in column
        all_exist = {}
        for k in [f'{patch_series}_path', f'{mask_patch_series}_path']:
            df['patches', f'{k}_exists'] = df.patches[k].apply(lambda x: (root / x).exists())
            all_exist[k] = df['patches', f'{k}_exists'].all()
        assert all(all_exist), f'Could not verify that all patches in {patch_series} exist'

    @property
    def labels(self):
        if 'classifications' not in self.df.columns:
            return None
        dfcl = self.df.classifications
        if 'category_id' not in dfcl.columns and 'category_label' not in dfcl.columns:
            return None
        labels_dict = {g[0][0]: g[0][1] for g in dfcl.groupby(['category_id', 'category_label'])}
        return [labels_dict.get(i, 'undefined') for i in range(1, 1 + max(labels_dict.keys()))]

    def split(self, test_size=0.3) -> Dict[str, pd.Series]:
        tr_rows, te_rows = train_test_split(self.df.index, test_size=test_size)

        return {
            'train': self.__class__(self.df.loc[tr_rows], self.root, {'split': 'train', **self.info}),
            'test': self.__class__(self.df.loc[te_rows], self.root, {'split': 'test', **self.info})
        }

    @property
    def roiset_index(self) -> list:
        return [n for n in self.df.index.names if n != 'label']

    def list_bounding_boxes(self):
        return self.df.bounding_box.reset_index().to_dict(orient='records')

class _SessionPhenoBase(object):
    def __init__(self):
        """
        Singleton class for a phenobase attached to a server session that persists RoiSet data
        """
        self._phenobase = None

    def add_roiset(
            self,
            roiset: RoiSet,
            index_dict: RoiSetIndex,
            export_params: RoiSetExportParams = None,
    ):
        """
        Add an RoiSet into PhenoBase; initialize PhenoBase if empty
        :param roiset: RoiSet object to be added to PhenoBase
        :param index_dict: dict that uniquely identifies the RoiSet; keys must match existing ones
        :param export_params: (optional) parameters for exporting each RoiSet patch series
        return: dict of added RoiSet in PhenoBase if successful
        """
        if self._phenobase is None:
            self._phenobase = PhenoBase.from_roiset(
                root=session.paths['outbound_images'],
                roiset=roiset,
                index_dict=index_dict,
                export_params=export_params,
            )
        else:
            self._phenobase.push(
                roiset,
                index_dict=index_dict,
                export_params=export_params,
            )
        return index_dict

    def write_phenobase_table(self):
        if self._phenobase is not None:
            return self._phenobase.write_df()

    def list_bounding_boxes(self):
        if self._phenobase is not None:
            return self._phenobase.list_bounding_boxes()
        return []

    @property
    def count(self):
        if self._phenobase is not None:
            return self._phenobase.count
        return 0



session_phenobase = _SessionPhenoBase()


class Error(Exception):
    pass

class InvalidPatchStackError(Error):
    pass

class RoiSetIndexError(Error):
    pass

class AnnotationColumnsError(Error):
    pass

class MissingPatchSeriesError(Error):
    pass
