import numpy as np
import pandas as pd
from skimage.measure import regionprops_table

from svlt.accessors import GenericImageDataAccessor, PatchStack, InMemoryDataAccessor
from svlt.models import InstanceMaskSegmentationModel
from svlt.rois.labels import get_label_ids


class IntensityThresholdInstanceMaskSegmentationModel(InstanceMaskSegmentationModel):
    def __init__(self, tr: float = 0.5):
        """
        Model that labels all objects as class 1 if the intensity in specified channel exceeds a threshold; labels all
        objects as class 1 if threshold = 0.0
        :param tr: threshold in range of 0.0 to 1.0; model handles normalization to full pixel intensity range
        :param channel: channel to use for thresholding
        """
        self.tr = tr
        self.loaded = self.load()
        super().__init__(info={'tr': tr})

    def load(self):
        return True

    def infer(
            self,
            img: GenericImageDataAccessor,
            mask: GenericImageDataAccessor,
            allow_3d: bool = False,
            connect_3d: bool = True,
    ) -> GenericImageDataAccessor:
        if img.chroma != 1:
            raise ShapeMismatchError(
                f'IntensityThresholdInstanceMaskSegmentationModel expects 1 channel but received {img.chroma}'
            )
        if isinstance(img, PatchStack):  # assume one object per patch
            df = img.get_object_df(mask)
            om = np.zeros(mask.shape, 'uint16')
            def _label_patch_class(la):
                om[la] = (mask.iat(la).data > 0) * 1
            df.loc[df['intensity_mean'] > (self.tr * img.dtype_max), 'label'].apply(_label_patch_class)
            return PatchStack(om)
        else:
            labels = get_label_ids(mask)
            df = pd.DataFrame(regionprops_table(
                labels.data_yxz,
                intensity_image=img.data_yxz,
                properties=('label', 'area', 'intensity_mean')
            ))

            om = np.zeros(labels.shape, labels.dtype)

            def _label_object_class(la):
                om[labels.data == la] = 1
            df.loc[df['intensity_mean'] > (self.tr * img.dtype_max), 'label'].apply(_label_object_class)

            return InMemoryDataAccessor(om)


class Error(Exception):
    pass


class ShapeMismatchError(Error):
    pass