import itertools
from math import sqrt
from pathlib import Path

import numpy as np
import pandas as pd

def filter_df(df: pd.DataFrame, filters: dict = {}) -> pd.DataFrame:
    query_str = 'label > 0'  # always true
    if filters is not None:  # parse filters
        for k, val in filters.items():
            assert k in ('area', 'diag', 'min_hw')
            if val is None:
                continue
            vmin = val['min']
            vmax = val['max']
            assert vmin >= 0
            query_str = query_str + f' & {k} > {vmin} & {k} < {vmax}'
    return df.loc[df.bounding_box.query(query_str).index, :]


def filter_df_overlap_bbox(df1: pd.DataFrame, df2: pd.DataFrame = None) -> pd.DataFrame:
    """
    If passed a single DataFrame, return the subset whose bounding boxes overlap in 3D space.  If passed two DataFrames,
    return the subset where a ROI in the first overlaps a ROI in the second.  May return duplicates entries where a ROI
    overlaps with multiple neighbors.
    :param df1: DataFrame with potentially overlapping bounding boxes
    :param df2: (optional) second DataFrame
    :return DataFrame describing subset of overlapping ROIs
        bbox_overlaps_with: index of ROI that overlaps
        bbox_intersec: pixel area of intersecting region
    """

    def _compare(r0, r1):
        olx = (r0.x0 < r1.x1) and (r0.x1 > r1.x0)
        oly = (r0.y0 < r1.y1) and (r0.y1 > r1.y0)
        olz = (r0.zi == r1.zi)
        return olx and oly and olz

    def _intersec(r0, r1):
        return (r0.x1 - r1.x0) * (r0.y1 - r1.y0)

    first = []
    second = []
    intersec = []

    if df2 is not None:
        for pair in itertools.product(df1.index, df2.index):
            if _compare(
                    df1.bounding_box.loc[pair[0]],
                    df2.bounding_box.loc[pair[1]]
            ):
                first.append(pair[0])
                second.append(pair[1])
                intersec.append(
                    _intersec(
                        df1.bounding_box.loc[pair[0]],
                        df2.bounding_box.loc[pair[1]]
                    )
                )
    else:
        for pair in itertools.combinations(df1.index, 2):
            if _compare(
                    df1.bounding_box.loc[pair[0]],
                    df1.bounding_box.loc[pair[1]]
            ):
                first.append(pair[0])
                second.append(pair[1])
                first.append(pair[1])
                second.append(pair[0])
                isc = _intersec(
                    df1.bounding_box.loc[pair[0]],
                    df1.bounding_box.loc[pair[1]]
                )
                intersec.append(isc)
                intersec.append(isc)

    sdf = df1.bounding_box.loc[first]
    sdf.loc[:, 'overlaps_with'] = second
    sdf.loc[:, 'bbox_intersec'] = intersec
    return sdf


def filter_df_overlap_seg(df1: pd.DataFrame, df2: pd.DataFrame = None) -> pd.DataFrame:
    """
    If passed a single DataFrame, return the subset whose segmentations overlap in 3D space.  If passed two DataFrames,
    return the subset where a ROI in the first overlaps a ROI in the second.  May return duplicates entries where a ROI
    overlaps with multiple neighbors.
    :param df1: DataFrame with potentially overlapping bounding boxes
    :param df2: (optional) second DataFrame
    :return DataFrame describing subset of overlapping ROIs
        seg_overlaps_with: index of ROI that overlaps
        seg_intersec: pixel area of intersecting region
        seg_iou: intersection over union
    """

    dfbb = filter_df_overlap_bbox(df1, df2)

    def _overlap_seg(r):
        roi1 = df1.loc[r.name]
        if df2 is not None:
            roi2 = df2.loc[r.overlaps_with]
        else:
            roi2 = df1.loc[r.overlaps_with]
        bb1 = roi1.bounding_box
        bb2 = roi2.bounding_box
        ex0 = min(bb1.x0, bb2.x0, bb1.x1, bb2.x1)
        ew = max(bb1.x0, bb2.x0, bb1.x1, bb2.x1) - ex0
        ey0 = min(bb1.y0, bb2.y0, bb1.y1, bb2.y1)
        eh = max(bb1.y0, bb2.y0, bb1.y1, bb2.y1) - ey0
        emask = np.zeros((eh, ew), dtype='uint8')
        sl1 = np.s_[(bb1.y0 - ey0): (bb1.y1 - ey0), (bb1.x0 - ex0): (bb1.x1 - ex0)]
        sl2 = np.s_[(bb2.y0 - ey0): (bb2.y1 - ey0), (bb2.x0 - ex0): (bb2.x1 - ex0)]
        emask[sl1] = roi1.masks.binary_mask
        emask[sl2] = emask[sl2] + roi2.masks.binary_mask
        return emask

    emasks = dfbb.apply(_overlap_seg, axis=1)
    dfbb['seg_overlaps'] = emasks.apply(lambda x: np.any(x > 1))
    dfbb['seg_intersec'] = emasks.apply(lambda x: (x == 2).sum())
    dfbb['seg_iou'] = emasks.apply(lambda x: (x == 2).sum() / (x > 0).sum())
    return dfbb


def is_df_3d(df: pd.DataFrame) -> bool:
    return 'z0' in df.bounding_box.columns and 'z1' in df.bounding_box.columns


def insert_level(df: pd.DataFrame, name: str):
    df.columns = pd.MultiIndex.from_product(
        [
            [name],
            list(df.columns.values),
        ],
    )


def read_roiset_df(csv_path: Path) -> pd.DataFrame:
    return pd.read_csv(csv_path, header=[0, 1], index_col=0)


def df_insert_slices(df: pd.DataFrame, sd: dict, expand_box_by) -> pd.DataFrame:
    h = sd['Y']
    w = sd['X']
    nz = sd['Z']

    bb = 'bounding_box'
    df[bb, 'h'] = df[bb, 'y1'] - df[bb, 'y0']
    df[bb, 'w'] = df[bb, 'x1'] - df[bb, 'x0']
    df[bb, 'diag'] = (df[bb, 'w'] ** 2 + df[bb, 'h'] ** 2).apply(sqrt)
    df[bb, 'min_hw'] = df[[[bb, 'w'], [bb, 'h']]].min(axis=1)

    ebxy, ebz = expand_box_by
    ebb = 'expanded_bounding_box'
    df[ebb, 'ebb_y0'] = (df[bb, 'y0'] - ebxy).apply(lambda x: max(x, 0))
    df[ebb, 'ebb_y1'] = (df[bb, 'y1'] + ebxy).apply(lambda x: min(x, h))
    df[ebb, 'ebb_x0'] = (df[bb, 'x0'] - ebxy).apply(lambda x: max(x, 0))
    df[ebb, 'ebb_x1'] = (df[bb, 'x1'] + ebxy).apply(lambda x: min(x, w))

    # handle based on whether bounding box coordinates are 2d or 3d
    if is_df_3d(df):
        df[ebb, 'ebb_z0'] = (df[bb, 'z0'] - ebz).apply(lambda x: max(x, 0))
        df[ebb, 'ebb_z1'] = (df[bb, 'z1'] + ebz).apply(lambda x: max(x, nz))
    else:
        if 'zi' not in df.bounding_box.columns:
            df[bb, 'zi'] = 0
        df[ebb, 'ebb_z0'] = (df[bb, 'zi'] - ebz).apply(lambda x: max(x, 0))
        df[ebb, 'ebb_z1'] = (df[bb, 'zi'] + ebz).apply(lambda x: min(x, nz))

    df[ebb, 'ebb_h'] = df[ebb, 'ebb_y1'] - df[ebb, 'ebb_y0']
    df[ebb, 'ebb_w'] = df[ebb, 'ebb_x1'] - df[ebb, 'ebb_x0']
    df[ebb, 'ebb_nz'] = df[ebb, 'ebb_z1'] - df[ebb, 'ebb_z0'] + 1

    # compute relative bounding boxes
    rbb = 'relative_bounding_box'
    df[rbb, 'rel_y0'] = df[bb, 'y0'] - df[bb, 'y0']
    df[rbb, 'rel_y1'] = df[bb, 'y1'] - df[bb, 'y0']
    df[rbb, 'rel_x0'] = df[bb, 'x0'] - df[bb, 'x0']
    df[rbb, 'rel_x1'] = df[bb, 'x1'] - df[bb, 'x0']

    assert np.all(df[rbb, 'rel_x1'] <= (df[ebb, 'ebb_x1'] - df[ebb, 'ebb_x0']))
    assert np.all(df[rbb, 'rel_y1'] <= (df[ebb, 'ebb_y1'] - df[ebb, 'ebb_y0']))

    if is_df_3d(df):
        df['slices', 'slice'] = df['bounding_box'].apply(
            lambda r:
            np.s_[int(r.y0): int(r.y1), int(r.x0): int(r.x1), :, int(r.z0): int(r.z1)],
            axis=1,
            result_type='reduce',
        )
    else:
        df['slices', 'slice'] = df['bounding_box'].apply(
            lambda r:
            np.s_[int(r.y0): int(r.y1), int(r.x0): int(r.x1), :, int(r.zi): int(r.zi + 1)],
            axis=1,
            result_type='reduce',
        )
    df['slices', 'expanded_slice'] = df['expanded_bounding_box'].apply(
        lambda r:
        np.s_[int(r.ebb_y0): int(r.ebb_y1), int(r.ebb_x0): int(r.ebb_x1), :, int(r.ebb_z0): int(r.ebb_z1) + 1],
        axis=1,
        result_type='reduce',
    )
    df['slices', 'relative_slice'] = df['relative_bounding_box'].apply(
        lambda r:
        np.s_[int(r.rel_y0): int(r.rel_y1), int(r.rel_x0): int(r.rel_x1), :, :],
        axis=1,
        result_type='reduce',
    )
    return df
