from pathlib import Path

import numpy as np
import pandas as pd

from svlt.models import InstanceMaskSegmentationModel
from svlt.process import mask_largest_object
from svlt.rois.roiset import RoiSetExportParams, RoiSet


class RoiSetWithDerivedChannelsExportParams(RoiSetExportParams):
    derived_channels: bool = False


class RoiSetWithDerivedChannels(RoiSet):

    def __init__(self, *a, **k):
        self.accs_derived = []
        super().__init__(*a, **k)

    def classify_by(
            self, name: str, channels: list[int],
            object_classification_model: InstanceMaskSegmentationModel,
            derived_channel_functions: list[callable] = None
    ):
        """
        Insert a column in RoiSet data table that associates each ROI with an integer class, determined by passing
        specified inputs through an instance segmentation classifier.  Derive additional inputs for object
        classification by passing a raw input channel through one or more functions.

        :param name: name of column to insert
        :param channels: list of nc raw input channels to send to classifier
        :param object_classification_model: InstanceSegmentation model object
        :param derived_channel_functions: list of functions that each receive a PatchStack accessor with nc channels and
            that return a single-channel PatchStack accessor of the same shape
        :return: None
        """

        acc_in = self.get_patches_acc(channels=channels, expanded=False, pad_to=None)
        if derived_channel_functions is not None:
            for fcn in derived_channel_functions:
                der = fcn(acc_in) # returns patch stack
                self.accs_derived.append(der)

            # combine channels
            acc_app = acc_in
            for acc_der in self.accs_derived:
                acc_app = acc_app.append_channels(acc_der)

        else:
            acc_app = acc_in

        # do this on a patch basis, i.e. only one object per frame
        obmap_patches = object_classification_model.label_patch_stack(
            acc_app,
            self.get_patch_masks_acc(expanded=False, pad_to=None)
        )

        self._df['classify_by_' + name] = pd.Series(dtype='Int16')

        se = pd.Series(dtype='Int64', index=self._df.index)

        for i, la in enumerate(self._df.index):
            oc = np.unique(
                mask_largest_object(
                    obmap_patches.iat(i).data_yxz
                )
            )[-1]
            se[la] = oc
        self.set_classification(name, se)

    def run_exports(self, where: Path, prefix, params: RoiSetWithDerivedChannelsExportParams) -> dict:
        """
        Export various representations of ROIs, e.g. patches, annotated stacks, and object maps.
        :param where: path of directory in which to write all export products
        :param prefix: prefix of the name of each product's file or subfolder
        :param params: RoiSetExportParams object describing which products to export and with which parameters
        :return: nested dict of Path objects describing the location of export products
        """
        record = super().run_exports(where, prefix, params)

        k = 'derived_channels'
        if k in params.dict().keys():
            record[k] = []
            for di, dacc in enumerate(self.accs_derived):
                fp = where / k / f'dc{di:01d}.tif'
                fp.parent.mkdir(exist_ok=True, parents=True)
                dacc.export_pyxcz(fp)
                record[k].append(str(fp))
        return record
