from typing import Dict, Union

from fastapi import APIRouter
from pydantic import BaseModel, Field

from svlt.accessors import GenericImageDataAccessor
from svlt.pipelines.segment_zproj import segment_zproj_pipeline
from svlt.pipelines.common import call_pipeline
from svlt.rois.roiset import RoiSet, RoiSetMetaParams, RoiSetExportParams
from svlt.rois.labels import get_label_ids

from svlt.pipelines.common import PipelineQueueRecord, PipelineTrace, PipelineParams, PipelineRecord

from svlt.models import Model, InstanceMaskSegmentationModel

class RoiSetObjectMapParams(PipelineParams):
    class _SegmentationParams(BaseModel):
        channel: int = Field(
            None,
            description='Channel of input image to use for solving segmentation; use all channels if empty'
        )
        zi: Union[int, None] = Field(
            None,
            description='z coordinate to use on input image when solving segmentation; apply MIP if empty',
        )

    accessor_id: str = Field(
        description='ID(s) of previously loaded accessor(s) to use as pipeline input'
    )
    pixel_classifier_segmentation_model_id: str = Field(
        description='Pixel classifier applied to segmentation_channel(s) to segment objects'
    )
    object_classifier_model_id: Union[str, None] = Field(
        None,
        description='Object classifier used to classify segmented objects'
    )
    patches_channel: int = Field(
        description='Channel of input image used in patches sent to object classifier'
    )
    segmentation: _SegmentationParams = Field(
        _SegmentationParams(),
        description='Parameters used to solve segmentation'
    )
    roi_params: RoiSetMetaParams = RoiSetMetaParams(**{
        'mask_type': 'boxes',
        'filters': {
            'area': {'min': 1e3, 'max': 1e8}
        },
        'expand_box_by': [128, 2],
        'deproject_channel': None,
    })
    export_params: RoiSetExportParams = RoiSetExportParams()

    export_label_interm: bool = False


class RoiSetToObjectMapRecord(PipelineRecord):
    pass


router = APIRouter()


@router.put('/roiset_to_obmap')
def roiset_object_map(p: RoiSetObjectMapParams) -> Union[RoiSetToObjectMapRecord, PipelineQueueRecord]:
    """
    Compute a RoiSet from 2d segmentation, apply to z-stack, and optionally apply object classification.
    """
    return call_pipeline(roiset_object_map_pipeline, p)


def roiset_object_map_pipeline(
        accessors: Dict[str, GenericImageDataAccessor],
        models: Dict[str, Model],
        **k
) -> PipelineTrace:
    d = PipelineTrace(accessors[''])

    d['mask'] = segment_zproj_pipeline(
        accessors,
        {'': models['pixel_classifier_segmentation_']},
        **k['segmentation'],
    ).last

    d['labeled'] = get_label_ids(d.last)
    rois = RoiSet.from_object_ids(d['input'], d['labeled'], RoiSetMetaParams(**k['roi_params']))

    # optionally append RoiSet products
    for ki, vi in rois.get_export_product_accessors(RoiSetExportParams(**k['export_params'])).items():
        d[ki] = vi

    # optionally run an object classifier if specified
    if obmod := models.get('object_classifier_'):
        obmod_name = k['object_classifier_model_id']
        assert isinstance(obmod, InstanceMaskSegmentationModel)
        rois.classify_by(
            obmod_name,
            [k['patches_channel']],
            obmod,
        )
        d[obmod_name] = rois.get_object_class_map(obmod_name)
    else:
        d['objects_unclassified'] = d.last.apply(lambda x: ((x > 0) * 1).astype('uint16'))
    return d
