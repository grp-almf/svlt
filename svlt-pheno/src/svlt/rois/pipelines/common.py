from typing import Dict, Union

from ..phenobase import session_phenobase
from svlt.pipelines.common import PipelineParams, PipelineRecord, PipelineQueueRecord, call_pipeline
from svlt.rois.roiset import RoiSetExportParams, PatchParams


class RoiSetPipelineParams(PipelineParams):
    exports: RoiSetExportParams = RoiSetExportParams()
    roiset_index: Dict[str, int]
    patches: Dict[str, PatchParams] = {}


def call_roiset_pipeline(func, p: RoiSetPipelineParams) -> Union[PipelineRecord, PipelineQueueRecord]:
    """
    Wraps pipelines.shared.call_pipeline for pipeline functions that return an RoiSet in addition to PipelineTrace.
    Upon execution, add the returned RoiSet to session PhenoBase and otherwise delegate to call_pipeline
    :param func: pipeline function with the signature:
        func(accessors: Dict[str, GenericImageDataAccessor], models: Dict[str, Model], **k) -> (PipelineTrace, RoiSet)
    :param p: pipeline parameters object, which must contain a unique index for the RoiSet
    :return:
        record objects describing either the results of pipeline execution or the queued task
    """
    def outer(*args, **kwargs):
        trace, rois = func(*args, **kwargs)
        session_phenobase.add_roiset(
            roiset=rois,
            index_dict=p.roiset_index,
            export_params=p.exports,
        )
        session_phenobase.write_phenobase_table()
        return trace
    return call_pipeline(outer, p)
