import importlib

from svlt.api import app

for ex in ['rois']:
    m = importlib.import_module(f'svlt.{ex}.router', package=__package__)
    app.include_router(m.router)
