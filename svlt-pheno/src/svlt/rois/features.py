import pandas as pd
from skimage.measure import regionprops_table

from .roiset import RoiSet

def regionprops(rois: RoiSet, make_3d: bool = False, channel: int = None) -> pd.DataFrame:
    props = [
        'area',
        'area_bbox',
        'area_convex',
        'area_filled',
        'axis_major_length',
        'axis_minor_length',
        'equivalent_diameter_area',
        'euler_number',
        'extent',
        'intensity_max',
        'intensity_mean',
        'intensity_min',
        'intensity_std',
        'num_pixels',
        'solidity',
    ]
    if not make_3d:
        props = props + [
            'eccentricity',
            'feret_diameter_max',
            'orientation',
            'perimeter',
            'perimeter_crofton',
        ]

    acc_la = rois.get_patch_obmap_acc(make_3d=make_3d)
    acc_in = rois.get_patches_acc(make_3d=make_3d, channels=[channel])

    def _extract_features(roi):
        i = roi['patches', 'index']
        if make_3d:
            la_i = acc_la.iat(i).data_yxz
            im_i = acc_in.iat(i).data_yxzc
        else:
            la_i = acc_la.iat(i).data_yx
            im_i = acc_in.iat(i).data_yxzc[:, :, 0, :]
        return pd.Series(
            {k: v[0] for k, v in regionprops_table(la_i, im_i, properties=props).items()}
        )

    dff = rois.df().apply(_extract_features, axis=1)
    if channel is not None:
        dff.rename(
            columns={
                f'intensity_{k}-0': f'intensity_{k}-{channel}' for k in ['max', 'min', 'mean', 'std']
            },
            inplace=True
        )
    return dff