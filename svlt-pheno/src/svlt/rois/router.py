import importlib
from typing import Union

from fastapi import APIRouter
from pydantic import BaseModel, Field

from svlt.session import session
from .phenobase import session_phenobase
from .models import IntensityThresholdInstanceMaskSegmentationModel

router = APIRouter(
    prefix='/pheno',
    tags=['pheno'],
)

router.include_router(
    importlib.import_module(
        f'{__package__}.pipelines.router'
    ).router
)

@router.get('/phenobase/bounding_box')
def get_phenobase_bounding_boxes() -> list:
    if session_phenobase is None:
        return []
    return session_phenobase.list_bounding_boxes()

class BinaryThresholdSegmentationParams(BaseModel):
    tr: Union[int, float] = Field(0.5, description='Threshold for binary segmentation')

@router.put('/models/classify/threshold/load')
def load_intensity_threshold_instance_segmentation_model(p: BinaryThresholdSegmentationParams, model_id=None) -> dict:
    result = session.load_model(IntensityThresholdInstanceMaskSegmentationModel, key=model_id, params=p)
    session.log_info(f'Loaded permissive instance segmentation model {result}')
    return {'model_id': result}