from pathlib import Path
import unittest

import numpy as np

from svlt.rois.roiset import RoiSetMetaParams
from svlt.rois.derived import RoiSetWithDerivedChannelsExportParams, RoiSetWithDerivedChannels
from svlt.accessors import generate_file_accessor, PatchStack
import svlt.conf.testing as conf
from svlt.conf.testing import DummyInstanceMaskSegmentationModel

data = conf.meta['image_files']
params = conf.meta['roiset']
output_path = conf.meta['output_path']

class TestDerivedChannels(unittest.TestCase):
    def setUp(self) -> None:
        self.stack = generate_file_accessor(data['multichannel_zstack_raw']['path'])
        self.stack_ch_pa = self.stack.get_mono(params['patches_channel'])
        self.seg_mask = generate_file_accessor(data['multichannel_zstack_mask2d']['path'])

    def test_classify_by_with_derived_channel(self):
        class ModelWithDerivedInputs(DummyInstanceMaskSegmentationModel):
            def infer(self, img, mask):
                return PatchStack(super().infer(img, mask).data * img.chroma)

        roiset = RoiSetWithDerivedChannels.from_binary_mask(
            self.stack,
            self.seg_mask,
            params=RoiSetMetaParams(
                filters={'area': {'min': 1e3, 'max': 1e4}},
                deproject_channel=0,
            )
        )
        self.assertIsInstance(roiset, RoiSetWithDerivedChannels)
        roiset.classify_by(
            'multiple_input_model',
            [0, 1],
            ModelWithDerivedInputs(),
            derived_channel_functions=[
                lambda acc: acc.apply(lambda x: 2 * x),
                lambda acc: PatchStack((0.5 * acc.get_channels([1]).data).astype('uint16'))
            ]
        )
        self.assertGreater(roiset.accs_derived[0].chroma, 1)
        self.assertTrue(all(roiset.df()['classifications', 'multiple_input_model'].unique() == [5]))
        self.assertTrue(all(np.unique(roiset.get_object_class_map('multiple_input_model').data) == [0, 5]))

        self.assertEqual(len(roiset.accs_derived), 2)
        for di in roiset.accs_derived:
            self.assertEqual(roiset.get_patches_acc().hw, di.hw)
            self.assertEqual(roiset.get_patches_acc().nz, di.nz)
            self.assertEqual(roiset.get_patches_acc().count, di.count)

        dpas = roiset.run_exports(
            output_path / 'derived_channels', 'der',
            RoiSetWithDerivedChannelsExportParams(derived_channels=True)
        )
        for fp in dpas['derived_channels']:
            assert Path(fp).exists()
        return roiset