from pathlib import Path
from shutil import copyfile
import unittest

import numpy as np

from svlt.accessors import generate_file_accessor
import svlt.conf.testing as conf
from svlt.rois.pipelines.roiset_obmap import RoiSetObjectMapParams, roiset_object_map_pipeline

data = conf.meta['image_files']
output_path = conf.meta['output_path']
test_params = conf.meta['roiset']


class BaseTestRoiSetMonoProducts(object):

    @property
    def fpi(self):
        return data['multichannel_zstack_raw']['path'].__str__()

    @property
    def stack(self):
        return generate_file_accessor(self.fpi)

    @property
    def stack_ch_pa(self):
        return self.stack.get_mono(test_params['patches_channel'])

    @property
    def seg_mask(self):
        return generate_file_accessor(data['multichannel_zstack_mask2d']['path'])

    def _get_export_params(self):
        return {
            'patches': {
                '2d_annotated': {
                    'draw_bounding_box': True,
                    'rgb_overlay_channels': [3, None, None],
                    'rgb_overlay_weights': [0.2, 1.0, 1.0],
                    'pad_to': 512,
                },
                '2d': {
                    'draw_bounding_box': False,
                    'draw_mask': False,
                },
            },
            'annotated_zstacks': None,
            'object_classes': True,
        }

    def _get_roi_params(self):
        return {
            'mask_type': 'boxes',
            'filters': {
                'area': {'min': 1e2, 'max': 1e8}
            },
            'expand_box_by': [128, 2],
            'deproject_channel': 0,
        }

    def _get_models(self):
        from svlt.models import BinaryThresholdSegmentationModel
        from svlt.rois.models import IntensityThresholdInstanceMaskSegmentationModel
        return {
            'pixel_classifier_segmentation': {
                'name': 'min_px_mod',
                'model': BinaryThresholdSegmentationModel(tr=0.2),
            },
            'object_classifier': {
                'name': 'min_ob_mod',
                'model': IntensityThresholdInstanceMaskSegmentationModel(),
            },
        }


class TestRoiSetWorkflow(BaseTestRoiSetMonoProducts, unittest.TestCase):

    def _pipeline_params(self):
        return {
            'api': False,
            'accessor_id': 'acc_id',
            'pixel_classifier_segmentation_model_id': 'px_id',
            'object_classifier_model_id': 'ob_id',
            'segmentation': {
                'channel': 0,
                'smooth': 1.5,
                'binary_closing': True,
            },
            'patches_channel': 1,
            'roi_params': self._get_roi_params(),
            'export_params': self._get_export_params(),
        }

    def test_object_map_workflow(self):
        n_rois = 4
        acc_in = generate_file_accessor(self.fpi)
        params = RoiSetObjectMapParams(
            **self._pipeline_params(),
        )
        trace = roiset_object_map_pipeline(
            {'': acc_in},
            {f'{k}_': v['model'] for k, v in self._get_models().items()},
            **params.dict()
        )
        self.assertEqual(trace.pop('patches_2d_annotated').count, n_rois)
        self.assertEqual(trace.pop('patches_2d').count, n_rois)
        trace.write_interm(Path(output_path) / 'trace', 'roiset_worfklow_trace', skip_first=False, skip_last=False)
        self.assertTrue('ob_id' in trace.keys())
        self.assertEqual(len(trace['ob_id'].unique()[0]), 2)


class TestRoiSetWorkflowOverApi(conf.TestServerBaseClass, BaseTestRoiSetMonoProducts):

    input_data = data['multichannel_zstack_raw']


    def setUp(self) -> None:
        self.where_out = output_path / 'roiset'
        self.where_out.mkdir(parents=True, exist_ok=True)
        return conf.TestServerBaseClass.setUp(self)

    def test_load_input_accessor(self):
        fname = self.copy_input_file_to_server()
        return self.assertPutSuccess(f'accessors/read_from_file/{fname}')

    def test_load_pixel_classifier(self):
        mid = self.assertPutSuccess(
            'models/seg/threshold/load/',
            query={'tr': 0.2},
        )['model_id']
        self.assertTrue(mid.startswith('BinaryThresholdSegmentationModel'))
        return mid

    def test_load_object_classifier(self):
        mid = self.assertPutSuccess(
            'pheno/models/classify/threshold/load/',
            body={'tr': 0}
        )['model_id']
        self.assertTrue(mid.startswith('IntensityThresholdInstanceMaskSegmentation'))
        return mid

    def _object_map_workflow(self, ob_classifer_id):
        return self.assertPutSuccess(
            'pheno/pipelines/roiset_to_obmap',
            body={
                'accessor_id': self.test_load_input_accessor(),
                'pixel_classifier_segmentation_model_id': self.test_load_pixel_classifier(),
                'object_classifier_model_id': ob_classifer_id,
                'segmentation': {'channel': 0},
                'patches_channel': 1,
                'roi_params': self._get_roi_params(),
                'export_params': self._get_export_params(),
            },
        )

    def test_workflow_with_object_classifier(self):
        obmod_id = self.test_load_object_classifier()
        res = self._object_map_workflow(obmod_id)
        acc_obmap = self.get_accessor(res['output_accessor_id'])
        self.assertTrue(np.all(acc_obmap.unique()[0] == [0, 1]))


    def test_workflow_without_object_classifier(self):
        res = self._object_map_workflow(None)
        acc_obmap = self.get_accessor(res['output_accessor_id'])
        self.assertTrue(np.all(acc_obmap.unique()[0] == [0, 1]))


class TestTaskQueuedRoiSetWorkflowOverApi(TestRoiSetWorkflowOverApi):
    def _object_map_workflow(self, ob_classifer_id):

        res_queue = self.assertPutSuccess(
            'pheno/pipelines/roiset_to_obmap',
            body={
                'schedule': True,
                'accessor_id': self.test_load_input_accessor(),
                'pixel_classifier_segmentation_model_id': self.test_load_pixel_classifier(),
                'object_classifier_model_id': ob_classifer_id,
                'segmentation': {'channel': 0},
                'patches_channel': 1,
                'roi_params': self._get_roi_params(),
                'export_params': self._get_export_params(),
            }
        )

        # check that task in enqueued
        task_id = res_queue['task_id']
        task_info = self.assertGetSuccess(f'tasks/get/{task_id}')
        self.assertEqual(task_info['status'], 'READY')

        # run the task
        res_run = self.assertPutSuccess(
            f'tasks/run/{task_id}'
        )
        self.assertTrue(res_run)
        self.assertEqual(self.assertGetSuccess(f'tasks/get/{task_id}')['status'], 'FINISHED')

        return res_run

class TestAddRoiSetOverApi(conf.TestServerBaseClass):
    def test_add_roiset(self):
        pa_stack = data['multichannel_zstack_raw']['path']
        pa_mask = data['multichannel_zstack_mask2d']['path']
        where_in = Path(self.assertGetSuccess('paths')['inbound_images'])
        copyfile(pa_stack, where_in / pa_stack.name)
        copyfile(pa_mask, where_in / pa_mask.name)

        acc_id_stack = self.assertPutSuccess(f'accessors/read_from_file/{pa_stack.name}')
        acc_id_mask = self.assertPutSuccess(f'accessors/read_from_file/{pa_mask.name}')

        res = self.assertPutSuccess(
            'pheno/pipelines/add_roiset',
            body={
                'accessor_id': acc_id_stack,
                'labels_accessor_id': acc_id_mask,  # binary mask, not labels, so there's only on ROI
                'roiset_index': {'X': 0, 'T': 0},
                'exports': {'patches': {'channel_zero': {'white_channel': 0}}},
                'roi_params': {'deproject_channel': 0}
            }
        )

        # mask patch stack has one position
        acc_id_out = res['output_accessor_id']
        sd = self.assertGetSuccess(f'accessors/get/{acc_id_out}')['shape_dict']
        self.assertEqual(sd['P'], 1)

        # RoiSet has one entry
        phenobase = self.assertGetSuccess('pheno/phenobase/bounding_box')
        self.assertEqual(len(phenobase), 1)