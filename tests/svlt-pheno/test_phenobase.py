import math
import shutil
import unittest

import numpy as np

from svlt.accessors import generate_file_accessor, InMemoryDataAccessor
from svlt.conf import testing as conf

from svlt.rois.models import IntensityThresholdInstanceMaskSegmentationModel
from svlt.rois.phenobase import MissingPatchSeriesError, PhenoBase, RoiSetIndexError, session_phenobase
from svlt.rois.roiset import RoiSet, PatchParams, RoiSetExportParams, RoiSetMetaParams

data = conf.meta['image_files']
output_path = conf.meta['output_path']
params = conf.meta['roiset']


class TestPhenoBase(unittest.TestCase):

    def setUp(self, **kwargs):
        def _split_in_quarters(acc):
            sd = acc.shape_dict
            self.assertEqual(list(sd.keys()), ['Y', 'X', 'C', 'Z'])
            h = math.floor(0.5 * sd['Y'])
            w = math.floor(0.5 * sd['X'])
            nda = acc.data
            return [
                InMemoryDataAccessor(a) for a in [
                    nda[0: h, 0:w, :, :],
                    nda[0:h, w: 2 * w, :, :],
                    nda[h: 2*h, 0:w, :, :],
                    nda[h: 2 * h, w: 2 * w, :, :],
                ]
            ]

        data = conf.meta['image_files']
        zstacks = _split_in_quarters(
            generate_file_accessor(
                data['multichannel_zstack_raw']['path']
            ).get_mono(
                conf.meta['roiset']['patches_channel']
            )
        )
        masks = _split_in_quarters(
            generate_file_accessor(
                data['multichannel_zstack_mask2d']['path']
            )
        )
        self.roisets = [
            RoiSet.from_binary_mask(
                zstacks[i],
                masks[i],
                params=RoiSetMetaParams(
                    filters={'area': {'min': 1e2, 'max': 1e4}},
                    expand_box_by=(128, 2)
                )
            ) for i in range(0, 4)
        ]

    def test_roisets_contain_mix_of_rois(self):
        counts = [r.count for r in self.roisets]
        self.assertTrue(0 in counts)
        self.assertGreater(sum([c > 0 for c in counts]), 2)

    @staticmethod
    def _serialize_roisets(roisets, where):
        write_to = where / 'phenobase'
        if write_to.exists():
            shutil.rmtree(write_to)
        write_to.mkdir(parents=True, exist_ok=True)
        for ri, roiset in enumerate(roisets):
            roiset.run_exports(
                write_to,
                prefix=f'acq{ri:02d}',
                params=RoiSetExportParams(
                    patches={'channel_zero': PatchParams(white_channel=0)},
                    write_patches_to_subdirectory=True,
                )
            )
        return where

    def test_from_serialized_roisets(self):
        count = sum([r.count for r in self.roisets])
        series = 'patches_channel_zero'
        where = self._serialize_roisets(self.roisets, output_path / 'phenobase_unlabeled')
        phenobase = PhenoBase.read(where)
        self.assertEqual(phenobase.count, count)
        self.assertIsNone(phenobase.labels)

        # test size of patch stack products
        self.assertEqual(phenobase.list_patch_series(), [series])
        self.assertEqual(phenobase.get_raw_patchstack(series).count, count)
        self.assertEqual(phenobase.get_patch_masks().count, count)

        phenobase.write_df()
        self.assertTrue(
            (output_path / 'phenobase_unlabeled' / 'phenobase.csv').exists()
        )
        return phenobase

    def test_classified_rois(self):
        for roiset in self.roisets:
            roiset.classify_by(
                'permissive_model',
                [0],
                IntensityThresholdInstanceMaskSegmentationModel(tr=0.0)
            )
        phenobase = PhenoBase.read(
            self._serialize_roisets(self.roisets, output_path / 'phenobase_labeled')
        )
        self.assertEqual(phenobase.count, sum([r.count for r in self.roisets]))
        self.assertEqual(phenobase.labels, [1])

        obmaps = phenobase.get_patch_obmaps()
        self.assertTrue(all(obmaps.unique()[0] == [0, 1]))

        phenobase.write_df()
        self.assertTrue(
            (output_path / 'phenobase_labeled' / 'phenobase.csv').exists()
        )

    def test_split_phenobase(self):
        phenobase = self.test_from_serialized_roisets()
        split = phenobase.split(0.5)
        self.assertEqual(split['train'].count, split['test'].count)

    def test_sample_phenobase(self):
        phenobase = self.test_from_serialized_roisets()
        sample = phenobase.sample(10)
        self.assertEqual(sample.count, 10)

    def test_update_with_new_roiset(self):
        where = self._serialize_roisets(self.roisets, output_path / 'phenobase_update')
        phenobase = PhenoBase.read(where)
        count = sum([r.count for r in self.roisets])
        self.assertEqual(phenobase.count, count)

        # export new RoiSet
        new_roiset = RoiSet.from_binary_mask(
            self.roisets[0].acc_raw,
            self.roisets[0].acc_obj_ids,
            params=RoiSetMetaParams(
                filters={'area': {'min': 1e2, 'max': 1e4}},
                expand_box_by=(128, 2)
            )
        )
        ri = len(self.roisets)
        new_roiset.run_exports(
            output_path / 'phenobase_update' / 'phenobase',
            prefix=f'phenobase_acq{ri:02d}',
            params=RoiSetExportParams(
                patches={'channel_zero': PatchParams(white_channel=0)},
                write_patches_to_subdirectory=True,
            )
        )

        phenobase.update()
        self.assertEqual(phenobase.count, count + new_roiset.count)

    def test_init_from_roiset(self):
        # export new RoiSet
        roiset = RoiSet.from_binary_mask(
            self.roisets[0].acc_raw,
            self.roisets[0].acc_obj_ids,
            params=RoiSetMetaParams(
                filters={'area': {'min': 1e2, 'max': 1e4}},
                expand_box_by=(128, 2)
            )
        )

        phenobase = PhenoBase.from_roiset(
            root=output_path / 'phenobase_init_from_roiset',
            roiset=roiset,
            index_dict={'acq': 1},
            export_params=RoiSetExportParams(patches={'channel_zero': PatchParams(white_channel=0)}),
        )
        self.assertEqual(phenobase.count, roiset.count)
        self.assertEqual(phenobase.roiset_index, ['coord_acq'])

    def test_push_roiset(self):
        where = self._serialize_roisets(self.roisets, output_path / 'phenobase_push')
        phenobase = PhenoBase.read(where)
        count = sum([r.count for r in self.roisets])
        self.assertEqual(phenobase.count, count)

        # export new RoiSet
        new_roiset = RoiSet.from_binary_mask(
            self.roisets[0].acc_raw,
            self.roisets[0].acc_obj_ids,
            params=RoiSetMetaParams(
                filters={'area': {'min': 1e2, 'max': 1e4}},
                expand_box_by=(128, 2)
            )
        )
        self.assertEqual(phenobase.roiset_index, ['coord_acq'])

        # without specifying patch exports
        with self.assertRaises(MissingPatchSeriesError):
            phenobase.push(new_roiset, {'acq': 99})

        # clashes with existing index
        with self.assertRaises(RoiSetIndexError):
            phenobase.push(new_roiset, {'acq': 3})

        # delegate patch exports to phenobase
        phenobase.push(
            new_roiset,
            {'acq': 99},
            export_params=RoiSetExportParams(patches={'channel_zero': PatchParams(white_channel=0)}),
        )
        self.assertEqual(phenobase.count, count + new_roiset.count)

    def test_push_empty_roiset(self):
        where = self._serialize_roisets(self.roisets, output_path / 'phenobase_push')
        phenobase = PhenoBase.read(where)
        count = phenobase.count

        # export new RoiSet
        acc_in = self.roisets[0].acc_raw
        mask = InMemoryDataAccessor(np.zeros(acc_in.shape, dtype=acc_in.dtype))
        empty_roiset = RoiSet.from_binary_mask(
            acc_in,
            mask,
        )
        self.assertEqual(empty_roiset.count, 0)
        self.assertEqual(phenobase.roiset_index, ['coord_acq'])
        phenobase.push(
            roiset=empty_roiset,
            index_dict={'acq': 10},
            export_params=RoiSetExportParams(patches={'channel_zero': PatchParams(white_channel=0)}),
        )
        self.assertEqual(phenobase.count - count, 0)


class TestSessionPhenoBase(unittest.TestCase):

    def test_initiate_phenobase_with_roiset(self):
        data = conf.meta['image_files']
        acc_in = generate_file_accessor(data['multichannel_zstack_raw']['path'])
        mask = generate_file_accessor(data['multichannel_zstack_mask2d']['path'])

        # make test roiset
        roiset1 = RoiSet.from_binary_mask(
            acc_in,
            mask,
            params=RoiSetMetaParams(
                filters={'area': {'min': 1e3, 'max': 1e4}},
                expand_box_by=(128, 2),
                deproject_channel=0,
            )
        )
        self.assertGreater(roiset1.count, 2)

        # initiate phenobase in session scope
        self.assertIsNone(session_phenobase._phenobase)
        session_phenobase.add_roiset(
            roiset1,
            index_dict={'X': 0, 'T': 0},
            export_params=RoiSetExportParams(patches={'ch0': PatchParams(white_channel=0)}),
        )
        self.assertEqual(session_phenobase.count, roiset1.count)

        # add to phenobase in session scope
        roiset2 = RoiSet.from_binary_mask(
            acc_in.apply(lambda x: x + 1),
            mask,
            params=RoiSetMetaParams(
                filters={'area': {'min': 1e3, 'max': 1e4}},
                expand_box_by=(128, 2),
                deproject_channel=0,
            )
        )
        self.assertGreater(roiset2.count, 2)

        session_phenobase.add_roiset(
            roiset2,
            index_dict={'X': 1, 'T': 1},
            export_params=RoiSetExportParams(patches={'ch0': PatchParams(white_channel=0)}),
        )
        self.assertEqual(session_phenobase.count, roiset1.count + roiset2.count)