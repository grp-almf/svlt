import re
import unittest

import numpy as np
from pathlib import Path

import pandas as pd

from svlt.accessors import generate_file_accessor, InMemoryDataAccessor, write_accessor_data_to_file
from svlt.process import smooth

import svlt.conf.testing as conf

from svlt.rois.labels import get_label_ids
from svlt.rois.roiset import RoiSet, RoiSetExportParams, RoiSetMetaParams
from svlt.rois.models import IntensityThresholdInstanceMaskSegmentationModel
from svlt.rois.df import filter_df_overlap_bbox, filter_df_overlap_seg, read_roiset_df

data = conf.meta['image_files']
output_path = conf.meta['output_path']
params = conf.meta['roiset']



class BaseTestRoiSetMonoProducts(object):

    def setUp(self) -> None:
        # set up test raw data and segmentation from file
        self.stack = generate_file_accessor(data['multichannel_zstack_raw']['path'])
        self.stack_ch_pa = self.stack.get_mono(params['patches_channel'])
        self.seg_mask = generate_file_accessor(data['multichannel_zstack_mask2d']['path'])


class TestRoiSetMonoProducts(BaseTestRoiSetMonoProducts, unittest.TestCase):

    def _make_roi_set(self, mask_type='boxes', **kwargs):
        roiset = RoiSet.from_binary_mask(
            self.stack_ch_pa,
            self.seg_mask,
            params=RoiSetMetaParams(
                mask_type=mask_type,
                filters=kwargs.get('filters', {'area': {'min': 1e3, 'max': 1e4}}),
                expand_box_by=(128, 2)
            )
        )
        return roiset

    def test_roi_mask_shape(self, **kwargs):
        roiset = self._make_roi_set(**kwargs)

        # all masks' bounding boxes are at least as big as ROI area
        def _validate_roi(roi):
            self.assertEqual(roi.masks.binary_mask.dtype, 'bool')
            sh = roi.masks.binary_mask.shape
            h = roi.bounding_box.h
            w = roi.bounding_box.w
            area = roi.bounding_box.area
            self.assertEqual(sh, (h, w))
            self.assertGreaterEqual(sh[0] * sh[1], area)
        roiset.df().apply(_validate_roi, axis=1)

    def test_roi_zmask(self, **kwargs):
        roiset = self._make_roi_set(**kwargs)
        zmask = roiset.get_zmask()
        zmask_acc = InMemoryDataAccessor(zmask)
        self.assertTrue(zmask_acc.is_mask())

        # assert dimensionality of zmask
        self.assertEqual(zmask_acc.nz, roiset.acc_raw.nz)
        self.assertEqual(zmask_acc.chroma, 1)
        write_accessor_data_to_file(output_path / 'zmask.tif', zmask_acc)

        # mask values are not just all True or all False
        self.assertTrue(np.any(zmask))
        self.assertFalse(np.all(zmask))


    def test_roiset_from_non_zstacks(self, **kwargs):
        acc_zstack_slice = InMemoryDataAccessor(self.stack_ch_pa.data[:, :, :, 0])
        self.assertEqual(acc_zstack_slice.nz, 1)

        roiset = RoiSet.from_binary_mask(acc_zstack_slice, self.seg_mask, params=RoiSetMetaParams(mask_type='boxes'))
        zmask = roiset.get_zmask()

        zmask_acc = InMemoryDataAccessor(zmask)
        self.assertTrue(zmask_acc.is_mask())

    def test_create_roiset_with_no_2d_objects(self):
        zero_obmap = InMemoryDataAccessor(np.zeros(self.seg_mask.shape, self.seg_mask.dtype))
        roiset = RoiSet.from_object_ids(self.stack_ch_pa, zero_obmap)
        self.assertEqual(roiset.count, 0)
        cl = 'dummy_class'
        roiset.classify_by(cl, [0], conf.DummyInstanceMaskSegmentationModel())
        self.assertTrue(cl in roiset.df().classifications.columns)

    def test_create_roiset_with_no_3d_objects(self):
        seg_mask_3d = generate_file_accessor(data['multichannel_zstack_mask3d']['path'])
        zero_obmap = InMemoryDataAccessor(np.zeros(seg_mask_3d.shape, seg_mask_3d.dtype))
        roiset = RoiSet.from_object_ids(self.stack_ch_pa, zero_obmap)
        self.assertEqual(roiset.count, 0)
        cl = 'dummy_class'
        roiset.classify_by(cl, [0], conf.DummyInstanceMaskSegmentationModel())
        self.assertTrue(cl in roiset.df().classifications.columns)

    def test_slices_are_valid(self):
        roiset = self._make_roi_set()
        for s in roiset.get_slices():
            ebb = roiset.acc_raw.data[s]
            self.assertEqual(len(ebb.shape), 4)
            self.assertTrue(np.all([si >= 1 for si in ebb.shape]))

    def test_dataframe_and_mask_array_in_iterator(self):
        roiset = self._make_roi_set()
        def _validate_mask_shape(roi):
            ma = roi.masks.binary_mask
            bb = roi.bounding_box
            self.assertEqual(ma.dtype, 'bool')
            self.assertEqual(ma.shape, (bb.h, bb.w))
        roiset.df().apply(_validate_mask_shape, axis=1)

    def test_rel_slices_are_valid(self):
        roiset = self._make_roi_set()
        def _validate_rel_slices(roi):
            ebb = roiset.acc_raw.data[roi.slices.expanded_slice]
            self.assertEqual(len(ebb.shape), 4)
            self.assertTrue(np.all([si >= 1 for si in ebb.shape]))
            rbb = ebb[roi.slices.relative_slice]
            self.assertEqual(len(rbb.shape), 4)
            self.assertTrue(np.all([si >= 1 for si in rbb.shape]))
        roiset.df().apply(_validate_rel_slices, axis=1)


    def test_make_expanded_2d_patches(self):
        roiset = self._make_roi_set()
        where = output_path / 'expanded_2d_patches'
        se_res = roiset.export_patches(
            where,
            draw_bounding_box=True,
            expanded=True,
            pad_to=256,
            make_3d=False,
        )
        df = roiset.df()
        for f in se_res:
            acc = generate_file_accessor(where / f)
            la = int(re.search(r'la([\d]+)', str(f)).group(1))
            roi_q = df.loc[df.index == la, :]
            self.assertEqual(len(roi_q), 1)
            self.assertEqual((256, 256), acc.hw)
            self.assertEqual(1, acc.nz)

    def test_make_tight_2d_patches(self):
        roiset = self._make_roi_set()
        where = output_path / 'tight_2d_patches'
        se_res = roiset.export_patches(
            where,
            draw_bounding_box=True,
            expanded=False,
        )
        df = roiset.df()
        for f in se_res:  # all exported files are same shape as bounding boxes in RoiSet's datatable
            acc = generate_file_accessor(where / f)
            la = int(re.search(r'la([\d]+)', str(f)).group(1))
            roi_q = df.loc[df.index == la, :]
            self.assertEqual(len(roi_q), 1)
            bbox = roi_q.iloc[0].bounding_box
            self.assertEqual((bbox.h, bbox.w), acc.hw)
            self.assertEqual(1, acc.nz)

    def test_make_expanded_3d_patches(self):
        roiset = self._make_roi_set()
        where = output_path / '3d_patches'
        se_res = roiset.export_patches(
            where,
            make_3d=True,
            expanded=True
        )
        self.assertGreaterEqual(len(se_res), 1)
        for f in se_res:
            acc = generate_file_accessor(where / f)
            self.assertGreater(acc.nz, 1)


    def test_export_annotated_zstack(self):
        roiset = self._make_roi_set()
        where = output_path / 'annotated_zstack'
        file = roiset.export_annotated_zstack(
            where,
        )
        result = generate_file_accessor(where / file)
        self.assertEqual(result.shape, roiset.acc_raw.shape)


    def test_make_binary_masks(self):
        roiset = self._make_roi_set()
        se_res = roiset.export_patch_masks(output_path / '2d_mask_patches', )

        df = roiset.df()
        for f in se_res:  # all exported files are same shape as bounding boxes in RoiSet's datatable
            acc = generate_file_accessor(output_path / '2d_mask_patches' / f)
            la = int(re.search(r'la([\d]+)', str(f)).group(1))
            roi_q = df.loc[df.index == la, :]
            self.assertEqual(len(roi_q), 1)
            bbox = roi_q.iloc[0].bounding_box
            self.assertEqual((bbox.h, bbox.w), acc.hw)

    def test_classify_by(self):
        roiset = self._make_roi_set()
        cl = 'dummy_class'
        roiset.classify_by(cl, [0], conf.DummyInstanceMaskSegmentationModel())
        self.assertTrue(all(roiset.df()['classifications', cl].unique() == [1]))
        self.assertTrue(all(np.unique(roiset.get_object_class_map(cl).data) == [0, 1]))
        return roiset

    def test_classify_by_multiple_channels(self):
        roiset = RoiSet.from_binary_mask(self.stack, self.seg_mask, params=RoiSetMetaParams(deproject_channel=0))
        cl = 'dummy_class'
        roiset.classify_by(cl, [0, 1], conf.DummyInstanceMaskSegmentationModel())
        self.assertTrue(all(roiset.df().classifications[cl].unique() == [1]))
        self.assertTrue(all(np.unique(roiset.get_object_class_map('dummy_class').data) == [0, 1]))
        return roiset

    def test_transfer_classification(self):
        roiset1 = RoiSet.from_binary_mask(self.stack, self.seg_mask, params=RoiSetMetaParams(deproject_channel=0))

        # prepare alternative mask and compare
        smoothed_mask = self.seg_mask.apply(lambda x: smooth(x, sig=1.0))
        roiset2 = RoiSet.from_binary_mask(self.stack, smoothed_mask, params=RoiSetMetaParams(deproject_channel=0))
        dmask = (self.seg_mask.data / 255) + (smoothed_mask.data / 255)
        self.assertTrue(np.all(np.unique(dmask) == [0, 1, 2]))
        total_iou = (dmask == 2).sum() / ((dmask == 1).sum() + (dmask == 2).sum())
        self.assertGreater(total_iou, 0.5)

        # classify first RoiSet
        cl = 'dummy_class'
        roiset1.classify_by(cl, [0, 1], conf.DummyInstanceMaskSegmentationModel())

        self.assertTrue(cl in roiset1.classification_columns)
        self.assertFalse(cl in roiset2.classification_columns)
        res = roiset2.get_instance_classification(roiset1)
        self.assertTrue(cl in roiset2.classification_columns)
        self.assertLess(
            roiset2.df().classifications[cl].count(),
            roiset1.df().classifications[cl].count(),
        )


    def test_export_object_classes(self):
        record = self.test_classify_by().run_exports(
            output_path / 'object_class_maps',
            'obmap',
            RoiSetExportParams(object_classes=True)
        )
        opa = record['object_classes_dummy_class']
        self.assertTrue(Path(opa).exists())
        acc = generate_file_accessor(opa)
        self.assertTrue(all(np.unique(acc.data) == [0, 1]))


    def test_raw_patches_are_correct_shape(self):
        roiset = self._make_roi_set()
        patches = roiset.get_patches_acc()
        np, h, w, nc, nz = patches.shape
        self.assertEqual(np, roiset.count)
        self.assertEqual(nc, roiset.acc_raw.chroma)
        self.assertEqual(nz, 1)

    def test_patch_masks_are_correct_shape(self):
        roiset = self._make_roi_set()
        df_patch_masks = roiset.get_patch_masks()
        self.assertIsInstance(df_patch_masks.columns, pd.MultiIndex)
        def _validate_roi(roi):
            h, w, nz = roi.masks.patch_mask.shape
            self.assertEqual(nz, 1)
            self.assertEqual(h, roi.bounding_box.h)
            self.assertEqual(w, roi.bounding_box.w)
        df_patch_masks.apply(_validate_roi, axis=1)

    def test_patch_obmaps_match_df_labels(self):
        roiset = self._make_roi_set()
        acc_patch_obmaps = roiset.get_patch_obmap_acc()
        acc_la = [la for la in acc_patch_obmaps.unique()[0] if la > 0]
        acc_la.sort()
        df_la = roiset.df().index.values
        df_la.sort()
        self.assertEqual(len(acc_la), len(df_la))
        self.assertTrue(all([acc_la[i] == df_la[i] for i in range(0, len(acc_la))]))

        def _check_label(roi):
            i = roi['patches', 'index']
            la = roi.name
            return acc_patch_obmaps.iat(i).unique()[0][-1] == la
        self.assertTrue(roiset.df()[[('patches', 'index')]].apply(_check_label, axis=1).all())

class TestRoiSet3dProducts(unittest.TestCase):

    where = output_path / 'run_exports_mono_3d'

    def setUp(self) -> None:
        # set up test raw data and segmentation from file
        self.stack = generate_file_accessor(data['multichannel_zstack_raw']['path'])
        self.stack_ch_pa = self.stack.get_mono(params['segmentation_channel'])
        self.seg_mask_3d = generate_file_accessor(data['multichannel_zstack_mask3d']['path'])
        self.where.mkdir(parents=True, exist_ok=True)

    def test_create_roiset_from_3d_obj_ids(self):
        roiset = RoiSet.from_binary_mask(
            self.stack_ch_pa,
            self.seg_mask_3d,
            allow_3d=True,
            connect_3d=True,
            params=RoiSetMetaParams(filters={'area': {'min': 1e2, 'max': 1e8}})
        )
        df = roiset.df()
        df.to_csv(self.where / 'df.csv')

        bbdf = roiset.df().bounding_box
        self.assertGreater(len(bbdf['zi'].unique()), 1)
        self.assertTrue((bbdf['z1'] - bbdf['z0'] > 1).any())

        roiset.acc_obj_ids.write(self.where / 'labels.tif')
        return roiset

    def test_run_export_mono_3d_patch(self):
        p = RoiSetExportParams(**{
            'patches': {
                '3d': {
                    'make_3d': True,
                    'white_channel': -1,
                    'draw_bounding_box': False,
                    'draw_mask': False,
                    'pad_to': None,
                    'rgb_overlay_channels': None,
                    'update_focus_zi': False,
                },
                '3d_masks': {
                    'make_3d': True,
                    'is_patch_mask': True,
                },
                '2d_masks': {
                    'make_3d': False,
                    'is_patch_mask': True,
                },
            },
        })

        res = self.test_create_roiset_from_3d_obj_ids().run_exports(
            self.where,
            prefix='test',
            params=p
        )

        # test that exported patches are 3d
        def _get_nz_from_file(fname):
            pa = self.where / fname
            pacc = generate_file_accessor(pa)
            return pacc.nz

        self.assertTrue(any([_get_nz_from_file(f) > 1 for f in res['patches_3d']]))
        self.assertTrue(any([_get_nz_from_file(f) > 1 for f in res['patches_3d_masks']]))
        self.assertTrue(all([_get_nz_from_file(f) == 1 for f in res['patches_2d_masks']]))

    def test_run_update_focus_during_patch_export(self):
        p = RoiSetExportParams(**{
            'patches': {
                '2d': {
                    'make_3d': False,
                    'focus_metric': 'max_sobel',
                    'white_channel': -1,
                    'draw_bounding_box': False,
                    'draw_mask': False,
                    'pad_to': None,
                    'rgb_overlay_channels': None,
                    'update_focus_zi': True,
                },
            },
        })
        roiset = self.test_create_roiset_from_3d_obj_ids()
        starting_zi = roiset.df().bounding_box['zi']

        res = roiset.run_exports(
            self.where,
            prefix='test',
            params=p
        )
        self.assertIsInstance(roiset.df().columns, pd.MultiIndex)
        updated_zi = roiset.df().bounding_box['zi']
        self.assertTrue((starting_zi != updated_zi).any())

    def test_mip_patch_masks(self):
        p = RoiSetExportParams(**{
            'patches': {
                '3d_masks': {
                    'make_3d': True,
                    'is_patch_mask': True,
                },
                '2d_masks': {
                    'make_3d': False,
                    'is_patch_mask': True,
                    'mask_mip': True,
                },
            },
        })

        rois = self.test_create_roiset_from_3d_obj_ids()
        self.assertIsInstance(rois._df.columns, pd.MultiIndex)
        res = rois.run_exports(
            self.where,
            prefix='test',
            params=p
        )

        # test that exported patches are 3d
        def _get_nz_from_file(fname):
            pa = self.where / fname
            pacc = generate_file_accessor(pa)
            return pacc.nz

        n_3d = len(res['patches_3d_masks'])
        n_2d = len(res['patches_2d_masks'])
        self.assertEqual(n_3d, n_2d)
        for pi in range(0, n_3d):
            acc_3d = generate_file_accessor(self.where / res['patches_3d_masks'][pi])
            acc_2d = generate_file_accessor(self.where / res['patches_2d_masks'][pi])
            self.assertTrue(
                np.all(
                    acc_3d.get_mip().data == acc_2d.data
                )
            )

    def test_run_export_mono_3d_labels_overlay(self):
        # export via .run_exports() method
        res = self.test_create_roiset_from_3d_obj_ids().run_exports(
            self.where,
            prefix='labels_overlay',
            params=RoiSetExportParams(
                **{
                    'labels_overlay': {
                        'white_channel': -1, 'transparency': 0.2, 'mip': False, 'rescale_clip': 0.00
                    }
                }
            )
        )
        acc = generate_file_accessor(self.where / res['labels_overlay'])
        self.assertGreater(acc.nz, 1)

    def test_serialize_and_deserialize_3d_patches(self):
        ref_roiset = self.test_create_roiset_from_3d_obj_ids()
        ref_roiset.serialize(self.where / 'serialize', prefix='ref')

        # make another RoiSet from just the data table, raw images, and (tight) patch masks
        test_roiset = RoiSet.deserialize(self.stack_ch_pa, self.where / 'serialize', prefix='ref')
        self.assertEqual(ref_roiset.get_zmask().shape, test_roiset.get_zmask().shape, )
        self.assertTrue((ref_roiset.get_zmask() == test_roiset.get_zmask()).all())
        self.assertTrue(
            np.all(
                test_roiset.df().index.unique() == ref_roiset.df().index.unique()
            )
        )


class TestRoiSetMultichannelProducts(BaseTestRoiSetMonoProducts, unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.roiset = RoiSet.from_binary_mask(
            self.stack,
            self.seg_mask,
            params=RoiSetMetaParams(
                expand_box_by=(128, 2),
                mask_type='boxes',
                filters={
                    'area': {'min': 1e3, 'max': 1e4},
                    'diag': {'min': 1e1, 'max': 1e5},
                    'min_hw': {'min': 1e1, 'max': 1e4}
                },
                deproject_channel=0,
            )
        )

    def test_multichannel_to_mono_2d_patches(self):
        where = output_path / 'multichannel' / 'mono_2d_patches'
        se_res = self.roiset.export_patches(
            where,
            white_channel=0,
            draw_bounding_box=True,
            expanded=True,
            pad_to=256,
        )
        result = generate_file_accessor(where / se_res.iloc[0])
        self.assertEqual(result.chroma, 1)

    def test_multichannel_to_color_2d_patches(self):
        chs = [0, 2,]
        where = output_path / 'multichannel' / 'color_2d_patches'
        self.assertGreater(self.roiset.acc_raw.chroma, 1)
        patches_acc = self.roiset.get_patches_acc(channels=chs)
        self.assertEqual(patches_acc.chroma, len(chs))

        se_res = self.roiset.export_patches(
            where,
            channels=chs,
            draw_bounding_box=True,
            expanded=True,
            pad_to=256,
            force_tif=True,
        )
        result = generate_file_accessor(where / se_res.iloc[0])
        self.assertEqual(result.chroma, len(chs))

    def test_multichannnel_to_mono_2d_patches_rgb_bbox(self):
        where = output_path / 'multichannel' / 'mono_2d_patches_rgb_bbox'
        se_res = self.roiset.export_patches(
            where,
            white_channel=3,
            draw_bounding_box=True,
            bounding_box_channel=1,
            expanded=True,
            pad_to=256,
        )
        result = generate_file_accessor(where / se_res.iloc[0])
        self.assertEqual(result.chroma, 3)

    def test_multichannnel_to_rgb_2d_patches_bbox(self):
        where = output_path / 'multichannel' / 'rgb_2d_patches_bbox'
        se_res = self.roiset.export_patches(
            where,
            white_channel=4,
            rgb_overlay_channels=(3, None, None),
            draw_mask=False,
            draw_bounding_box=True,
            bounding_box_channel=1,
            rgb_overlay_weights=(0.1, 1.0, 1.0),
            expanded=True,
            pad_to=256,
        )
        result = generate_file_accessor(where / se_res.iloc[0])
        self.assertEqual(result.chroma, 3)

    def test_multichannnel_to_rgb_2d_patches_mask(self):
        where = output_path / 'multichannel' / 'rgb_2d_patches_mask'
        se_res = self.roiset.export_patches(
            where,
            white_channel=4,
            rgb_overlay_channels=(3, None, None),
            draw_mask=True,
            mask_channel=0,
            rgb_overlay_weights=(0.1, 1.0, 1.0),
            expanded=True,
            pad_to=256,
        )
        result = generate_file_accessor(where / se_res.iloc[0])
        self.assertEqual(result.chroma, 3)

    def test_multichannnel_to_rgb_2d_patches_contour(self):
        where = output_path / 'multichannel' / 'rgb_2d_patches_contour'
        se_res = self.roiset.export_patches(
            where,
            rgb_overlay_channels=(3, None, None),
            draw_contour=True,
            contour_channel=1,
            rgb_overlay_weights=(0.1, 1.0, 1.0),
            expanded=True,
            pad_to=256,
        )
        result = generate_file_accessor(where / se_res.iloc[0])
        self.assertEqual(result.chroma, 3)
        self.assertEqual(result.get_mono(2).data.max(), 0)  # blue channel is black

    def test_multichannel_to_multichannel_tif_patches(self):
        where = output_path / 'multichannel' / 'multichannel_tif_patches'
        se_res = self.roiset.export_patches(
            where,
            expanded=True,
            pad_to=256,
        )
        result = generate_file_accessor(where / se_res.iloc[0])
        self.assertEqual(result.chroma, 5)
        self.assertEqual(result.nz, 1)

    def test_multichannel_to_label_ids_overlay(self):
        where = output_path / 'multichannel' / 'multichannel_label_ids_overlay'

        # export z-stack product
        acc_out = self.roiset.get_export_product_accessors(
            params=RoiSetExportParams(**{'labels_overlay': {'white_channel': -1, 'transparency': 0.5}})
        )['labels_overlay']
        self.assertEqual(acc_out.chroma, 3)
        self.assertGreater(acc_out.nz, 1)
        acc_out.write(where / 'overlay_composite_zstack.tif', composite=True)

        # export MIP product
        acc_out = self.roiset.get_export_product_accessors(
            params=RoiSetExportParams(**{'labels_overlay': {'white_channel': 0, 'transparency': 0.2, 'mip': True}})
        )['labels_overlay']
        self.assertEqual(acc_out.chroma, 3)
        self.assertEqual(acc_out.nz, 1)
        acc_out.write(where / 'overlay_composite_mip.tif', composite=True)

        # export via .run_exports() method
        self.roiset.run_exports(
            where,
            prefix='run_exports',
            params=RoiSetExportParams(
                **{
                    'labels_overlay': {
                        'white_channel': -1, 'transparency': 0.2, 'mip': True, 'rescale_clip': 0.00
                    }
                }
            )
        )



    def test_multichannel_annotated_zstack(self):
        where = output_path / 'multichannel' / 'annotated_zstack'
        file = self.roiset.export_annotated_zstack(
            where,
            'test_multichannel_annotated_zstack',
            expanded=True,
            pad_to=256,
            draw_label=True,
        )
        result = generate_file_accessor(where / file)
        self.assertEqual(result.chroma, self.stack.chroma)
        self.assertEqual(result.nz, self.stack.nz)

    def test_export_single_channel_annotated_zstack(self):
        where = output_path / 'annotated_zstack'
        file = self.roiset.export_annotated_zstack(
            where,
            channel=3,
            expanded=True,
            pad_to=256,
            draw_label=True,
        )
        result = generate_file_accessor(where / file)
        self.assertEqual(result.hw, self.roiset.acc_raw.hw)
        self.assertEqual(result.nz, self.roiset.acc_raw.nz)
        self.assertEqual(result.chroma, 1)

    def test_run_exports(self):
        p = RoiSetExportParams(**{
            'patches': {
                '2d_annotated': {
                    'white_channel': 3,
                    'draw_bounding_box': True,
                    'rgb_overlay_channels': [3, None, None],
                    'rgb_overlay_weights': [0.2, 1.0, 1.0],
                    'pad_to': 512,
                },
                '2d': {
                    'white_channel': 3,
                    'draw_bounding_box': False,
                    'draw_mask': False,
                },
            },
            'annotated_zstacks': {},
            'object_classes': True,
            'dataframe': True,
        })

        where = output_path / 'run_exports'
        res = self.roiset.run_exports(
            where,
            prefix='test',
            params=p
        )

        # test on return paths
        for k, v in res.items():
            if isinstance(v, list):
                for f in v:
                    try:
                        self.assertFalse(Path(f).is_absolute())
                    except Exception as e:
                        print(e)
                    self.assertTrue((where / f).exists())
            else:
                self.assertFalse(Path(v).is_absolute())
                self.assertTrue((where / v).exists())

        # test on paths in CSV
        test_df = read_roiset_df(where / res['dataframe'])
        for c in ['tight_patch_masks_path', 'patches_2d_path', 'patches_2d_annotated_path']:
            self.assertTrue(c in test_df.patches.columns)
            for f in test_df.patches[c]:
                self.assertTrue((where / f).exists(), where / f)

    def test_get_interm_prods(self):
        p = RoiSetExportParams(**{
            'patches': {
                '2d': {
                    'white_channel': 3,
                    'draw_bounding_box': False,
                    'draw_mask': False,
                },
                '2d_annotated': {
                    'white_channel': 3,
                    'draw_bounding_box': True,
                    'rgb_overlay_channels': [3, None, None],
                    'rgb_overlay_weights': [0.2, 1.0, 1.0],
                    'pad_to': 512,
                },
            },
            'annotated_zstacks': {},
            'object_classes': True,
        })
        self.roiset.classify_by('dummy_class', [3], conf.DummyInstanceMaskSegmentationModel())
        interm = self.roiset.get_export_product_accessors(
            params=p
        )
        self.assertNotIn('patches_3d', interm.keys())
        bbox = self.roiset.df().bounding_box
        self.assertEqual(
            interm['patches_2d_annotated'].hw,
            (bbox.h.max(), bbox.w.max())
        )
        self.assertEqual(
            interm['patches_2d'].hw,
            (bbox.h.max(), bbox.w.max())
        )
        self.assertEqual(
            interm['annotated_zstacks'].hw,
            self.stack.hw
        )
        self.assertEqual(
            interm['object_classes_dummy_class'].hw,
            self.stack.hw
        )
        self.assertTrue(np.all(interm['object_classes_dummy_class'].unique()[0] == [0, 1]))

    def test_run_export_expanded_2d_patch(self):
        p = RoiSetExportParams(**{
            'patches': {
                '2d': {
                    'white_channel': -1,
                    'draw_bounding_box': False,
                    'draw_mask': False,
                    'expanded': True,
                    'pad_to': 256,
                },
            },
        })
        self.assertTrue(hasattr(p.patches['2d'], 'pad_to'))
        self.assertTrue(hasattr(p.patches['2d'], 'expanded'))

        where = output_path / 'run_exports_expanded_2d_patch'
        res = self.roiset.run_exports(
            where,
            prefix='test',
            params=p
        )

        # test that exported patches are padded dimension
        for fn in res['patches_2d']:
            pa = where / fn
            self.assertTrue(pa.exists())
            pacc = generate_file_accessor(pa)
            self.assertEqual(pacc.hw, (256, 256))

    def test_run_export_mono_2d_patch(self):
        p = RoiSetExportParams(**{
            'patches': {
                '2d': {
                    'white_channel': -1,
                    'draw_bounding_box': False,
                    'draw_mask': False,
                    'expanded': True,
                    'pad_to': 256,
                    'rgb_overlay_channels': None,
                },
            },
        })
        self.assertTrue(hasattr(p.patches['2d'], 'pad_to'))
        self.assertTrue(hasattr(p.patches['2d'], 'expanded'))

        where = output_path / 'run_exports_mono_2d_patch'
        res = self.roiset.run_exports(
            where,
            prefix='test',
            params=p
        )

        # test that exported patches are padded dimension
        for fn in res['patches_2d']:
            pa = where / fn
            self.assertTrue(pa.exists())
            pacc = generate_file_accessor(pa)
            self.assertEqual(pacc.chroma, 1)


class TestRoiSetSerialization(unittest.TestCase):

    def setUp(self) -> None:
        # set up test raw data and segmentation from file
        self.stack = generate_file_accessor(data['multichannel_zstack_raw']['path'])
        self.stack_ch_pa = self.stack.get_mono(params['segmentation_channel'])
        self.seg_mask_3d = generate_file_accessor(data['multichannel_zstack_mask3d']['path'])
        self.seg_mask_2d = generate_file_accessor(data['multichannel_zstack_raw']['path'])

    @staticmethod
    def _label_is_2d(id_map, la):  # single label's zmask has same counts as its MIP
        mask_3d = (id_map == la)
        mask_mip = mask_3d.max(axis=-1)
        return mask_3d.sum() == mask_mip.sum()

    def test_id_map_connects_z(self):
        id_map = get_label_ids(self.seg_mask_3d, allow_3d=True, connect_3d=True)
        labels = np.unique(id_map.data)[1:]
        is_2d = all([self._label_is_2d(id_map.data, la) for la in labels])
        self.assertFalse(is_2d)

    def test_id_map_disconnects_z(self):
        id_map = get_label_ids(self.seg_mask_3d, allow_3d=True, connect_3d=False)
        labels = np.unique(id_map.data)[1:]
        is_2d = all([self._label_is_2d(id_map.data, la) for la in labels])
        self.assertTrue(is_2d)

    def test_create_roiset_from_2d_obj_ids(self):
        id_map = get_label_ids(self.seg_mask_3d, allow_3d=False)
        self.assertEqual(self.stack_ch_pa.shape[0:3], id_map.shape[0:3])
        self.assertEqual(id_map.nz, 1)

        roiset = RoiSet.from_object_ids(
            self.stack_ch_pa,
            id_map,
            params=RoiSetMetaParams(mask_type='contours')
        )
        self.assertEqual(roiset.count, id_map.data.max())
        self.assertGreater(len(roiset.df()['bounding_box', 'zi'].unique()), 1)
        return roiset

    def test_create_roiset_from_df_and_patch_masks(self):
        ref_roiset = self.test_create_roiset_from_2d_obj_ids()
        where_ser = output_path / 'serialize'
        ref_roiset.serialize(where_ser, prefix='ref')
        where_df = where_ser / 'dataframe' / 'ref.csv'
        self.assertTrue(where_df.exists())
        df_test = read_roiset_df(where_df)

        # check that patches are correct size
        where_patch_masks = where_ser / 'tight_patch_masks'
        patch_filenames = []
        for pmf in where_patch_masks.iterdir():
            self.assertTrue(pmf.suffix.upper() == '.PNG')
            la = int(re.search(r'la([\d]+)', str(pmf)).group(1))
            roi_q = df_test.loc[df_test.index == la, :]
            self.assertEqual(len(roi_q), 1)
            bb = roi_q.iloc[0].bounding_box
            m_acc = generate_file_accessor(pmf)
            self.assertEqual((bb.h, bb.w), m_acc.hw)
            patch_filenames.append(pmf.name)
            self.assertEqual(m_acc.nz, 1)

        # make another RoiSet from just the data table, raw images, and (tight) patch masks
        test_roiset = RoiSet.deserialize(self.stack_ch_pa, where_ser, prefix='ref')
        self.assertEqual(
            ref_roiset.get_zmask().shape,
            test_roiset.get_zmask().shape
        )
        self.assertTrue(
            (ref_roiset.get_zmask() == test_roiset.get_zmask()).all()
        )
        self.assertTrue(
            np.all(test_roiset.df().index == ref_roiset.df().index)
        )
        cols = ['y1', 'y0', 'x1', 'x0', 'zi']
        self.assertTrue(
            (test_roiset.df().bounding_box[cols] == ref_roiset.df().bounding_box[cols]).all().all()
        )

        # re-serialize and check that patch masks are the same
        where_dser = output_path / 'deserialize'
        test_roiset.serialize(where_dser, prefix='test')
        for fr in patch_filenames:
            pr = (where_ser / 'tight_patch_masks' / fr)
            self.assertTrue(pr.exists())
            pt = (where_dser / 'tight_patch_masks' / fr.replace('ref', 'test'))
            self.assertTrue(pt.exists())
            r_acc = generate_file_accessor(pr)
            t_acc = generate_file_accessor(pt)
            self.assertTrue(np.all(r_acc.data == t_acc.data))

class TestEmptyRoiSet(unittest.TestCase):
    """
    Many RoiSet methods need special handling of zero-length DataFrame tables
    """

    def setUp(self) -> None:
        self.stack = generate_file_accessor(data['multichannel_zstack_raw']['path'])
        self.empty_binary_mask = InMemoryDataAccessor(np.zeros((*self.stack.hw, 1, self.stack.nz), dtype=bool))
        self.empty_roiset = RoiSet.from_binary_mask(self.stack, self.empty_binary_mask, allow_3d=True)

    def test_get_patches(self):
        roiset = self.empty_roiset
        params = {
            'white_channel': 0,
            'rescale_clip': 0.00,
            'focus_metric': 'max_sobel',
            'update_focus_zi': True,
        }
        self.assertEqual(roiset.count, 0)
        se_patches = roiset.get_patches(**params)
        self.assertIsInstance(se_patches, pd.Series)
        self.assertEqual(len(se_patches), 0)
        self.assertIsNone(roiset.get_patches_acc(**params))

    def test_get_patch_masks(self):
        roiset = self.empty_roiset
        self.assertEqual(roiset.count, 0)
        df_patches = roiset.get_patch_masks(make_3d=True)
        self.assertIsInstance(df_patches, pd.DataFrame)
        self.assertEqual(len(df_patches), 0)
        self.assertIsNone(roiset.get_patches_acc(make_3d=True))

    def test_get_obmaps(self):
        roiset = self.empty_roiset
        self.assertEqual(roiset.count, 0)
        acc_obmap = roiset.get_patch_obmap_acc(make_3d=True)
        self.assertIsNone(acc_obmap)

    def test_run_exports(self):
        roiset = self.empty_roiset
        export_params = RoiSetExportParams(**{
            'patches': {
                '2d_af405': {
                    'white_channel': 0,
                    'rescale_clip': 0.00,
                    'focus_metric': 'max_sobel',
                },
                '2d_tl': {
                    'white_channel': -1,
                    'rescale_clip': 0.00,
                    'focus_metric': 'max_sobel',
                },
                '3d_tl': {
                    'white_channel': 2,
                    'make_3d': True,
                    'draw_bounding_box': False,
                    'draw_mask': False,
                    'rescale_clip': 0.001,
                },
                '2d_masks': {
                    'is_patch_mask': True,
                    'make_3d': False,
                },
                '3d_masks': {
                    'is_patch_mask': True,
                    'make_3d': True,
                },
            },
            'labels_overlay': {
                'white_channel': -1,
                'transparency': 0.4,
                'mip': False,
                'rescale_clip': 0.00,
            },
            'annotated_z_stack': {},
            'dataframe': True,
        })
        self.assertIsNone(roiset.run_exports(output_path / 'empty_roiset', 'empty', export_params))

    def test_classify_by(self):
        roiset = self.empty_roiset
        self.assertFalse('permissive_model' in roiset.classification_columns)
        self.assertTrue(
            roiset.classify_by('permissive_model', [0], IntensityThresholdInstanceMaskSegmentationModel(tr=0.0))
        )
        self.assertEqual(roiset.count, 0)
        self.assertTrue('permissive_model' in roiset.classification_columns)

class TestRoiSetObjectDetection(unittest.TestCase):

    def setUp(self) -> None:
        # set up test raw data and segmentation from file
        self.stack = generate_file_accessor(data['multichannel_zstack_raw']['path'])
        self.stack_ch_pa = self.stack.get_mono(params['segmentation_channel'])
        self.seg_mask_3d = generate_file_accessor(data['multichannel_zstack_mask3d']['path'])

    def test_create_roiset_from_bounding_boxes(self):
        from skimage.measure import label, regionprops_table

        mask = self.seg_mask_3d
        labels = label(mask.data_yxz, connectivity=3)
        table = pd.DataFrame(
            regionprops_table(labels)
        ).rename(
            columns={'bbox-0': 'y', 'bbox-1': 'x', 'bbox-2': 'zi', 'bbox-3': 'y1', 'bbox-4': 'x1'}
        ).drop(
            columns=['bbox-5']
        )
        table['w'] = table['x1'] - table['x']
        table['h'] = table['y1'] - table['y']
        bboxes = table[['y', 'x', 'h', 'w']].to_dict(orient='records')

        roiset_bbox = RoiSet.from_bounding_boxes(self.stack_ch_pa, bboxes)
        patches_bbox = roiset_bbox.get_patches_acc()
        self.assertEqual(len(table), patches_bbox.count)


        # roiset w/ seg for comparison
        roiset_seg = RoiSet.from_binary_mask(self.stack_ch_pa, mask, allow_3d=True)
        patches_seg = roiset_seg.get_patches_acc()

        # test bounding box dimensions match those from RoiSet generated directly from segmentation
        self.assertEqual(roiset_seg.count, roiset_bbox.count)
        for i in range(0, roiset_seg.count):
            patch_from_seg = patches_seg.iat(0, crop=True)
            patch_from_bbox = patches_bbox.iat(0, crop=True)
            self.assertEqual(patch_from_seg.hw, patch_from_bbox.hw)
            self.assertEqual(patch_from_seg.chroma, patch_from_bbox.chroma)

        # test that serialization does not write patch masks
        roiset_ser_path = output_path / 'roiset_from_bbox'
        dd = roiset_bbox.serialize(roiset_ser_path)
        self.assertTrue('tight_patch_masks' not in dd.keys())
        self.assertFalse((roiset_ser_path / 'tight_patch_masks').exists())

        # test that deserialized RoiSet matches the original
        roiset_des = RoiSet.deserialize(self.stack_ch_pa, roiset_ser_path)
        self.assertEqual(roiset_des.count, roiset_bbox.count)
        for i in range(0, roiset_des.count):
            patch_from_seg = patches_seg.iat(0, crop=True)
            patch_from_bbox = patches_bbox.iat(0, crop=True)
            self.assertEqual(patch_from_seg.hw, patch_from_bbox.hw)
            self.assertEqual(patch_from_seg.chroma, patch_from_bbox.chroma)
        self.assertTrue((roiset_bbox.get_zmask() == roiset_des.get_zmask()).all())


class TestRoiSetPolygons(BaseTestRoiSetMonoProducts, unittest.TestCase):

    def test_compute_polygons(self):
        roiset_ref = RoiSet.from_binary_mask(
            self.stack_ch_pa,
            self.seg_mask,
            params=RoiSetMetaParams(
                mask_type='contours',
                filters={'area': {'min': 1e1, 'max': 1e6}}
            )
        )

        poly = roiset_ref.get_polygons()
        roiset_test = RoiSet.from_polygons_2d(self.stack_ch_pa, poly)
        binary_poly = (roiset_test.acc_obj_ids.get_mono(0, mip=True).data > 0)
        self.assertEqual(self.seg_mask.shape, binary_poly.shape)


        # most mask pixels are within in fitted polygon
        test_mask = np.logical_and(
            np.logical_not(binary_poly),
            (self.seg_mask.data == 255)
        )
        self.assertLess(test_mask.sum() / test_mask.size, 0.001)

        # output results
        od = output_path / 'polygons'
        write_accessor_data_to_file(od / 'from_polygons.tif', InMemoryDataAccessor(binary_poly))
        write_accessor_data_to_file(od / 'ref_mask.tif', self.seg_mask)
        write_accessor_data_to_file(od / 'diff.tif', InMemoryDataAccessor(test_mask))


    def test_overlap_bbox(self):
        df = pd.DataFrame({
            ('bounding_box', 'x0'): [0, 1, 2, 1, 1],
            ('bounding_box', 'x1'): [2, 3, 4, 3, 3],
            ('bounding_box', 'y0'): [0, 0, 0, 2, 0],
            ('bounding_box', 'y1'): [2, 2, 2, 3, 2],
            ('bounding_box', 'zi'): [0, 0, 0, 0, 1],
        })
        df.set_index(pd.Index(range(100, 105)), inplace=True)
        res = filter_df_overlap_bbox(df)
        self.assertEqual(len(res), 4)
        self.assertTrue((res.loc[100, 'overlaps_with'] == [101]).all())
        self.assertTrue((res.loc[101, 'overlaps_with'] == [100, 102]).all())
        self.assertTrue((res.bbox_intersec == 2).all())
        return res


    def test_overlap_bbox_multiple(self):
        df1 = pd.DataFrame({
            ('bounding_box', 'x0'): [0, 1],
            ('bounding_box', 'x1'): [2, 3],
            ('bounding_box', 'y0'): [0, 0],
            ('bounding_box', 'y1'): [2, 2],
            ('bounding_box', 'zi'): [0, 0],
        })
        df1.set_index(pd.Index(range(100, 102)), inplace=True)
        df2 = pd.DataFrame({
            ('bounding_box', 'x0'): [2],
            ('bounding_box', 'x1'): [4],
            ('bounding_box', 'y0'): [0],
            ('bounding_box', 'y1'): [2],
            ('bounding_box', 'zi'): [0],
        })
        df2.set_index(pd.Index(range(200, 201)), inplace=True)
        res = filter_df_overlap_bbox(df1, df2)
        self.assertTrue((res.loc[101, 'overlaps_with'] == [200]).all())
        self.assertEqual(len(res), 1)
        self.assertTrue((res.bbox_intersec == 2).all())


    def test_overlap_seg(self):
        df = pd.DataFrame({
            ('bounding_box', 'x0'): [0, 1, 2],
            ('bounding_box', 'x1'): [2, 3, 4],
            ('bounding_box', 'y0'): [0, 0, 0],
            ('bounding_box', 'y1'): [2, 2, 2],
            ('bounding_box', 'zi'): [0, 0, 0],
            ('masks', 'binary_mask'): [
                [
                    [1, 1],
                    [1, 0]
                ],
                [
                    [0, 1],
                    [1, 1]
                ],
                [
                    [1, 1],
                    [1, 1]
                ],
            ]
        })
        df.set_index(pd.Index(range(100, 103)), inplace=True)
        res = filter_df_overlap_seg(df)
        self.assertTrue((res.loc[res.seg_overlaps, :].index == [101, 102]).all())
        self.assertTrue((res.loc[res.seg_overlaps, 'seg_iou'] == 0.4).all())

    def test_overlap_seg_multiple(self):
        df1 = pd.DataFrame({
            ('bounding_box', 'x0'): [0, 1],
            ('bounding_box', 'x1'): [2, 3],
            ('bounding_box', 'y0'): [0, 0],
            ('bounding_box', 'y1'): [2, 2],
            ('bounding_box', 'zi'): [0, 0],
            ('masks', 'binary_mask'): [
                [
                    [1, 1],
                    [1, 0]
                ],
                [
                    [0, 1],
                    [1, 1]
                ],
            ]
        })
        df1.set_index(pd.Index(range(100, 102)), inplace=True)
        df2 = pd.DataFrame({
            ('bounding_box', 'x0'): [2],
            ('bounding_box', 'x1'): [4],
            ('bounding_box', 'y0'): [0],
            ('bounding_box', 'y1'): [2],
            ('bounding_box', 'zi'): [0],
            ('masks', 'binary_mask'): [
                [
                    [1, 1],
                    [1, 1]
                ],
            ]
        })
        df2.set_index(pd.Index(range(200, 201)), inplace=True)
        res = filter_df_overlap_seg(df1, df2)
        self.assertTrue((res.loc[101, 'overlaps_with'] == [200]).all())
        self.assertEqual(len(res), 1)
        self.assertTrue((res.bbox_intersec == 2).all())
        self.assertTrue((res.loc[res.seg_overlaps, :].index == [101]).all())
        self.assertTrue((res.loc[res.seg_overlaps, 'seg_iou'] == 0.4).all())

class TestIntensityThresholdObjectModel(BaseTestRoiSetMonoProducts, unittest.TestCase):
    def test_instance_segmentation(self):

        img = self.stack.get_mono(channel=0, mip=True)
        mask = self.seg_mask

        model = IntensityThresholdInstanceMaskSegmentationModel()
        obmap = model.label_instance_class(img, mask)

        img.write(output_path / 'TestIntensityThresholdObjectModel' / 'img.tif')
        mask.write(output_path / 'TestIntensityThresholdObjectModel' / 'mask.tif')
        obmap.write(output_path / 'TestIntensityThresholdObjectModel' / 'obmap.tif')

        self.assertGreater((mask.data > 0).sum(), (obmap.data > 0).sum())

    def test_roiset_with_instance_segmentation(self):
        roiset = RoiSet.from_binary_mask(
            self.stack,
            self.seg_mask,
            params=RoiSetMetaParams(
                mask_type='countours',
                filters={'area': {'min': 1e3, 'max': 1e4}},
                expand_box_by=(128, 2),
                deproject_channel=0
            )
        )
        cl1 = 'permissive_model'
        roiset.classify_by(cl1, [0], IntensityThresholdInstanceMaskSegmentationModel(tr=0.0))
        self.assertEqual(roiset.df().classifications[cl1].sum(), roiset.count)

        cl2 = 'avg_intensity'
        roiset.classify_by(cl2, [0], IntensityThresholdInstanceMaskSegmentationModel(tr=0.2))
        self.assertLess(roiset.df().classifications[cl2].sum(), roiset.count)
        return roiset

    def test_aggregate_classification_results(self):
        roiset = self.test_roiset_with_instance_segmentation()
        om_mod = roiset.get_object_class_map('permissive_model')
        om_tr = roiset.get_object_class_map('avg_intensity')
        om_fil = roiset.get_object_class_map('permissive_model', filter_by=['avg_intensity'])
        self.assertTrue(np.all(om_fil.unique()[0] == [0, 1]))
        self.assertEqual(om_fil.data.sum(), om_tr.data.sum())
        self.assertGreater(om_mod.data.sum(), om_fil.data.sum())