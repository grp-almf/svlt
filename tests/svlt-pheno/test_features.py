import unittest

from svlt.accessors import generate_file_accessor
import svlt.conf.testing as conf
from svlt.rois.features import regionprops
from svlt.rois.roiset import RoiSet, RoiSetMetaParams


data = conf.meta['image_files']
output_path = conf.meta['output_path']
params = conf.meta['roiset']
stack = generate_file_accessor(data['multichannel_zstack_raw']['path'])
seg_mask = generate_file_accessor(data['multichannel_zstack_mask3d']['path'])


class TestRoiSetMonoProducts(unittest.TestCase):

    def _make_roi_set(self, mask_type='boxes', **kwargs):
        roiset = RoiSet.from_binary_mask(
            stack,
            seg_mask,
            params=RoiSetMetaParams(
                mask_type=mask_type,
                filters=kwargs.get('filters', {'area': {'min': 1e3, 'max': 1e4}}),
                expand_box_by=(128, 2)
            ),
            allow_3d=True,
        )
        return roiset

    def test_multichannel_regionprops_features(self):
        roiset = self._make_roi_set()
        self.assertGreater(roiset.acc_raw.chroma, 1)
        self.assertGreater(roiset.acc_obj_ids.nz, 1)
        features_2d = regionprops(roiset, make_3d=False)
        features_3d = regionprops(roiset, make_3d=True)
        self.assertTrue(all(features_3d.area == roiset.df().bounding_box.area))

        # test channel-specific
        for s in ['max', 'min', 'mean', 'std']:
            self.assertTrue(
                all([f'intensity_{s}-{c}' in features_2d.columns for c in range(0, roiset.acc_raw.chroma)])
            )
            self.assertTrue(
                all([f'intensity_{s}-{c}' in features_3d.columns for c in range(0, roiset.acc_raw.chroma)])
            )

    def test_mono_regionprops_features(self):
        ch = 2
        roiset = self._make_roi_set()
        self.assertGreater(roiset.acc_raw.chroma, 1)
        self.assertGreater(roiset.acc_obj_ids.nz, 1)
        features_2d = regionprops(roiset, make_3d=False, channel=ch)
        features_3d = regionprops(roiset, make_3d=True, channel=ch)
        self.assertTrue(all(features_3d.area == roiset.df().bounding_box.area))

        # test channel-specific
        for s in ['max', 'min', 'mean', 'std']:
            self.assertTrue(f'intensity_{s}-{ch}' in features_2d.columns)
            self.assertTrue(all(
                [f'intensity_{s}-{c}' not in features_2d.columns for c in range(0, roiset.acc_raw.chroma) if c != ch]
            ))
            self.assertTrue(f'intensity_{s}-{ch}' in features_3d.columns)
            self.assertTrue(all(
                [f'intensity_{s}-{c}' not in features_3d.columns for c in range(0, roiset.acc_raw.chroma) if c != ch]
            ))

    def test_roiset_extract_features(self):
        roiset = self._make_roi_set()
        roiset.extract_features(regionprops, make_3d=True, channel=2)
        self.assertTrue(('features', 'intensity_std-2') in roiset.df().columns)
        self.assertTrue('intensity_std-2' in roiset.get_features())

    def test_empty_roiset_extract_features(self):
        import numpy as np
        from svlt.accessors import InMemoryDataAccessor

        arr_mask = InMemoryDataAccessor(np.zeros([*stack.hw, 1, stack.nz], dtype='uint8'))

        roiset = RoiSet.from_binary_mask(
            stack,
            arr_mask,
            params=RoiSetMetaParams(
                filters={'area': {'min': 1e3, 'max': 1e4}},
                expand_box_by=(128, 2)
            ),
            allow_3d=True,
        )
        self.assertEqual(roiset.count, 0)
        roiset.extract_features(regionprops, make_3d=True, channel=2)
        self.assertTrue(('features', 'intensity_std-2') not in roiset.df().columns)
        self.assertIsNone(roiset.get_features())