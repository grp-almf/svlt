import svlt.conf.testing as conf

czifile = conf.meta['image_files']['czifile']


class TestApiFromAutomatedClient(conf.TestServerBaseClass):

    def test_permissive_instance_segmentation_model(self):
        self.assertPutSuccess(
            '/pheno/models/classify/threshold/load',
            body={}
        )