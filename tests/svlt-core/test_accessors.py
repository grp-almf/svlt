import unittest

import numpy as np

from svlt.accessors import PatchStack, make_patch_stack_from_file, FileNotFoundError, Nd2ImageFileAccessor

from svlt.accessors import CziImageFileAccessor, DataShapeError, generate_file_accessor, InMemoryDataAccessor, PngFileAccessor, write_accessor_data_to_file, TifSingleSeriesFileAccessor
import svlt.conf.testing as conf

data = conf.meta['image_files']
output_path = conf.meta['output_path']

def _random_int(*args):
    return np.random.randint(0, 2 ** 8, size=args, dtype='uint8')

class TestCziImageFileAccess(unittest.TestCase):

    def setUp(self) -> None:
        pass

    def test_tiffile_is_correct_shape(self):
        tf = generate_file_accessor(data['tifffile']['path'])

        self.assertIsInstance(tf, TifSingleSeriesFileAccessor)
        self.assertEqual(tf.shape_dict['Y'], data['tifffile']['h'])
        self.assertEqual(tf.shape_dict['X'], data['tifffile']['w'])
        self.assertEqual(tf.chroma, data['tifffile']['c'])
        self.assertTrue(tf.is_3d())
        self.assertEqual(len(tf.data.shape), 4)
        self.assertEqual(tf.shape[0], data['tifffile']['h'])
        self.assertEqual(tf.shape[1], data['tifffile']['w'])
        self.assertEqual(tf.get_axis('x'), 1)

    def test_czifile_is_correct_shape(self):
        cf = CziImageFileAccessor(data['czifile']['path'])
        self.assertEqual(cf.shape_dict['Y'], data['czifile']['h'])
        self.assertEqual(cf.shape_dict['X'], data['czifile']['w'])
        self.assertEqual(cf.chroma, data['czifile']['c'])
        self.assertFalse(cf.is_3d())
        self.assertEqual(len(cf.data.shape), 4)
        self.assertEqual(cf.shape[0], data['czifile']['h'])
        self.assertEqual(cf.shape[1], data['czifile']['w'])

    def test_get_single_channel_from_zstack(self):
        w = 256
        h = 512
        nc = 4
        nz = 11
        c = 3
        cf = InMemoryDataAccessor(_random_int(h, w, nc, nz))
        sc = cf.get_mono(c)
        self.assertEqual(sc.shape, (h, w, 1, nz))

    def test_get_single_channel_mip_from_zstack(self):
        w = 256
        h = 512
        nc = 4
        nz = 11
        c = 3
        cf = InMemoryDataAccessor(np.random.rand(h, w, nc, nz))
        sc = cf.get_mono(c, mip=True)
        self.assertEqual(sc.shape, (h, w, 1, 1))

    def test_get_single_channel_argmax_from_zstack(self):
        w = 256
        h = 512
        nc = 4
        nz = 11
        c = 3
        cf = InMemoryDataAccessor(np.random.rand(h, w, nc, nz))
        am = cf.get_mono(c).get_z_argmax()
        self.assertEqual(am.shape, (h, w, 1, 1))
        self.assertTrue(np.all(am.unique()[0] == range(0, nz)))

    def test_get_single_channel_z_series_from_zstack(self):
        w = 256
        h = 512
        nc = 4
        nz = 11
        c = 3
        cf = InMemoryDataAccessor(np.random.rand(h, w, nc, nz))
        zs = cf.get_mono(c).get_focus_vector()
        self.assertEqual(zs.shape, (nz, ))

    def test_get_zi(self):
        w = 256
        h = 512
        nc = 4
        nz = 11
        zi = 5
        cf = InMemoryDataAccessor(_random_int(h, w, nc, nz))
        sz = cf.get_zi(zi)
        self.assertEqual(sz.shape_dict['Z'], 1)

        self.assertTrue(np.all(sz.data[:, :, :, 0] == cf.data[:, :, :, zi]))

    def test_get_mip(self):
        w = 256
        h = 512
        nc = 4
        nz = 11
        zi = 5
        cf = InMemoryDataAccessor(_random_int(h, w, nc, nz))
        sm = cf.get_mip()
        self.assertEqual(sm.shape_dict['Z'], 1)
        self.assertTrue(np.all(cf.data.max(axis=-1, keepdims=True) == sm.data))

    def test_get_mono(self):
        w = 256
        h = 512
        nc = 4
        nz = 11
        zi = 5
        acc = InMemoryDataAccessor(_random_int(h, w, nc, nz))
        self.assertEqual(acc.get_mono(0).data_mono.shape, (h, w, nz))

    def test_make_from_mono_2d(self):
        w = 256
        h = 512
        nda = _random_int(h, w)
        acc = InMemoryDataAccessor.from_mono(nda)
        self.assertEqual(acc.chroma, 1)
        self.assertEqual(acc.hw, (h, w))
        self.assertEqual(acc.nz, 1)

    def test_make_from_mono_3d(self):
        w = 256
        h = 512
        nz = 11
        nda = _random_int(h, w, nz)
        acc = InMemoryDataAccessor.from_mono(nda)
        self.assertEqual(acc.chroma, 1)
        self.assertEqual(acc.hw, (h, w))
        self.assertEqual(acc.nz, nz)

    def test_crop_yx(self):
        w = 256
        h = 512
        nc = 4
        nz = 11
        cf = InMemoryDataAccessor(_random_int(h, w, nc, nz))

        # test 2d cropping
        yxhw = (100, 200, 10, 20)
        sc2d = cf.crop_hw(yxhw)
        self.assertEqual(sc2d.shape_dict['Z'], nz)
        self.assertEqual(sc2d.shape_dict['C'], nc)
        self.assertEqual(sc2d.hw, yxhw[2:])

        # test 3d cropping
        depth = 3
        yxzhwd = (100, 200, 3, 10, 20, depth)
        sc3d = cf.crop_hwd(yxzhwd)
        self.assertEqual(sc3d.shape_dict['Z'], depth)
        self.assertEqual(sc3d.shape_dict['C'], nc)
        self.assertEqual(sc3d.hw, yxhw[2:])

    def test_yxzc(self):
        w = 256
        h = 512
        nc = 4
        nz = 11
        cf = InMemoryDataAccessor(_random_int(h, w, nc, nz))

        self.assertEqual(cf.data_yxzc.shape, (h, w, nz, nc))


    def test_write_single_channel_tif(self):
        ch = 4
        cf = CziImageFileAccessor(data['czifile']['path'])
        mono = cf.get_mono(ch)
        self.assertTrue(
            write_accessor_data_to_file(
                output_path / f'{cf.fpath.stem}_ch{ch}.tif',
                mono
            )
        )
        self.assertEqual(cf.data.shape[0:2], mono.data.shape[0:2])
        self.assertEqual(cf.data.shape[3], mono.data.shape[2])

    def test_write_two_channel_png(self):
        from svlt.process import resample_to_8bit
        cf = CziImageFileAccessor(data['czifile']['path'])
        acc = cf.get_channels([0, 1])
        opa = output_path / f'{cf.fpath.stem}_2ch.png'
        acc_out = acc.apply(resample_to_8bit, preserve_dtype=False)

        self.assertTrue(
            write_accessor_data_to_file(opa, acc_out)
        )
        val_acc = generate_file_accessor(opa)
        self.assertEqual(val_acc.chroma, 3)
        self.assertTrue(np.all(val_acc.data[:, :, -1, :] == 0))  # final channel is blank

    def test_conform_data_shorter_than_xycz(self):
        h = 256
        w = 512
        data = _random_int(h, w, 1)
        acc = InMemoryDataAccessor(data)
        self.assertEqual(
            InMemoryDataAccessor.conform_data(data).shape,
            (256, 512, 1, 1)
        )
        self.assertEqual(
            acc.shape_dict,
            {'Y': 256, 'X': 512, 'C': 1, 'Z': 1}
        )

    def test_conform_data_longer_than_xycz(self):
        data = _random_int(256, 512, 12, 8, 3)
        with self.assertRaises(DataShapeError):
            acc = InMemoryDataAccessor(data)


    def test_write_multichannel_image_preserve_axes(self):
        h = 256
        w = 512
        c = 3
        nz = 10

        yxcz = _random_int(h, w, c, nz)
        acc = InMemoryDataAccessor(yxcz)
        fp = output_path / f'rand3d.tif'
        self.assertTrue(
            write_accessor_data_to_file(fp, acc)
        )
        # need to sort out x,y flipping since np convention yxcz flips axes in 3d tif
        self.assertEqual(acc.shape_dict['X'], w, acc.shape_dict)
        self.assertEqual(acc.shape_dict['Y'], h, acc.shape_dict)

        # re-open file and check axes order
        from tifffile import TiffFile
        fh = TiffFile(fp)
        self.assertEqual(len(fh.series), 1)
        se = fh.series[0]
        fh_shape_dict = {se.axes[i]: se.shape[i] for i in range(0, len(se.shape))}
        self.assertEqual(fh_shape_dict, acc.shape_dict, 'Axes are not preserved in TIF output')

    def test_read_png(self, pngfile=data['rgbpngfile']):
        acc = PngFileAccessor(pngfile['path'])
        self.assertEqual(acc.hw, (pngfile['h'], pngfile['w']))
        self.assertEqual(acc.chroma, pngfile['c'])
        self.assertEqual(acc.nz, 1)

    def test_read_mono_png(self):
        return self.test_read_png(pngfile=data['monopngfile'])

    def test_read_zstack_mono_mask(self):
        acc = generate_file_accessor(data['monozstackmask']['path'])
        self.assertTrue(acc.is_mask())

    def test_read_in_pixel_scale_from_czi(self):
        cf = CziImageFileAccessor(data['czifile']['path'])
        pxs = cf.pixel_scale_in_micrometers
        self.assertAlmostEqual(pxs['X'], data['czifile']['um_per_pixel'], places=3)

    def test_append_channels(self):
        w = 256
        h = 512
        nc = 2
        nz = 11
        acc1 = InMemoryDataAccessor(np.random.rand(h, w, nc, nz))
        acc2 = InMemoryDataAccessor(np.random.rand(h, w, nc, nz))

        app = acc1.append_channels(acc2)
        self.assertEqual(app.shape, (h, w, 2 * nc, nz))
        self.assertTrue(
            np.all(
                app.get_channels([0, 1]).data == acc1.data
            )
        )
        self.assertTrue(
            np.all(
                app.get_channels([2, 3]).data == acc2.data
            )
        )

    def test_lazy_load(self):
        # initialize an accessor with lazy flag
        acc_cf = generate_file_accessor(data['czifile']['path'], lazy=True)
        self.assertIsNone(acc_cf._data)
        self.assertIsNone(acc_cf._metadata)

        # implicitly load by accessing its data property
        nda = acc_cf.data
        self.assertIsNotNone(acc_cf._data)
        self.assertIsNotNone(acc_cf._metadata)
        self.assertIsInstance(acc_cf._data, np.ndarray)
        self.assertIsInstance(acc_cf._metadata, dict)

    def test_apply_function_with_params(self):
        acc = InMemoryDataAccessor(np.ones((16, 16, 1, 3), dtype='uint8'))
        def _multiply(nda, by):
            return nda * by
        res = acc.apply(_multiply, {'by': 2})
        self.assertTrue(np.all(res.data == 2))

    def test_apply_mono_function(self):
        acc = InMemoryDataAccessor(np.ones((16, 16, 2, 3), dtype='uint8'))
        with self.assertRaises(DataShapeError):
            res = acc.apply(lambda x: 2 * x, mono=True)
        res = acc.get_mono(channel=0).apply(lambda x: 2 * x, mono=True)
        self.assertTrue(np.all(res.data == 2))
        self.assertEqual(res.nz, acc.nz)


class TestPatchStackAccessor(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def test_make_patch_stack_from_3d_array(self):
        w = 256
        h = 512
        n = 4
        acc = PatchStack(_random_int(n, h, w, 1, 1))
        self.assertEqual(acc.count, n)
        self.assertEqual(acc.hw, (h, w))
        self.assertEqual(acc.pyxcz.shape, (n, h, w, 1, 1))

        self.assertEqual(acc.shape[1:], acc.iat(0, crop=True).shape)

    def test_make_patch_stack_from_list(self):
        w = 256
        h = 512
        n = 4
        acc = PatchStack([_random_int(h, w, 1, 1) for _ in range(0, n)])
        self.assertEqual(acc.count, n)
        self.assertEqual(acc.hw, (h, w))
        self.assertEqual(acc.pyxcz.shape, (n, h, w, 1, 1))
        return acc

    def test_make_patch_stack_from_file(self):
        h = data['monozstackmask']['h']
        w = data['monozstackmask']['w']
        c = data['monozstackmask']['c']
        n = data['monozstackmask']['z']

        acc = make_patch_stack_from_file(data['monozstackmask']['path'])
        self.assertEqual(acc.hw, (h, w))
        self.assertEqual(acc.count, n)
        self.assertEqual(acc.pyxcz.shape, (n, h, w, c, 1))

    def test_raises_filenotfound(self):
        with self.assertRaises(FileNotFoundError):
            acc = make_patch_stack_from_file('c:/fake/file/name.tif')

    def test_make_3d_patch_stack_from_nonuniform_list(self):
        w = 256
        h = 512
        c = 3
        nz = 5
        n = 4

        patches = [_random_int(h, w, c, nz) for _ in range(0, n)]
        patches.append(_random_int(h, 2 * w, c, nz))
        acc = PatchStack(patches)
        self.assertEqual(acc.count, n + 1)
        self.assertEqual(acc.hw, (h, 2 * w))
        self.assertEqual(acc.chroma, c)
        self.assertEqual(acc.iat(0).shape, (h, 2 * w, c, nz))
        self.assertEqual(acc.iat_yxcz(0).shape, (h, 2 * w, c, nz))

        # test that initial patches are maintained
        for i in range(0, acc.count):
            self.assertEqual(patches[i].shape, acc.iat(i, crop=True).shape)
            self.assertEqual(acc.shape[1:], acc.iat(i, crop=False).shape)

        ps_list_cropped = acc.get_list(crop=True)
        self.assertTrue(all([np.all(ps_list_cropped[i] == patches[i]) for i in range(0, n)]))

        ps_list_uncropped = acc.get_list(crop=False)
        self.assertTrue(all([p.shape == acc.shape[1:] for p in ps_list_uncropped]))

        # test that this persists after channel selection
        for i in range(0, acc.count):
            self.assertEqual(patches[i].shape[0:2], acc.get_channels([0]).iat(i, crop=True).shape[0:2])
            self.assertEqual(patches[i].shape[3], acc.get_channels([0]).iat(i, crop=True).shape[3])

    def test_write_nonuniform_patches(self):
        w = 256
        h = 512
        c = 3
        nz = 5
        where = output_path / 'write_patches'
        patches = [_random_int(h, w, c, nz), _random_int(2 * h, w, c, nz), _random_int(h, 2 * w, c, nz)]
        acc = PatchStack(patches)

        # a single image file is written
        fp_one_file = where / 'all_patches.tif'
        single_file_path = acc.write(fp_one_file)[0]
        read_acc = PatchStack.read(single_file_path)
        self.assertEqual(read_acc.hw, (2 * h, 2 * w))

        # multiple image files of different size are written
        fp_many_files = where / 'single_patches.tif'
        new_paths = acc.write(fp_many_files, single_file=False)
        for i, fp in enumerate(new_paths):
            read_acc = generate_file_accessor(fp)
            self.assertEqual(read_acc.hw, patches[i].shape[0: 2])

    def test_make_3d_patch_stack_from_list_force_long_dim(self):
        def _r(h, w):
            return np.random.randint(0, 2 ** 8, size=(h, w, 1, 1), dtype='uint8')
        patches = [_r(256, 128), _r(128, 256), _r(512, 10), _r(10, 512)]

        acc_ref = PatchStack(patches, force_ydim_longest=False)
        self.assertEqual(acc_ref.hw, (512, 512))
        self.assertEqual(acc_ref.iat(-1, crop=False).hw, (512, 512))
        self.assertEqual(acc_ref.iat(-1, crop=True).hw, (10, 512))

        acc_rot = PatchStack(patches, force_ydim_longest=True)
        self.assertEqual(acc_rot.hw, (512, 128))
        self.assertEqual(acc_rot.iat(-1, crop=False).hw, (512, 128))
        self.assertEqual(acc_rot.iat(-1, crop=True).hw, (512, 10))

        nda_rot_rot = np.rot90(acc_rot.iat(-1, crop=True).data, axes=(1, 0))
        nda_ref = acc_ref.iat(-1, crop=True).data
        self.assertTrue(np.all(nda_ref == nda_rot_rot))

        self.assertLess(acc_rot.data.size, acc_ref.data.size)

    def test_pczyx(self):
        w = 256
        h = 512
        n = 4
        nz = 15
        nc = 3
        acc = PatchStack(_random_int(n, h, w, nc, nz))
        self.assertEqual(acc.count, n)
        self.assertEqual(acc.pczyx.shape, (n, nc, nz, h, w))
        self.assertEqual(acc.hw, (h, w))
        self.assertEqual(acc.get_mono(channel=0).data_yxz.shape, (n, h, w, nz))
        self.assertEqual(acc.get_mono(channel=0, mip=True).data_yx.shape, (n, h, w))
        return acc

    def test_can_mask(self):
        w = 30
        h = 20
        n = 2
        nz = 3
        nc = 3
        acc1 = PatchStack(_random_int(n, h, w, nc, nz))
        acc2 = PatchStack(_random_int(n, 2*h, w, nc, nz))
        mask = PatchStack(_random_int(n, h, w, 1, nz) > 0.5)
        self.assertFalse(acc1.is_mask())
        self.assertFalse(acc2.is_mask())
        self.assertTrue(mask.is_mask())
        self.assertFalse(acc1.can_mask(acc2))
        self.assertFalse(acc1.can_mask(acc2))
        self.assertTrue(mask.can_mask(acc1))
        self.assertFalse(mask.can_mask(acc2))

    def test_object_df(self):
        w = 30
        h = 20
        n = 2
        nz = 3
        nc = 3
        acc = PatchStack(_random_int(n, h, w, nc, nz))
        mask_data= np.zeros((n, h, w, 1, nz), dtype='uint8')
        mask_data[0, 0:5, 0:5, :, :] = 255
        mask_data[1, 0:10, 0:10, :, :] = 255
        mask = PatchStack(mask_data)
        df = acc.get_mono(0).get_object_df(mask)
        # intensity means are centered around half of full range
        self.assertTrue(np.all(((df['intensity_mean'] / acc.dtype_max) - 0.5)**2 < 1e-2))
        self.assertTrue(df['area'][1] / df['area'][0] == 4.0)
        return acc

    def test_get_one_channel(self):
        acc = self.test_pczyx()
        mono = acc.get_mono(channel=1)
        for a in 'PXYZ':
            self.assertEqual(mono.shape_dict[a], acc.shape_dict[a])
        self.assertEqual(mono.shape_dict['C'], 1)

    def test_get_multiple_channels(self):
        acc = self.test_pczyx()
        channels = [0, 1]
        mcacc = acc.get_channels(channels=channels)
        for a in 'PXYZ':
            self.assertEqual(mcacc.shape_dict[a], acc.shape_dict[a])
        self.assertEqual(mcacc.shape_dict['C'], len(channels))

    def test_get_one_channel_mip(self):
        acc = self.test_pczyx()
        mono_mip = acc.get_mono(channel=1, mip=True)
        for a in 'PXY':
            self.assertEqual(mono_mip.shape_dict[a], acc.shape_dict[a])
        for a in 'CZ':
            self.assertEqual(mono_mip.shape_dict[a], 1)

    def test_export_pczyx_patch_hyperstack(self):
        acc = self.test_pczyx()
        fp = output_path / 'patch_hyperstack.tif'
        acc.export_pyxcz(fp)
        acc2 = make_patch_stack_from_file(fp)
        self.assertEqual(acc.shape, acc2.shape)

    def test_append_channels(self):
        acc = self.test_pczyx()
        app = acc.append_channels(acc)
        p, h, w, nc, nz = acc.shape
        self.assertEqual(app.shape, (p, h, w, 2 * nc, nz))
        self.assertTrue(
            np.all(
                app.get_channels([0, 1, 2]).data == acc.data
            )
        )
        self.assertTrue(
            np.all(
                app.get_channels([3, 4, 5]).data == acc.data
            )
        )
    def test_empty_patch_stack(self):
        with self.assertRaises(ValueError):
            acc = PatchStack([])

class TestNd2ImageFileAccess(unittest.TestCase):
    def test_nd2file_is_correct_shape(self):
        f = data['nd2file']
        acc = generate_file_accessor(f['path'])

        self.assertIsInstance(acc, Nd2ImageFileAccessor)
        self.assertEqual(acc.shape_dict['Y'], f['h'])
        self.assertEqual(acc.shape_dict['X'], f['w'])
        self.assertEqual(acc.chroma, f['c'])

class TestMultipositionCziFileAccess(unittest.TestCase):
    def test_multiposition_czi(self):
        from svlt.accessors import generate_multiposition_file_accessors
        f = data['multipos_czi']
        c = f['c']
        nz = f['z']
        h = f['h']
        w = f['w']
        np = f['p']

        mpacc = generate_multiposition_file_accessors(f['path'], lazy=True)
        self.assertEqual(mpacc.count, np)
        self.assertEqual(
            mpacc.tczyx_iat(1).shape,
            (1, c, nz, h, w)
        )
        self.assertEqual(
            mpacc.iat_data(1).shape,
            (h, w, c, nz)
        )

        # test lazy loading
        self.assertIsNone(mpacc._data[2])
        self.assertEqual(
            mpacc.iat_data(2).shape,
            (h, w, c, nz)
        )
        self.assertIsNotNone(mpacc._data[2])

        # test load all accessors
        accs = mpacc.get_accessors()
        self.assertEqual(len(accs), np)

        self.assertEqual(
            mpacc.pyxcz().shape,
            (np, h, w, c, nz)
        )

        # write last accessor
        accs[-1].write(output_path / 'multiposition_czi' / 'last.tif')