from os.path import exists
import pathlib
import unittest

import numpy as np
from svlt.accessors import generate_file_accessor, InMemoryDataAccessor
from svlt.session import session
import svlt.conf.testing as conf

class TestGetSessionObject(unittest.TestCase):
    def setUp(self) -> None:
        session.restart()
        self.sesh = session

    def test_session_logfile_is_valid(self):
        self.assertTrue(exists(self.sesh.logfile), 'Session did not create a log file in the correct place')

    def test_changing_session_root_creates_new_directory(self):
        old_paths = self.sesh.get_paths()
        self.sesh.restart()
        new_paths = self.sesh.get_paths()
        self.assertEqual(
            old_paths['logs'].parent.parent,
            new_paths['logs'].parent.parent
        )
        self.assertNotEqual(
            old_paths['logs'].parent,
            new_paths['logs'].parent
        )

    def test_change_session_subdirectory(self):
        old_paths = self.sesh.get_paths()
        self.sesh.set_data_directory('outbound_images', old_paths['inbound_images'])
        self.assertEqual(self.sesh.paths['outbound_images'], self.sesh.paths['inbound_images'])

    def test_restarting_session_creates_new_logfile(self):
        logfile1 = self.sesh.logfile
        self.assertTrue(logfile1.exists())
        self.sesh.restart()
        logfile2 = self.sesh.logfile
        self.assertTrue(logfile2.exists())
        self.assertNotEqual(logfile1, logfile2, 'Restarting session does not generate new logfile')

    def test_reimporting_session_uses_same_logfile(self):
        session1 = self.sesh
        logfile1 = session1.logfile
        self.assertTrue(logfile1.exists())

        from svlt.session import session as session2
        self.assertEqual(session1, session2)
        logfile2 = session2.logfile
        self.assertTrue(logfile2.exists())
        self.assertEqual(logfile1, logfile2, 'Reimporting session incorrectly creates new logfile')

    def test_log_warning(self):
        msg = 'A test warning'
        self.sesh.log_info(msg)
        with open(self.sesh.logfile, 'r') as fh:
            log = fh.read()
        self.assertTrue(msg in log)

    def test_get_logs(self):
        self.sesh.log_info('Info example 1')
        self.sesh.log_warning('Example warning')
        self.sesh.log_info('Info example 2')
        logs = self.sesh.get_log_data()
        self.assertEqual(len(logs), 4)
        self.assertEqual(logs[1]['level'], 'WARNING')
        self.assertEqual(logs[-1]['message'], 'Initialized session')

    def test_change_output_path(self):
        pa = self.sesh.get_paths()['inbound_images']
        self.assertIsInstance(pa, pathlib.Path)
        self.sesh.set_data_directory('outbound_images', pa.__str__())
        self.assertEqual(self.sesh.paths['inbound_images'], self.sesh.paths['outbound_images'])
        self.assertIsInstance(self.sesh.paths['outbound_images'], pathlib.Path)


class TestSessionPersistentData(unittest.TestCase):

    def test_add_and_remove_accessor(self):
        data = conf.meta['image_files']
        acc_in = generate_file_accessor(data['multichannel_zstack_raw']['path'])
        shd = acc_in.shape_dict

        # add accessor to session registry
        acc_id = session.add_accessor(acc_in)
        self.assertEqual(session.get_accessor_info(acc_id)['shape_dict'], shd)
        self.assertTrue(session.is_accessor_loaded(acc_id))

        # remove accessor from session registry
        session.del_accessor(acc_id)
        self.assertEqual(session.get_accessor_info(acc_id)['shape_dict'], shd)
        self.assertFalse(session.is_accessor_loaded(acc_id))

    def test_add_and_use_accessor(self):
        acc = InMemoryDataAccessor(
            np.random.randint(
                0,
                2 ** 8,
                size=(512, 256, 3, 7),
                dtype='uint8'
            )
        )
        shd = acc.shape_dict

        # add accessor to session registry
        acc_id = session.add_accessor(acc)
        self.assertEqual(session.get_accessor_info(acc_id)['shape_dict'], shd)
        self.assertTrue(session.is_accessor_loaded(acc_id))

        # get accessor from session registry without popping
        acc_get = session.get_accessor(acc_id, pop=False)
        self.assertIsInstance(acc_get, InMemoryDataAccessor)
        self.assertEqual(acc_get.shape_dict, shd)
        self.assertTrue(session.is_accessor_loaded(acc_id))

        # get accessor from session registry with popping
        acc_get = session.get_accessor(acc_id, pop=True)
        self.assertIsInstance(acc_get, InMemoryDataAccessor)
        self.assertEqual(acc_get.shape_dict, shd)
        self.assertFalse(session.is_accessor_loaded(acc_id))

    def test_memory_is_released(self):
        import tracemalloc

        # allocate memory
        tracemalloc.start()
        acc_id = session.add_accessor(
            InMemoryDataAccessor(
                np.random.randint(
                    0,
                    2 ** 8,
                    size=(100, 100, 10, 100),
                    dtype='uint8'
                )
            )
        )
        snapshot_alloc = tracemalloc.take_snapshot()

        # release memory
        session.del_accessor(acc_id)
        snapshot_release = tracemalloc.take_snapshot()

        diff = snapshot_release.compare_to(snapshot_alloc, key_type='lineno')
        self.assertAlmostEqual(diff[0].size_diff / 10e6, -1, places=2)

    def test_lazy_load_accessor(self):
        acc = generate_file_accessor(
            conf.meta['image_files']['multichannel_zstack_raw']['path'],
            lazy=True
        )

        # accessor remains unloaded in session
        self.assertTrue(acc.lazy)
        self.assertFalse(acc.loaded)
        acc_id = session.add_accessor(acc)
        self.assertFalse(session.is_accessor_loaded(acc_id))

        # session recognizes that accessor is loaded
        data = acc.data
        self.assertTrue(acc.loaded)
        self.assertTrue(session.is_accessor_loaded(acc_id))

        # now delete the accessor object in both test and session scope
        del acc
        session.del_all_accessors()
        self.assertFalse(session.is_accessor_loaded(acc_id))