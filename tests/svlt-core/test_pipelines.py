import unittest

import numpy as np

from svlt.accessors import generate_file_accessor, InMemoryDataAccessor, write_accessor_data_to_file
from svlt.pipelines import segment, segment_zproj
from svlt.pipelines.common import PipelineParams, PipelineRecord, PipelineTrace
from svlt.session import RunTaskError, session

import svlt.conf.testing as conf
from svlt.conf.testing import DummySemanticSegmentationModel

czifile = conf.meta['image_files']['czifile']
zstack = conf.meta['image_files']['tifffile']
output_path = conf.meta['output_path']


class TestSegmentationPipelines(unittest.TestCase):
    def setUp(self) -> None:
        self.model = DummySemanticSegmentationModel()

    def test_call_segment_pipeline(self):
        acc = generate_file_accessor(czifile['path'])
        trace = segment.segment_pipeline({'': acc}, {'': self.model}, channel=2, smooth=3)
        outfp = output_path / 'pipelines' / 'segment_binary_mask.tif'
        write_accessor_data_to_file(outfp, trace.last)

        import tifffile
        img = tifffile.imread(outfp)
        w = czifile['w']
        h = czifile['h']

        self.assertEqual(
            img.shape,
            (h, w),
            'Inferred image is not the expected shape'
        )

        self.assertEqual(
            img[int(w/2), int(h/2)],
            255,
            'Middle pixel is not white as expected'
        )

        self.assertEqual(
            img[0, 0],
            0,
            'First pixel is not black as expected'
        )

        interm_fps = trace.write_interm(
            output_path / 'pipelines' / 'segment_interm',
            prefix=czifile['name'],
            skip_last=False
        )
        self.assertTrue([ofp.stem.split('_')[-1] for ofp in interm_fps] == ['mono', 'inference', 'smooth'])

    def test_call_segment_zproj_pipeline(self):
        acc = generate_file_accessor(zstack['path'])

        trace1 = segment_zproj.segment_zproj_pipeline({'': acc}, {'': self.model}, channel=0, smooth=3, zi=4)
        self.assertEqual(trace1.last.chroma, 1)
        self.assertEqual(trace1.last.nz, 1)

        trace2 = segment_zproj.segment_zproj_pipeline({'': acc}, {'': self.model}, channel=0, smooth=3)
        self.assertEqual(trace2.last.chroma, 1)
        self.assertEqual(trace2.last.nz, 1)

        trace3 = segment_zproj.segment_zproj_pipeline({'': acc}, {'': self.model})
        self.assertEqual(trace3.last.chroma, 1)  # still == 1: model returns a single channel regardless of input
        self.assertEqual(trace3.last.nz, 1)

    def test_append_traces(self):
        acc = generate_file_accessor(zstack['path']).apply(lambda x: x * 0.5)
        trace1 = PipelineTrace(acc)

        from time import sleep
        def _slowly_multiply(arr, by):
            sleep(0.1 * by)
            return arr * by

        trace1['double'] = trace1.last.apply(_slowly_multiply, params={'by': 2})

        trace2 = PipelineTrace(trace1.last)
        trace2['halve'] = trace2.last.apply(_slowly_multiply, params={'by': 0.5})
        trace3 = trace1.append(trace2, skip_first=False)

        self.assertEqual(len(trace3), len(trace1) + len(trace2))
        self.assertEqual(trace3['double'], trace3['appended_input'])
        self.assertTrue(np.all(trace3['input'].data == trace3['halve'].data))
        self.assertGreater(trace3.timer['double'], 0.2)
        self.assertGreater(trace3.timer['halve'], 0.05)

        trace4 = trace1.append(trace2, skip_first=True)
        self.assertEqual(len(trace4), len(trace1) + len(trace2) - 1)
        self.assertTrue(np.all(trace4['input'].data == trace4['halve'].data))

    def test_add_nda_to_trace(self):
        acc = generate_file_accessor(zstack['path'])
        trace1 = PipelineTrace(acc, enforce_accessors=False)
        trace1['halve'] = trace1.last.data * 0.5
        self.assertEqual(len(trace1), 2)

class TestSessionTaskManagement(unittest.TestCase):
    def test_pipeline_with_dependencies(self):
        self.assertEqual(len(session.list_accessors()), 0)

        # make a params model and reserve this dependency's accessor id
        class PipelineParamsWithDependency(PipelineParams):
            dependency_accessor_id: str
        dep_acc_id = session.reserve_accessor_id()
        self.assertFalse(session.get_accessor_info(dep_acc_id)['loaded'])

        # enqueue a task and check that an accessor id is reserved
        def _task(p):
            oid = session.tasks.get_output_accessor_id(p.calling_task_id)
            acc_out = session.get_accessor(p.dependency_accessor_id).apply(lambda x: 2 * x)
            session.bind_accessor(acc_out, oid)
            return PipelineRecord(output_accessor_id=oid, timer={}, success=True)
        params = PipelineParamsWithDependency(api=False, dependency_accessor_id=dep_acc_id)
        task_id = session.tasks.add_task(_task, params=params)
        self.assertEqual(session.tasks.get_task_info(task_id)['status'], 'WAITING')

        # check on state of accessors
        self.assertEqual(len(session.list_accessors()), 2)
        out_acc_id = session.tasks.get_output_accessor_id(task_id)
        self.assertTrue(out_acc_id in session.list_accessors().keys())
        self.assertTrue(dep_acc_id in session.list_accessors().keys())
        self.assertIsNone(session.get_accessor(out_acc_id))
        self.assertFalse(session.get_accessor_info(out_acc_id)['loaded'])
        self.assertEqual(session.get_accessor_info(out_acc_id)['parent_task_id'], task_id)

        # try running task without resolving dependencies
        with self.assertRaises(RunTaskError):
            session.tasks.run_task(task_id, write_all=True)

        # bind dependency input accessor to its reserved ID
        in_acc = InMemoryDataAccessor(np.random.randint(0, 2 ** 8, size=(512, 256, 3, 7), dtype='uint8'))
        session.bind_accessor(
            in_acc,
            dep_acc_id
        )
        self.assertTrue(session.tasks.get_dependent_accessors(task_id) == [dep_acc_id])

        # try deleting dependent accessors but confirm that it's not allowed
        self.assertEqual([v['loaded'] for v in session.list_accessors().values()], [True, False])
        self.assertEqual(session.del_all_accessors(task_id), [])
        self.assertEqual([v['loaded'] for v in session.list_accessors().values()], [True, False])

        self.assertEqual(session.tasks.get_task_info(task_id)['status'], 'READY')
        self.assertTrue(session.tasks.get_accessors_for_cleanup() == [])
        session.tasks.run_task(task_id, write_all=True)

        # now check that accessor_id is bound to an object
        self.assertEqual(len(session.list_accessors()), 2)
        self.assertIsNotNone(out_acc := session.get_accessor(out_acc_id))
        self.assertTrue(session.get_accessor_info(out_acc_id)['loaded'])
        self.assertTrue(np.all(out_acc.data == 2 * in_acc.data))

        # check dependency query
        self.assertTrue(session.tasks.get_dependent_accessors(task_id) == [dep_acc_id])
        self.assertTrue(session.tasks.get_accessors_for_cleanup() == [dep_acc_id, out_acc_id])

        # delete all accessors
        self.assertTrue(all([v['loaded'] for v in session.list_accessors().values()]))
        self.assertEqual(session.del_all_accessors(task_id), [dep_acc_id, out_acc_id])
        self.assertTrue(all([not v['loaded'] for v in session.list_accessors().values()]))
