from pathlib import Path

import svlt.conf.testing as conf

czifile = conf.meta['image_files']['czifile']


class TestApiFromAutomatedClient(conf.TestServerBaseClass):
    
    input_data = czifile
    
    def test_trivial_api_response(self):
        self.assertGetSuccess('')

    def test_bounceback_parameters(self):
        r = self.assertPutSuccess('testing/bounce_back', body={'par1': 'hello', 'par2': ['ab', 'cd']})
        self.assertEqual(r['params']['par1'], 'hello', r)
        self.assertEqual(r['params']['par2'], ['ab', 'cd'], r)

    def test_default_session_paths(self):
        import svlt.conf.defaults
        r = self.assertGetSuccess('paths')
        conf_root = svlt.conf.defaults.root
        for p in ['inbound_images', 'outbound_images', 'logs']:
            self.assertTrue(r[p].startswith(conf_root.__str__()))
            suffix = Path(svlt.conf.defaults.subdirectories[p]).__str__()
            self.assertTrue(r[p].endswith(suffix))

    def test_list_empty_loaded_models(self):
        r = self.assertGetSuccess('models')
        self.assertEqual(r, {})

    def test_load_dummy_semantic_model(self):
        mid = self.assertPutSuccess(f'testing/models/dummy_semantic/load')['model_id']
        rl = self.assertGetSuccess('models')
        self.assertEqual(rl[mid]['class'], 'DummySemanticSegmentationModel')
        return mid

    def test_load_dummy_instance_model(self):
        mid = self.assertPutSuccess(f'testing/models/dummy_instance/load')['model_id']
        rl = self.assertGetSuccess('models')
        self.assertEqual(rl[mid]['class'], 'DummyInstanceMaskSegmentationModel')
        return mid

    def test_pipeline_errors_when_ids_not_found(self):
        fname = self.copy_input_file_to_server()
        model_id = self.assertPutSuccess(f'testing/models/dummy_semantic/load')['model_id']
        in_acc_id = self.assertPutSuccess(f'accessors/read_from_file/{fname}')

        # respond with 409 for invalid accessor_id
        self.assertPutFailure(
            f'pipelines/segment',
            409,
            body={'model_id': model_id, 'accessor_id': 'fake'},
        )

        # respond with 409 for invalid model_id
        self.assertPutFailure(
            f'pipelines/segment',
            409,
            body={'model_id': 'fake', 'accessor_id': in_acc_id}
        )

    def test_i2i_dummy_inference_by_api(self):
        fname = self.copy_input_file_to_server()
        model_id = self.assertPutSuccess(f'testing/models/dummy_semantic/load')['model_id']
        in_acc_id = self.assertPutSuccess(f'accessors/read_from_file/{fname}')

        # run segmentation pipeline on preloaded accessor
        r = self.assertPutSuccess(
            f'pipelines/segment',
            body={
                'accessor_id': in_acc_id,
                'model_id': model_id,
                'channel': 2,
                'keep_interm': True,
            },
        )
        out_acc_id = r['output_accessor_id']
        self.assertTrue(self.assertGetSuccess(f'accessors/get/{out_acc_id}')['loaded'])
        acc_out = self.get_accessor(out_acc_id, 'dummy_semantic_output.tif')
        self.assertEqual(acc_out.shape_dict['C'], 1)

        # validate intermediate data
        resp_list = self.assertGetSuccess(f'accessors')
        self.assertEqual(len(resp_list), 3)

    def test_restarting_session_clears_loaded_models(self):
        self.assertPutSuccess(f'testing/models/dummy_semantic/load')
        rl0 = self.assertGetSuccess('models')
        self.assertEqual(len(rl0), 1, f'Unexpected models in response: {rl0}')
        self.assertGetSuccess('session/restart')
        rl1 = self.assertGetSuccess('models')
        self.assertEqual(len(rl1), 0, f'Unexpected models in response: {rl1}')

    def test_change_inbound_path(self):
        r_inpath = self.assertGetSuccess('paths')
        self.assertPutSuccess(
            f'paths/watch_output',
            query={'path': r_inpath['inbound_images']}
        )
        r_check = self.assertGetSuccess('paths')
        self.assertEqual(r_check['inbound_images'], r_check['outbound_images'])

    def test_exception_when_changing_inbound_path(self):
        r_inpath = self.assertGetSuccess('paths')
        fakepath = 'c:/fake/path/to/nowhere'
        r_change = self.assertPutFailure(
            f'paths/watch_output',
            404,
            query={'path': fakepath},
        )
        self.assertIn(fakepath, r_change.json()['detail'])
        r_check = self.assertGetSuccess('paths')
        self.assertEqual(r_inpath['outbound_images'], r_check['outbound_images'])

    def test_no_change_inbound_path(self):
        r_inpath = self.assertGetSuccess('paths')
        r_change = self.assertPutSuccess(
            f'paths/watch_output',
            query={'path': r_inpath['outbound_images']}
        )
        r_check = self.assertGetSuccess('paths')
        self.assertEqual(r_inpath['outbound_images'], r_check['outbound_images'])

    def test_get_logs(self):
        r = self.assertGetSuccess('session/logs')
        self.assertEqual(r[0]['message'], 'Initialized session')

    def test_add_and_delete_accessor(self):
        fname = self.copy_input_file_to_server()

        # add accessor to session
        acc_id = self.assertPutSuccess(
            f'accessors/read_from_file/{fname}',
        )
        self.assertTrue(acc_id.startswith('acc_'))

        # confirm that accessor is listed in session context
        acc_list = self.assertGetSuccess(
            f'accessors',
        )
        self.assertEqual(len(acc_list), 1)
        self.assertTrue(list(acc_list.keys())[0].startswith('acc_'))
        self.assertTrue(acc_list[acc_id]['loaded'])

        # delete and check that its 'loaded' state changes
        self.assertTrue(self.assertGetSuccess(f'accessors/get/{acc_id}')['loaded'])
        self.assertEqual(self.assertGetSuccess(f'accessors/delete/{acc_id}'), acc_id)
        self.assertFalse(self.assertGetSuccess(f'accessors/get/{acc_id}')['loaded'])

        # and try a non-existent accessor ID
        self.assertGetFailure('accessors/get/auto_123456', 404)

        # load another... then remove all
        self.assertPutSuccess(f'accessors/read_from_file/{fname}')
        self.assertEqual(sum([v['loaded'] for v in self.assertGetSuccess('accessors').values()]), 1)
        self.assertEqual(len(self.assertGetSuccess(f'accessors/delete/*')), 1)
        self.assertEqual(sum([v['loaded'] for v in self.assertGetSuccess('accessors').values()]), 0)

    def test_empty_accessor_list(self):
        r_list = self.assertGetSuccess(
            f'accessors',
        )
        self.assertEqual(len(r_list), 0)

    def test_write_accessor(self):
        acc_id = self.assertPutSuccess('/testing/accessors/dummy_accessor/load')
        self.assertTrue(self.assertGetSuccess(f'accessors/get/{acc_id}')['loaded'])
        sd = self.assertGetSuccess(f'accessors/get/{acc_id}')['shape_dict']
        self.assertEqual(self.assertGetSuccess(f'accessors/get/{acc_id}')['filepath'], '')
        acc_out = self.get_accessor(accessor_id=acc_id, filename='test_output.tif')
        self.assertEqual(sd, acc_out.shape_dict)

    def test_binary_segmentation_model(self):
        mid = self.assertPutSuccess(
            '/models/seg/threshold/load/', body={'tr': 0.1}
        )['model_id']

        fname = self.copy_input_file_to_server()
        acc_id = self.assertPutSuccess(f'accessors/read_from_file/{fname}')
        r = self.assertPutSuccess(
            f'pipelines/segment',
            body={
                'accessor_id': acc_id,
                'model_id': mid,
                'channel': 0,
                'keep_interm': True,
            },
        )
        acc = self.get_accessor(r['output_accessor_id'])
        self.assertTrue(all(acc.unique()[0] == [0, 1]))
        self.assertTrue(all(acc.unique()[1] > 0))

    def test_queue_segmentation_pipeline(self):
        mid = self.assertPutSuccess(
            '/models/seg/threshold/load/', body={'tr': 0.1}
        )['model_id']

        fname = self.copy_input_file_to_server()
        acc_id = self.assertPutSuccess(f'accessors/read_from_file/{fname}')

        # enqueue task
        queue_resp = self.assertPutSuccess(
            f'pipelines/segment',
            body={
                'accessor_id': acc_id,
                'model_id': mid,
                'channel': 0,
                'keep_interm': True,
                'schedule': True,
            },
        )
        task_id = queue_resp['task_id']
        self.assertEqual(self.assertGetSuccess(f'/tasks/get/{task_id}')['status'], 'READY')
        self.assertEqual(
            self.assertGetSuccess(f'/tasks/get/{task_id}')['target'],
            'svlt.pipelines.segment.segment_pipeline'
        )

        # run task
        run_resp = self.assertPutSuccess(f'/tasks/run/{task_id}')
        self.assertEqual(self.assertGetSuccess(f'/tasks/get/{task_id}')['status'], 'FINISHED')
        acc = self.get_accessor(run_resp['output_accessor_id'])
        self.assertTrue(all(acc.unique()[0] == [0, 1]))
        self.assertTrue(all(acc.unique()[1] > 0))

    def test_run_dummy_task(self):
        acc_id = self.assertPutSuccess('/testing/accessors/dummy_accessor/load')
        acc_in = self.get_accessor(acc_id)

        task_id = self.assertPutSuccess(
            '/testing/tasks/create_dummy_task',
            body={
                'accessor_id': acc_id,
                'keep_interm': True,
            }
        )['task_id']
        task_info1 = self.assertGetSuccess(f'/tasks/get/{task_id}')
        self.assertEqual(task_info1['status'], 'READY')

        # run the task and compare results
        rec = self.assertPutSuccess(f'/tasks/run/{task_id}')
        task_info2 = self.assertGetSuccess(f'/tasks/get/{task_id}')
        self.assertEqual(task_info2['status'], 'FINISHED')
        acc_out = self.get_accessor(task_info2['result']['output_accessor_id'])
        self.assertTrue((acc_out.data == (acc_in.data + 1) * 2).all())

        # evaluate interm products
        accs = self.assertGetSuccess('/accessors')
        ks = list(accs.keys())
        self.assertEqual(len(accs), 3)
        self.assertTrue(accs[ks[1]]['loaded'])
        self.assertTrue(accs[ks[2]]['loaded'])

        # now delete accessors associated with task
        self.assertPutSuccess(f'/tasks/delete_accessors/{task_id}')
        accs = self.assertGetSuccess('/accessors')
        self.assertEqual(len(accs), 3)
        self.assertFalse(accs[ks[1]]['loaded'])
        self.assertFalse(accs[ks[2]]['loaded'])


    def test_run_failed_dummy_task(self):
        acc_id = self.assertPutSuccess('/testing/accessors/dummy_accessor/load')
        acc_in = self.get_accessor(acc_id)

        task_id = self.assertPutSuccess(
            '/testing/tasks/create_dummy_task',
            body={'accessor_id': acc_id, 'break_me': True}
        )['task_id']
        task_info1 = self.assertGetSuccess(f'/tasks/get/{task_id}')
        self.assertEqual(task_info1['status'], 'READY')

        # run the task and compare results
        rec = self.assertPutFailure(f'/tasks/run/{task_id}', 500)
        task_info2 = self.assertGetSuccess(f'/tasks/get/{task_id}')
        self.assertEqual(task_info2['status'], 'FAILED')

    def test_run_multiple_tasks(self):
        n = 3
        for _ in range(0, n):
            acc_id = self.assertPutSuccess('/testing/accessors/dummy_accessor/load')

            task_id = self.assertPutSuccess(
                '/testing/tasks/create_dummy_task',
                body={
                    'accessor_id': acc_id,
                    'keep_interm': True,
                }
            )['task_id']

        # confirm tasks are defined but not yet run
        tasks = self.assertGetSuccess('/tasks')
        self.assertEqual(len(tasks), n)
        self.assertTrue(all([v['status'] == 'READY' for v in tasks.values()]))
        self.assertTrue(all([v['result'] is None for v in tasks.values()]))
        self.assertEqual(len(self.assertGetSuccess('/tasks/by_status')['WAITING']), 0)
        self.assertEqual(len(self.assertGetSuccess('/tasks/by_status')['READY']), n)
        self.assertEqual(len(self.assertGetSuccess('/tasks/ready')), n)
        self.assertEqual(len(self.assertGetSuccess('/tasks/by_status')['FINISHED']), 0)
        self.assertEqual(self.assertGetSuccess('/tasks/next'), list(self.assertGetSuccess('/tasks/ready').keys())[0])

        # run all tasks
        self.assertPutSuccess('/tasks/run_all', query={'write_all': True})
        tasks = self.assertGetSuccess('/tasks')
        self.assertEqual(len(tasks), n)
        self.assertTrue(all([v['status'] == 'FINISHED' for v in tasks.values()]))
        self.assertTrue(all([v['result'] is not None for v in tasks.values()]))

        # check that output accessors are written automatically
        all_accs = self.assertGetSuccess('/accessors')
        new_accs = []
        for k, v in tasks.items():
            new_accs.append(v['result']['output_accessor_id'])
            for a in v['result']['interm_accessor_ids'].values():
                new_accs.append(a)
        self.assertEqual(len(all_accs), 9)
        self.assertEqual(len(new_accs), 6)
        self.assertTrue(all([all_accs[aid]['filepath'] != '' for aid in new_accs]))
        self.assertEqual(len(self.assertGetSuccess('/tasks/by_status')['READY']), 0)
        self.assertEqual(len(self.assertGetSuccess('/tasks/by_status')['FINISHED']), n)

    def test_read_multiposition_accessor(self):
        test_img = conf.meta['image_files']['multipos_czi']
        fp = test_img['path']
        self.assertTrue(fp.exists())
        self.assertPutSuccess(
            f'paths/watch_input',
            query={'path': fp.parent}
        )
        acc_list = self.assertPutSuccess(f'/accessors/read_multiposition_file/{fp.name}')
        self.assertEqual(len(acc_list), test_img['p'])
        for acc_id in acc_list:
            sd = self.assertGetSuccess(f'/accessors/get/{acc_id}')['shape_dict']
            self.assertEqual(sd['Y'], test_img['h'])
            self.assertEqual(sd['X'], test_img['w'])
            self.assertEqual(sd['C'], test_img['c'])
            self.assertEqual(sd['Z'], test_img['z'])