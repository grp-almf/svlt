# SVLT

Serving Vision to Living Things.

## Summary

SVLT is a service for on-demand computer vision, adapted specifically to image-based feedback in microscopy 
and other life sciences applications.  It abstracts image data access, persists machine learning models, 
and exposes an extensible API to facilitate low-latency analysis.  

## Install Git and miniforge
1. Install Miniforge for environment management:<br>https://github.com/conda-forge/miniforge/releases
2. Under the Start menu, open `Miniforge3 > Miniforge Prompt`

## Install SVLT from source:
1. Install Git:<br>https://git-scm.com/download/win
2. In the new terminal, clone the model_server repository:<br>`cd %userprofile%`<br>`git clone git@git.embl.de:grp-almf/svlt.git`
3. Requirements depend on which SVLT packages to include.  To create the target environment for svlt-pheno:<br>
`mamba env create --file svlt-pheno/requirements.yml --name svlt-pheno-env`
4. Activate the target environment: `mamba activate svlt-pheno-env`
5. Add the same packages from source in editable mode: `pip install --no-deps -e ./svlt-core ./svlt-pheno`

## To start the server:
1. From the Miniforge prompt, run `mamba activate svlt-pheno-env`
2. Then run `python -m scripts.run_server --port 6221`
3. A browser window should appear, with basic status information.
